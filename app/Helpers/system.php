<?php 
namespace Mybookings\Helpers;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Mybookings\Models\HotelMember;
use Mybookings\Models\ChainMember;
use Mybookings\Models\User;
use Session;
use Mybookings\Http\Requests;
use Illuminate\Http\Request;
class System {


	 public static function isAdmin(){
	 	$data = Sentinel::getUser();
        return $data->hasAccess('admin');
    }

    public static function isUser(){
	 	$data = Sentinel::getUser();
        return $data->hasAccess('user');
    }

    public static function currentUser(){
	 	$data = Sentinel::getUser();
        return $data;
    }

    public static function isChainMember(){
    	$data = Sentinel::getUser();
    	$user = User::where('id',$data->id)->first();
    	if($user->currentchainmember->count()==0){
    		return false;
    	}else{
    		return true;
    	}
    }

    public static function isHotelMember(){
        $data = Sentinel::getUser();
        $user = User::where('id',$data->id)->first();
        if($user->currenthotelmember->count()==0){
            return false;
        }else{
            return true;
        }
    }

    public static function isHotelSession(){
        return Session::has('hotel');
    }

}