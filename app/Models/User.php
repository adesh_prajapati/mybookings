<?php

namespace Mybookings\Models;
use Mybookings\Models\Profile;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Mybookings\Models\ChainMember;
use Mybookings\Models\HotelMember;
class User extends CartalystUser{
	protected $table = 'users';
	 protected $fillable = [
        'email',
        'password',
        'last_name',
        'first_name',
        'phone', 
        'permissions',
    ];

    public function profile() {
        return $this->hasOne('Mybookings\Models\Profile');
    }
    public function currentchainmember(){
        return $this->hasMany('Mybookings\Models\ChainMember');
    }
    public function currenthotelmember(){
        return $this->hasMany('Mybookings\Models\HotelMember');
    }
    public static function isAdmin(){
        $user = Sentinel::getUser();
        return $user->hasAccess('admin');
    }


}
