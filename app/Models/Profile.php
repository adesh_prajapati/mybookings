<?php

namespace Mybookings\Models;
use Mybookings\Models\User;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model{
	protected $table = 'profile';
	protected $fillable = array('phone', 'user_id');

}
