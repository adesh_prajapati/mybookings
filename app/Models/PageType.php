<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class PageType extends Model{
	protected $table = 'page_type';

	protected $fillable = array('id', 'name','icon','slug', 'created_at','updated_at');
}
