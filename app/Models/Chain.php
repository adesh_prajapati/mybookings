<?php

namespace Mybookings\Models;
use System;
use Illuminate\Database\Eloquent\Model;
use Mybookings\Models\Country;
class Chain extends Model{
	protected $table = 'chains';
	protected $fillable = ['name','address_1','address_2','postal_code','province','city','country','phone','website','email'];

	public function chainmember(){
    	return $this->hasMany('Mybookings\Models\ChainMember');
    }

    public function hotellists(){
    	return $this->hasMany('Mybookings\Models\Hotel');
    }

    public function phonecode(){
    	return $this->hasOne('Mybookings\Models\Country','country_name','country');
    }

    public static function currentUserChainData(){
        $user_data = System::currentUser();
        $user = User::where('id',$user_data->id)->first();
        return $user->currentchainmember;
    }
}
