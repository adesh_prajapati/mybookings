<?php

namespace Mybookings\Models;
use DateTime;
use DateTimeZone;
use Illuminate\Database\Eloquent\Model;

class HotelRequest extends Model{
	protected $table = 'request';
	protected $fillable = ['id', 'hotel_id', 'title', 'name', 'room_no', 'created_at', 'updated_at'];


	public static function getRequestDetailsByHotelId($hotel_id, $timezone){
		$request_page_data = array();
		$request_details = HotelRequest::where('hotel_id', $hotel_id)->get();
		foreach ($request_details as $request_detail) {
			$date = new DateTime("now", new DateTimeZone($timezone));
			$today = $date->format('Y-m-d H:i:s');
			$thatDay = new DateTime($request_detail->created_time, new DateTimeZone('UTC'));
			$request_date = $thatDay->setTimeZone(new DateTimeZone($timezone));
			$seconds = strtotime($today) - strtotime($thatDay->format('Y-m-d H:i:s'));
			$minutes = floor($seconds /60);
			if($minutes< 10){
				$icon_color = '#1AB394';
			}elseif($minutes > 10 && $minutes < 30){
				$icon_color = '#23C6C8';
			}elseif($minutes > 30 && $minutes < 60){
				$icon_color = '#F8AC59';
			}elseif($minutes >= 60){
				$icon_color = '#ED5565';
			}
			$request_page_data[] = array(
				'id' => $request_detail->id,
				'title' => $request_detail->title,
				'name' => $request_detail->name,
				'room_no' => $request_detail->room_no,
				'status' => $request_detail->status,
				'duration' => $minutes,
				'icon_color' => $icon_color
				);
		}
		return $request_page_data;
	}
}
