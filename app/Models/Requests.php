<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class Requests extends Model{
    protected $table = 'request';
	protected $fillable = ['id', 'hotel_id', 'title', 'name', 'room_no', 'created_at', 'updated_at'];
}
