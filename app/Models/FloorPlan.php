<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class FloorPlan extends Model{
	protected $table = 'floor_plan';
	protected $fillable = ['id', 'hotel_id', 'title', 'image', 'created_at', 'updated_at'];
}
