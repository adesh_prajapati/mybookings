<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model{
	protected $table = 'menu';
	protected $fillable = ['id', 'hotel_id', 'title', 'sub_text', 'currency', 'price', 'category', 'is_published', 'created_at', 'updated_at'];

	public static function getAllMenuServiceDetail($hotel_id){
		$menu_detail = where('hotel_id', $hotel_id)->orderBy('index')->get();
		foreach ($menu_detail as $menu) {
			$menu_data[] = array(
				'id' => $menu->id,
				'title' => $menu->title
				);
		}
	}
}
