<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class Hotspots extends Model{
	protected $table = 'hotspots';
	protected $fillable = ['id', 'hotel_id', 'title', 'address', 'description', 'website', 'category', 'phone', 'show_as_hotspots', 'published', 'created_at', 'updated_at'];

	public static function getAllHotspotsCategory(){
		$category_data = array('Airport' => 'Airport','Amusement' => 'Amusement','Bank/ATM' => 'Bank/ATM',
			'Casino' => 'Casino','Embassy' => 'Embassy','Emergency/Medical' => 'Emergency/Medical',
			'Grocery store' => 'Grocery store','Gym' => 'Gym','Movie Theater' => 'Movie Theater',
			'Museum' => 'Museum','Nightlife' => 'Nightlife','Park' => 'Park',
			'Public transport' => 'Public transport',
			'Religious buildings' => 'Religious buildings','Restaurant' => 'Restaurant','Spa' => 'Spa');
		return $category_data;
	}
}
