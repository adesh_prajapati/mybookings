<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class FaqPage extends Model{
	protected $table = 'faq_page';
	protected $fillable = ['id', 'hotel_id', 'question', 'answer', 'created_at', 'updated_at'];
}
