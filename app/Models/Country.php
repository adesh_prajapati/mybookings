<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model{
    protected $table = 'countries';
    protected $fillable = ['country_code','country_name','name','iso3','numcode','phonecode'];
}
