<?php

namespace Mybookings\Models;
use System;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model{
	protected $table = 'hotels';
	protected $fillable = ['name','slug','chain_id','address_1','address_2','postal_code','province','city','country','phone','website','email','social_facebook','social_twitter','social_instragram'];

	public function chain() {
        return $this->hasOne('Mybookings\Models\Chain');
    }

    public function hotels(){
    	return $this->hasMany('Mybookings\Models\HotelMember');
    }

    public function phonecode(){
    	return $this->hasOne('Mybookings\Models\Country','country_name','country');
    }

    public static function currentUserHotelData(){
        $user_data = System::currentUser();
        $user = User::where('id',$user_data->id)->first();
        return $user->currenthotelmember;
    }

    public static function timezonelist(){
        $zones =  \DateTimeZone::listIdentifiers();
        foreach ($zones as $name => $mask) {
            $tzlist[$mask] = $mask;
        }
        return $tzlist;
    }
}
