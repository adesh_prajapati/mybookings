<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;
use Mybookings\Models\User;
class ChainMember extends Model{
	protected $table = 'chain_members';

	protected $fillable = array('user_id', 'chain_id','role_id');

    public function user() {
        return $this->hasOne('Mybookings\Models\User','id','user_id');
    }

}
