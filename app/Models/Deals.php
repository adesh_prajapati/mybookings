<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class Deals extends Model{
	protected $table = 'deals';
	protected $fillable = ['id', 'hotel_id','name', 'description', 'full_description', 'image',
	'currency', 'price', 'tip_deal', 'show_on_home', 'published', 'created_at', 'updated_at'];
}
