<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;
use Mybookings\Models\User;
class HotelMember extends Model{
	protected $table = 'hotel_members';

	protected $fillable = array('user_id', 'hotel_id','role_id');

	public function user() {
        return $this->hasOne('Mybookings\Models\User','id','user_id');
    }
}
