<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class DirectorySetting extends Model{
	protected $table = 'directory_setting';
	protected $fillable = ['id', 'hotel_id', 'title_color', 'text_color', 'text_link_color', 'icon_color', 'button_color', 'intro_text', 'include_script', 'include_css', 'privacy_statement', 'created_at', 'updated_at'];

	public static function getDirectorySetting($hotel_id){
		$directory_setting = DirectorySetting::where('hotel_id',$hotel_id)->first();
		return $directory_setting;
	}


}
