<?php

namespace Mybookings\Models;

use Illuminate\Database\Eloquent\Model;

class NavigationMenu extends Model{
	protected $table = 'navigation_menu';
	protected $fillable = ['id', 'hotel_id', 'title', 'slug', 'page_type', 'category', 'icon', 'is_published', 'created_at', 'updated_at'];

	public static function getAllNavigationMenu($hotel_id){
		$menu_data = array();
		$data = array();
		$navigation_menu = NavigationMenu::where('hotel_id',$hotel_id)->get();
                $hotel_info = Hotel::where('id', $hotel_id)->first();
                
		foreach ($navigation_menu as $menu) {
			$add_params = '';
			if($menu->page_type == 'deals-tips'){
				$data = json_decode($menu->category);
				if(!$data){
					$add_params = '?type=';
				}else{
					$add_params = '?type='.implode("|", $data);
				}
			}
			if($menu->page_type == 'hotspots' || $menu->page_type == 'contact' || $menu->page_type == 'menu'){
				$data = json_decode($menu->category);
				if(!$data){
					$add_params = '?category=';
				}else{
					$add_params = '?category='.implode("|", $data);
				}
			}
			if($menu->page_type == 'pages'){
				$data = json_decode($menu->category);
				if(!$data){
					$add_params = '';
				}else{
					$add_params = '/'.$data;
				}
			}
			$menu_data[] = array(
				'title' => $menu->title,
				'action' => url($hotel_info->slug.'/directory/'.strtolower($menu->page_type).$add_params)
				);
		}
		return $menu_data;
	}
}
