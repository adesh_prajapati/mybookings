<?php

namespace Mybookings\Models;
use System;
use Illuminate\Database\Eloquent\Model;
use Mybookings\Models\Country;
class Pages extends Model{
	protected $table = 'pages';
	protected $fillable = ['id', 'hotel_id', 'title','description', 'slug', 'page_type', 'created_at', 'updated_at'];

	
}
