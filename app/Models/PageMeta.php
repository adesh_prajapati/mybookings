<?php

namespace Mybookings\Models;
use System;
use Illuminate\Database\Eloquent\Model;
use Mybookings\Models\Country;
class PageMeta extends Model{
	protected $table = 'page_meta';
	protected $fillable = ['id', 'hotel_id', 'key', 'value', 'created_at', 'updated_at'];
}
