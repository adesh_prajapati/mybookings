<?php 
if (!function_exists('pr')) {
	function pr($var) {
		echo '<pre>';
		print_r($var);
		echo '</pre>';
	}
}
if (!function_exists('isActiveRoute')) {
	function isActiveRoute(Array $routes, $output = "active"){
		foreach ($routes as $route){
			if (Route::currentRouteName() == $route) return $output;
		}
	}
}
if (!function_exists('areActiveRoutes')){
	function areActiveRoutes(Array $routes, $output = "active"){
		foreach ($routes as $route)
		{
			if (Route::currentRouteName() == $route) return $output;
		}
	}
}
function toJson($var) {
    header("Content-type: text/json");
    echo json_encode($var);
    die;
}

function toObject($array){
    return json_decode(json_encode($array),false);
}

// function set_active($path, $active = 'active'){ 
// 	return call_user_func_array('Request::is', (array)$path) ? $active : ''; 
// } 
// use it like so <a class="{{ set_active(['admin/institutes*','admin/courses*']) }}">Learning</a>