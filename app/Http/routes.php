<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', [
    'as' => '/', 'uses' => 'Auth\AuthController@getLoginPage'
    ]);
Route::post('login', [
    'as' => 'login', 'uses' => 'Auth\AuthController@postLoginPage'
    ]);
Route::get('dashboard', [
    'as' => 'dashboard', 'uses' => 'Admin\AdminController@getDashboardPage'
    ]);
Route::get('users', [
    'as' => 'users', 
    'uses' => 'Admin\AdminController@getUsers'
    ]);
Route::post('create/user', [
    'as' => 'create/user', 'uses' => 'Admin\AdminController@postCreateUser'
    ]);
Route::post('update/user', [
    'as' => 'update/user', 
    'uses' => 'Admin\AdminController@postUpdateUserInfo'
    ]);
Route::get('logout', [
    'as' => 'logout', 'uses' => 'Auth\AuthController@getLogout'
    ]);
Route::post('user/info', [
    'as' => 'user/info', 'uses' => 'Admin\AdminController@getUserInfo'
    ]);
Route::post('user/delete', [
    'as' => 'user/delete', 'uses' => 'Admin\AdminController@postUserDelete'
    ]);
Route::get('chain/list', [
    'as' => 'chain/list', 'uses' => 'Admin\ChainController@getChainList'
    ]);
Route::post('add/chain', [
    'as' => 'add/chain', 
    'uses' => 'Admin\ChainController@postCreateChain'
    ]);
Route::get('hotel/list', [
    'as' => 'hotel/list', 'uses' => 'Admin\HotelController@getHotelList'
    ]);
Route::post('add/hotel', [
    'as' => 'add/hotel', 'uses' => 'Admin\HotelController@postCreateHotel'
    ]);
Route::post('hotel/slug', [
    'as' => 'hotel/slug', 'uses' => 'Admin\HotelController@postHotelSlug'
    ]);
Route::get('hotel/create', [
    'as' => 'hotel/create', 'uses' => 'Admin\HotelController@getCreateHotelPage'
    ]);
Route::get('chain/view/{id}', [
    'as' => 'chain/view', 'uses' => 'Admin\ChainController@getChainInfo'
    ]);
Route::get('chain/edit/{id}', [
    'as' => 'chain/edit', 'uses' => 'Admin\ChainController@getEditChainPage'
    ]);
Route::get('chain/create', [
    'as' => 'chain/create', 'uses' => 'Admin\ChainController@getChainCreatePage'
    ]);
Route::get('hotel/view/{id}', [
    'as' => 'hotel/view', 'uses' => 'Admin\HotelController@getHotelInfoById'
    ]);
Route::get('hotel/edit/{id}', [
    'as' => 'hotel/edit', 'uses' => 'Admin\HotelController@getEditHotelById'
    ]);
Route::post('assign/user', [
    'as' => 'assign/user', 'uses' => 'Admin\AdminController@getAssignUser'
    ]);
Route::post('remove/access', [
    'as' => 'remove/access', 'uses' => 'Admin\AdminController@getRemoveAccessUser'
    ]);
Route::post('chain/delete', [
    'as' => 'chain/delete', 
    'uses' => 'Admin\ChainController@postDeleteChain'
    ]);
Route::post('hotel/delete', [
    'as' => 'hotel/delete', 'uses' => 'Admin\HotelController@postDeleteHotel'
    ]);
Route::get('manage/page/{id}', [
    'as' => 'manage/page', 
    'uses' => 'Admin\HotelController@getHotelManagePagesByHotelId'
    ]);
Route::get('directory/pages/list', [
    'as' => 'directory/pages/list', 
    'uses' => 'Admin\DirectoryController@getPagesList'
    ]);
Route::post('directory/static/text', [
    'as' => 'directory/static/text', 
    'uses' => 'Admin\DirectoryController@postPagesListByHotelId'
    ]);
Route::get('directory/page/edit/{page_id}', [
    'as' => 'directory/page/edit', 
    'uses' => 'Admin\DirectoryController@getEditHotelPage'
    ]);
Route::get('directory/pages/create', [
    'as' => 'directory/pages/create', 
    'uses' => 'Admin\DirectoryController@getCreateHotelPages'
    ]);
Route::post('directory/pages/create', [
    'as' => 'directory/pages/create', 
    'uses' => 'Admin\DirectoryController@postPageCreate'
    ]);
Route::post('directory/page/delete', [
    'as' => 'directory/page/delete', 
    'uses' => 'Admin\DirectoryController@postPageDelete'
    ]);
Route::get('directory/setting', [
    'as' => 'directory/setting', 
    'uses' => 'Admin\DirectoryController@getSetting'
    ]);
Route::get('directory/page/type', [
    'as' => 'directory/page/type', 
    'uses' => 'Admin\DirectoryController@getPageType'
    ]);
Route::post('directory/setting/create', [
    'as' => 'directory/setting/create', 
    'uses' => 'Admin\DirectoryController@postSetting'
    ]);
Route::get('directory/hotspots/list', [
    'as' => 'directory/hotspots/list', 
    'uses' => 'Admin\DirectoryController@getHotSpots'
    ]);
Route::get('directory/hotspots/create', [
    'as' => 'directory/hotspots/create', 
    'uses' => 'Admin\DirectoryController@getHotSpotsCreate'
    ]);
Route::get('directory/hotspots/edit/{hotspots_id}', [
    'as' => 'directory/hotspots/edit', 
    'uses' => 'Admin\DirectoryController@getHotSpotsEdit'
    ]);
Route::post('directory/hotspots/create', [
    'as' => 'directory/hotspots/create', 
    'uses' => 'Admin\DirectoryController@postHotSpotsCreate'
    ]);
Route::post('directory/hotspots/delete', [
    'as' => 'directory/hotspots/delete', 
    'uses' => 'Admin\DirectoryController@postHotSpotsDelete'
    ]);
Route::get('directory/deals/list', [
    'as' => 'directory/deals/list', 
    'uses' => 'Admin\DirectoryController@getDeals'
    ]);
Route::get('directory/deals/create', [
    'as' => 'directory/deals/create', 
    'uses' => 'Admin\DirectoryController@getDealsCreate'
    ]);
Route::get('directory/deals/edit/{deal_id}', [
    'as' => 'directory/deals/edit', 
    'uses' => 'Admin\DirectoryController@getDealsEdit'
    ]);
Route::post('directory/deals/create', [
    'as' => 'directory/deals/create', 
    'uses' => 'Admin\DirectoryController@postDealsCreate'
    ]);
Route::post('directory/deals/delete', [
    'as' => 'directory/deals/delete', 
    'uses' => 'Admin\DirectoryController@postDealsDelete'
    ]);

Route::get('directory/service/list', [
    'as' => 'directory/service/list', 
    'uses' => 'Admin\DirectoryController@getServices'
    ]);
Route::get('directory/service/create', [
    'as' => 'directory/service/create', 
    'uses' => 'Admin\DirectoryController@getServicesCreate'
    ]);
Route::get('directory/service/edit/{service_id}', [
    'as' => 'directory/service/edit', 
    'uses' => 'Admin\DirectoryController@getServicesEdit'
    ]);

Route::post('directory/service/create', [
    'as' => 'directory/service/create', 
    'uses' => 'Admin\DirectoryController@postServicesCreate'
    ]);
Route::post('directory/service/delete', [
    'as' => 'directory/service/delete', 
    'uses' => 'Admin\DirectoryController@postServicesDelete'
    ]);

Route::get('directory/requests/list', [
    'as' => 'directory/requests/list', 
    'uses' => 'Admin\DirectoryController@getRequests'
    ]);
Route::get('directory/request/status', [
    'as' => 'directory/request/status', 
    'uses' => 'Admin\DirectoryController@getRequestStatus'
    ]);
Route::get('directory/service/create', [
    'as' => 'directory/service/create', 
    'uses' => 'Admin\DirectoryController@getServiceCreate'
    ]);
Route::post('directory/service/action', [
    'as' => 'directory/service/action', 
    'uses' => 'Admin\DirectoryController@postServiceAction'
    ]);
Route::post('directory/Service/Index', [
    'as' => 'directory/Service/Index', 
    'uses' => 'Admin\DirectoryController@postServiceIndex'
    ]);

Route::get('directory/faq/list', [
    'as' => 'directory/faq/list', 
    'uses' => 'Admin\DirectoryController@getFaqsList'
    ]);
Route::post('directory/faq/create', [
    'as' => 'directory/faq/create', 
    'uses' => 'Admin\DirectoryController@postFaqCreate'
    ]);

Route::get('directory/menu/list', [
    'as' => 'directory/menu/list', 
    'uses' => 'Admin\DirectoryController@getNavigationMenuList'
    ]);

Route::get('directory/navigation', [
    'as' => 'directory/navigation', 
    'uses' => 'Admin\DirectoryController@getNavigationMenuList'
    ]);
Route::get('directory/navigation/create', [
    'as' => 'directory/navigation/create', 
    'uses' => 'Admin\DirectoryController@getNavigationMenuCreate'
    ]);
Route::get('directory/navigation/edit/{menu_id}', [
    'as' => 'directory/navigation/edit', 
    'uses' => 'Admin\DirectoryController@getNavigationMenuEdit'
    ]);
Route::post('directory/navigation/create', [
    'as' => 'directory/navigation/create', 
    'uses' => 'Admin\DirectoryController@postNavigationMenuCreate'
    ]);
Route::post('directory/navigation/delete', [
    'as' => 'directory/navigation/delete', 
    'uses' => 'Admin\DirectoryController@postNavigationMenuDelete'
    ]);

Route::post('directory/faq/delete', [
    'as' => 'directory/faq/delete', 
    'uses' => 'Admin\DirectoryController@postFaqDelete'
    ]);
Route::get('directory/gallery/list', [
    'as' => 'directory/gallery/list', 
    'uses' => 'Admin\DirectoryController@getGalleryCreate'
    ]);
Route::get('directory/gallery/create', [
    'as' => 'directory/gallery/create', 
    'uses' => 'Admin\DirectoryController@getGalleryCreate'
    ]);
Route::post('directory/gallery/create', [
    'as' => 'directory/gallery/create', 
    'uses' => 'Admin\DirectoryController@postGalleryCreate'
    ]);
Route::get('directory/floor/list', [
    'as' => 'directory/floor/list', 
    'uses' => 'Admin\DirectoryController@getFloorPlanCreate'
    ]);
Route::get('directory/floor/create', [
    'as' => 'directory/floor/create', 
    'uses' => 'Admin\DirectoryController@getFloorPlanCreate'
    ]);
Route::post('directory/floor/create', [
    'as' => 'directory/floor/create', 
    'uses' => 'Admin\DirectoryController@postFloorPlanCreate'
    ]);
Route::post('directory/floor/delete', [
    'as' => 'directory/floor/delete', 
    'uses' => 'Admin\DirectoryController@postFloorPlanDelete'
    ]);
Route::get('directory/redirect/{hotel_slug}', [
    'as' => 'directory/redirect', 
    'uses' => 'Admin\DirectoryController@getRedirect'
    ]);

    Route::get('{hotel_slug}',[
        'as' => 'directory', 
        'uses' => 'Directory\DirectoryFrontendController@getDirectory'
        ]);
    Route::get('{hotel_slug}/directory/requests',[
        'as' => 'directory/requests', 
        'uses' => 'Directory\DirectoryFrontendController@getRequest'
        ]);
    Route::post('{hotel_slug}/directory/requests/create',[
        'as' => 'directory/requests/create', 
        'uses' => 'Directory\DirectoryFrontendController@postRequestCreate'
        ]);
    Route::get('{hotel_slug}/directory/hotspots',[
        'as' => 'directory/hotspots', 
        'uses' => 'Directory\DirectoryFrontendController@getHotSpots'
        ]);
    Route::get('{hotel_slug}/directory/faq',[
        'as' => 'directory/faq', 
        'uses' => 'Directory\DirectoryFrontendController@getFaqs'
        ]);
    Route::post('{hotel_slug}/directory/hotspots/filter', [
        'as' => 'directory/hotspots/filter', 
        'uses' => 'Directory\DirectoryFrontendController@postFilterHotspots'
        ]);
    Route::post('{hotel_slug}/directory/hotspots/info', [
        'as' => 'directory/hotspots/info', 
        'uses' => 'Directory\DirectoryFrontendController@postHotspotsInfoById'
        ]);
    Route::get('{hotel_slug}/directory/floor', [
        'as' => 'directory/floor', 
        'uses' => 'Directory\DirectoryFrontendController@getFloor'
        ]);
    Route::get('{hotel_slug}/directory/deals-tips', [
        'as' => 'directory/deals-tips', 
        'uses' => 'Directory\DirectoryFrontendController@getDealsTips'
        ]);
    Route::get('{hotel_slug}/directory/service', [
        'as' => 'directory/service', 
        'uses' => 'Directory\DirectoryFrontendController@getService'
        ]);
    Route::get('{hotel_slug}/directory/menu', [
        'as' => 'directory/menu', 
        'uses' => 'Directory\DirectoryFrontendController@getMenu'
        ]);
    Route::get('{hotel_slug}/directory/gallery', [
        'as' => 'directory/gallery', 
        'uses' => 'Directory\DirectoryFrontendController@getGallery'
        ]);
    Route::get('{hotel_slug}/directory/contact', [
        'as' => 'directory/contact', 
        'uses' => 'Directory\DirectoryFrontendController@getContacts'
        ]);
    Route::get('{hotel_slug}/directory/pages/{id}', [
        'as' => 'directory/pages', 
        'uses' => 'Directory\DirectoryFrontendController@getPages'
        ]);
