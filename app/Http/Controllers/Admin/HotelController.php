<?php

namespace Mybookings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use Mybookings\Http\Requests;
use Mybookings\Http\Controllers\Controller;
use View;
use Config;
use System;
use Session;
use Response;
use File;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Mybookings\Models\Country;
use Mybookings\Models\Chain;
use Mybookings\Models\Hotel;
use Mybookings\Models\User;
use Mybookings\Models\Profile;
use Mybookings\Models\PageType;
use Mybookings\Models\Pages;
use Mybookings\Models\PageMeta;

class HotelController extends Controller{
  public function __construct(Request $request){
    $this->middleware('auth');
    $data = Sentinel::getUser();
    if($data){
      View::share('user_data',$data);
      View::share('profile_pic',Profile::where('user_id',$data->id)->first());
      View::share('country',Country::lists('country_name', 'country_name')->all());
      if(System::isAdmin()){
        $chain_lists = Chain::all();
        View::share('admin_view_data',$chain_lists);
      }elseif(System::isChainMember()){
        $chains = Chain::currentUserChainData();
        foreach ($chains as $chain) {
          $chainids[] = $chain->chain_id;
        }
        $chain_lists = Chain::whereIn('id',$chainids)->get();
        View::share('chain_view_data',$chain_lists);
        $hotel_data = Hotel::whereIn('chain_id', $chainids)->get();
        if($hotel_data){
          View::share('hotel_view_data',$hotel_data);
        }
      }elseif (System::isHotelMember()) {
       $hotels = Hotel::currentUserHotelData();
       foreach ($hotels as $hotel) {
        $hotelids[] = $hotel->hotel_id;
      }
      $hotel_lists = Hotel::whereIn('id',$hotelids)->get();
      View::share('hotel_view_data',$hotel_lists);   
    }
  }
}

public function getHotelList(){
  $chains = array();
  if(System::isAdmin()){
    $chains = Chain::lists('name', 'id')->all();
    $hotel_lists = Hotel::paginate(Config::get('mybooking.pagination.page_limit'));
  }elseif(System::isChainMember()){
    $chains = Chain::currentUserChainData();
    foreach ($chains as $chain) {
      $chainids[] = $chain->chain_id;
    } 
    $chains = Chain::whereIn('id',$chainids)->lists('name', 'id')->all();
    $hotel_lists = Hotel::whereIn('chain_id',$chainids)->paginate(Config::get('mybooking.pagination.page_limit'));  
  }elseif (System::isHotelMember()) {
   $hotels = Hotel::currentUserHotelData();
   foreach ($hotels as $hotel) {
    $hotelids[] = $hotel->hotel_id;
  }
  $chains = Chain::lists('name', 'id')->all();
  $hotel_lists = Hotel::whereIn('id',$hotelids)->paginate(Config::get('mybooking.pagination.page_limit'));   
}else{
  return view('errors.401');
}
return view('hotel.list')->with(array('chains'=>$chains,'hotel_lists'=>$hotel_lists));
}

public function postHotelSlug(Request $request){
  $hotel_slug_exist = Hotel::where('slug', $request->input('value'))->first();
  if($hotel_slug_exist){
    $result = 'yes';
  }else{
    $result = 'no';
  }
  return response()->json(['response' => 'success', 'result' => $result]);
}

public function getCreateHotelPage(){
  $chains = array();
  if(System::isAdmin()){
    $chains = Chain::lists('name', 'id')->all();
    $zones = Hotel::timezonelist();
    return view('hotel.create')->with(array('chains'=>$chains, 'timezone' => $zones));
  }else{
    return view('errors.401');
  }
}
public function postCreateHotel(Request $request){
  $rules = array(
            'name'                  => 'required',                        // just a normal required validation
            'slug'                  => 'required|unique:hotels,slug,'.$request->input('hotel_id'),                        // just a normal required validation
            'address_1'             => 'required',                        // just a normal required validation
            'postal_code'           => 'required',                        // just a normal required validation
            'city'                  => 'required',                        // just a normal required validation
            'country'               => 'required',                        // just a normal required validation
            'phone'                 => 'required',                        // just a normal required validation
            'website'               => 'required|url',                        // just a normal required validation
            'email'                 => 'required|email',     // required and must be unique in the ducks table
            'social_facebook'       => 'url',     // required and must be unique in the ducks table
            'social_twitter'        => 'url',     // required and must be unique in the ducks table
            'social_instagram'      => 'url',     // required and must be unique in the ducks table
            );
  $validator = Validator::make($request->input(), $rules);
  if($request->input('update')=='update'){
    if ($validator->fails()) {
      $messages = $validator->messages();
      return redirect('hotel/edit/'.$request->input('hotel_id'))
      ->withErrors($validator)->withInput($request->input());
    }else{
      $hotel_info = Hotel::where('id',$request->input('hotel_id'))->first();
      $hotel_info->name  = $request->input('name');
      if($hotel_info->slug  != $request->input('slug')){
        $hotel_info->slug  = $request->input('slug');
      }
      $hotel_info->address_1  = $request->input('address_1');
      $hotel_info->address_2  = $request->input('address_2');
      $hotel_info->chain_id  = $request->input('chain_id');
      $hotel_info->postal_code  = $request->input('postal_code');
      $hotel_info->province  = $request->input('province');
      $hotel_info->city  = $request->input('city');
      $hotel_info->country  = $request->input('country');
      $hotel_info->phone  = $request->input('phone_number');
      $hotel_info->website  = $request->input('website');
      $hotel_info->email  = $request->input('email');
      $hotel_info->status  = $request->input('status');
      $hotel_info->latlong = $this->get_lat_long($request->input('address_1'));
      $hotel_info->timezone = $request->input('timezone');
      $hotel_info->social_facebook = $request->input('social_facebook');
      $hotel_info->social_twitter = $request->input('social_twitter');
      $hotel_info->social_instagram = $request->input('social_instagram');
      if($hotel_info->save()){
       $result = File::makeDirectory(public_path().'/hotelfiles/'.$request->input('hotel_id').'/', 0775, true, true);
       Session::flash('success', 'Hotel successfully updated'); 
       return redirect('hotel/view/'.$request->input('hotel_id'));
     }else{
      return redirect('hotel/view/'.$request->input('hotel_id'));
    }
  }
}else{
  if ($validator->fails()) {
    $messages = $validator->messages();
    return redirect('create/hotel')
    ->withErrors($validator)->withInput($request->input());
  }else{
    $create_hotel = new Hotel();
    $create_hotel->name = $request->input('name');
    $create_hotel->slug = $request->input('slug');
    $create_hotel->address_1 = $request->input('address_1');
    $create_hotel->address_2 = $request->input('address_2');
    $create_hotel->postal_code = $request->input('postal_code');
    $create_hotel->province = $request->input('province');
    $create_hotel->city = $request->input('city');
    $create_hotel->country = $request->input('country');
    $create_hotel->phone = $request->input('phone_number');
    $create_hotel->website = $request->input('website');
    $create_hotel->email = $request->input('email');
    $create_hotel->latlong = $this->get_lat_long($request->input('address_1'));
    $create_hotel->timezone = $request->input('timezone');
    $create_hotel->social_facebook = $request->input('social_facebook');
    $create_hotel->social_twitter = $request->input('social_twitter');
    $create_hotel->social_instagram = $request->input('social_instagram');
    if($request->input('as_a_chain')!=null){
      $chain = new Chain();
      $chain->name = $request->input('name');
      $chain->address_1 = $request->input('address_1');
      $chain->address_2 = $request->input('address_2');
      $chain->address_2 = $request->input('address_2');
      $chain->postal_code = $request->input('postal_code');
      $chain->province = $request->input('province');
      $chain->city = $request->input('city');
      $chain->country = $request->input('country');
      $chain->phone = $request->input('phone_number');
      $chain->website = $request->input('website');
      $chain->email = $request->input('email');
      $chain->save();
    }
    if($request->has('chain_id')){
      $chain_id = $request->input('chain_id');
    }else{
      $chain_id = $chain['id'];
    }
    $create_hotel->chain_id = $chain_id;
    $create_hotel->save();
    
    $result = File::makeDirectory(public_path().'/hotelfiles/'.$create_hotel['id'].'/'.'images'.'/', 0775, true, true);
    Session::flash('success', 'Hotel successfully created');
    return redirect('hotel/list');
  }
}

}

public function getHotelInfoById($hotel_id,Request $request){
 $hotel_info = Hotel::where('id',$hotel_id)->first();
 if(!$hotel_info){
   $messages = array(
    'chain_error'=>"Oops! Hotel not found"
    );
   return redirect('hotel/list')->withErrors($messages);
 }
 $hotel_data = $this->getHotelByRole($hotel_id);
 if($hotel_data['flag']){
  // $request->session()->put('hotel', $hotel_info);
  return view('hotel.view')->with(array('hotel_info'=>$hotel_info,'user_list'=>$hotel_data['user_list'],'chains'=>$hotel_data['chains']));
}else{
  return view('errors.401');
}
}

public function getEditHotelById($hotel_id,Request $request){
 $hotel_info = Hotel::where('id',$hotel_id)->first();
 if(!$hotel_info){
   $messages = array(
    'chain_error'=>"Oops! Hotel not found"
    );
   return redirect('hotel/list')->withErrors($messages);
 } 
 $hotel_data = $this->getHotelByRole($hotel_id);
 if($hotel_data['flag']){
  // $request->session()->put('hotel', $hotel_info);
  $zones = Hotel::timezonelist();
  return view('hotel.edit')->with(array('timezone'=>$zones, 'hotel_info'=>$hotel_info, 
    'user_list'=>$hotel_data['user_list'],'chains'=>$hotel_data['chains']));
}else{
  return view('errors.401');
}
}

public function postDeleteHotel(Request $request){
  $hotel_id = $request->input('hotel_id');
  $hotel_info = Hotel::where('id',$hotel_id)->first();
  if($hotel_info){
    $hotel_info->hotels()->delete();
    $hotel_info->delete();
  }
  $result = File::Delete(public_path().'/hotelfiles/'.$hotel_id.'/'.'images'.'/');
  return response()->json(['response' => 'success']);
}

public function getHotelManagePagesByHotelId($id,Request $request){
  $hotel_info = Hotel::where('id',$id)->first();
  if(!$hotel_info){
   $messages = array('chain_error'=>"Oops! Hotel not found");
   return redirect('hotel/list')->withErrors($messages);
 }else{
   $hotel_data = $this->getHotelByRole($id);
   if($hotel_data['flag']){
    $request->session()->put('hotel', $hotel_info);
    return view('hotel.managepages');
  }else{
    return view('errors.401');
  }
}
}
private function get_lat_long($address){
  $address = str_replace(" ", "+", $address);
  $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false");
  $json = json_decode($json);
  $lat = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'};
  $long = $json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'};
  return $lat.','.$long;
}

private function getHotelByRole($hotel_id){
  $hotel_data = array();
  $users = array();
  $hotel_data['flag'] = true;
  $hotel_data['user_list'] = array();
  $hotel_data['chains'] = array();
  $hotel_info = Hotel::where('id',$hotel_id)->first();
  foreach ($hotel_info->hotels as $hotel) {
    $users[] = $hotel->user->id;
  }
  $hotel_data['user_list'] = User::whereNotIn('id', $users)->lists('first_name','id')->all();
  if(System::isAdmin()){
    $hotel_data['chains'] = Chain::lists('name','id')->all();
  }elseif(System::isChainMember()){
    $chaindata = Chain::currentUserChainData();
    foreach ($chaindata as $chain) {
      $chainids[] = $chain->chain_id;
    }
    if (!in_array($hotel_info->chain_id, $chainids)) {
     $hotel_data['flag'] = false;
   } 
   $hotel_data['chains'] = Chain::whereIn('id',$chainids)->lists('name','id')->all();
 }elseif(System::isHotelMember()){
  $hotels = Hotel::currentUserHotelData();
  foreach ($hotels as $hotel) {
    $hotelids[] = $hotel->hotel_id;
  }
  if (!in_array($hotel_id, $hotelids)) {
   $hotel_data['flag'] = false;
 } 
 $hotel_data['chains'] = Chain::where('id',$hotel_info->chain_id)->lists('name','id')->all();
}else{
 $hotel_data['flag'] = false;
}
return $hotel_data;
}

}
