<?php

namespace Mybookings\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Mybookings\Http\Requests;
use Mybookings\Http\Controllers\Controller;
use View;
use URL;
use Config;
use System;
use Session;
use Response;
use Intervention\Image\Facades\Image as Image;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Mybookings\Models\Country;
use Mybookings\Models\Chain;
use Mybookings\Models\Hotel;
use Mybookings\Models\User;
use Mybookings\Models\Profile;
use Mybookings\Models\PageType;
use Mybookings\Models\DirectorySetting;
use Mybookings\Models\Pages;
use Mybookings\Models\PageMeta;
use Mybookings\Models\FaqPage;
use Mybookings\Models\FloorPlan;
use Mybookings\Models\Hotspots;
use Mybookings\Models\Deals;
use Mybookings\Models\Menu;
use Mybookings\Models\HotelRequest;
use Mybookings\Models\NavigationMenu;


class DirectoryController extends Controller{
  public function __construct(Request $request){
    $this->middleware('auth', ['except' => ['getDirectory','getRequest']]);
    $data = Sentinel::getUser();
    if($data){
      View::share('user_data',$data);
      View::share('profile_pic',Profile::where('user_id',$data->id)->first());
      View::share('country',Country::lists('country_name', 'country_name')->all());
      if(System::isAdmin()){
        $chain_lists = Chain::all();
        View::share('admin_view_data',$chain_lists);
      }elseif(System::isChainMember()){
        $chains = Chain::currentUserChainData();
        foreach ($chains as $chain) {
          $chainids[] = $chain->chain_id;
        }
        $chain_lists = Chain::whereIn('id',$chainids)->get();
        View::share('chain_view_data',$chain_lists);
      }elseif (System::isHotelMember()) {
       $hotels = Hotel::currentUserHotelData();
       foreach ($hotels as $hotel) {
        $hotelids[] = $hotel->hotel_id;
      }
      $hotel_lists = Hotel::whereIn('id',$hotelids)->get();
      View::share('hotel_view_data',$hotel_lists);   
    }
  }
}
public function getPageType(){
  $pages_data = array();
  $pages_type = PageType::get();
  foreach ($pages_type as $page_type) {
    $pages_data[] = array(
      'id' => $page_type->id,
      'name' => $page_type->name,
      'icon' => $page_type->icon,
      'action' => route('directory/'.$page_type->slug.'/list')
      );
  }
  $result = $pages_data;
  return $result;
}

public function postPagesListByHotelId(Request $request){
  $pages_data = [];
  $pages_details = Pages::where('hotel_id', Session::get('hotel')['id'])->where('is_published', 1)->get();
  foreach ($pages_details as $pages_detail) {
    $pages_data[$pages_detail->slug] = $pages_detail->title;
  }
  return $pages_data;
}

public function getSetting(){
  $directory_data = DirectorySetting::where('hotel_id', Session::get('hotel')['id'])->first();
  return view('directory.setting')->with(array('directory_data' => $directory_data));
} 


public function getHotspots(){
  $hotspots_details = Hotspots::where('hotel_id', Session::get('hotel')['id'])->get();
  $hotsposts_category = Hotspots::getAllHotspotsCategory();
  return view('directory.hotspots.list')->with(array('hotspots_details' => $hotspots_details)); 
}

public function getHotSpotsEdit($hotspots_id){
  $hotspots_data = Hotspots::where('id', $hotspots_id)
  ->where('hotel_id', Session::get('hotel')['id'])
  ->first();
  if($hotspots_data){
    $hotsposts_category = Hotspots::getAllHotspotsCategory();
    return view('directory.hotspots.create')->with(array('hotspots_data' => $hotspots_data,'hotsposts_category' => $hotsposts_category)); 
  }else{
   $messages = array('error'=>"Oops! Hotspots not found");
   return redirect('directory/hotspots/list')->withErrors($messages);
 }                
}

public function getHotSpotsCreate(){
  $hotsposts_category = Hotspots::getAllHotspotsCategory();
  return view('directory.hotspots.create')->with(array('hotsposts_category' => $hotsposts_category));
}

public function postHotSpotsCreate(Request $request){
  if($request->has('hotspots_id')){
    $hotspots = Hotspots::where('id', $request->input('hotspots_id'))->first();
    $message = 'Successfully Updated';
  }else{
    $hotspots = new Hotspots();
    $message = 'Successfully Saved';
  }
  $hotspots->hotel_id = Session::get('hotel')['id'];
  $hotspots->title = $request->input('title');
  $hotspots->address = $request->input('address');
  $hotspots->latlong = $request->input('latlong');
  
  $hotspots->description = $request->input('description');
  $hotspots->website =  $request->input('website');
  $hotspots->category = $request->input('category');
  $hotspots->phone = $request->input('phone');
  $hotspots->show_as_hotspots = ($request->input('show_as_hotspot') == 'on')?1:0;
  $hotspots->is_published = ($request->input('is_published') == 'on')? 1 : 0 ;
  $hotspots->save();
  Session::flash('success',  $message);
  return redirect('directory/hotspots/list');
}

public function postHotSpotsDelete(Request $request){
  $faq_page = Hotspots::where('id', $request->input('hotspots_id'))->delete();
  return response()->json(['response' => 'success']);
}

public function getDeals(){
  $deals_data = Deals::where('hotel_id', Session::get('hotel')['id'])->get();
  return view('directory.deals.list')->with(array('deals_data' => $deals_data));
}

public function getDealsEdit($deal_id){
  $deals_data = Deals::where('id', $deal_id)->where('hotel_id', Session::get('hotel')['id'])->first();
  if($deals_data){
    return view('directory.deals.create')->with(array('deals_data' => $deals_data));
  }else{
   $messages = array('error'=>"Oops! Deal not found");
   return redirect('directory/deals/list')->withErrors($messages);
 }
}

public function getDealsCreate(){
  return view('directory.deals.create');
}

public function getRedirect($hotel_slug){
  return redirect('//'.Config::get('app.url').'/'.$hotel_slug);
}

public function postDealsCreate(Request $request){
  if ($request->has('deal_id')) {
    $deals = Deals::where('id', $request->input('deal_id'))->first();
    if($request->has('image_crop_path')){
      $image_name = $this->uploadFileThumbnail($request->input('image_crop_path'),$request->input('img_crop'));
    }else{
      $image_name = $deals->image;
    }
    $message = 'Successfully Updated';
  }else{
   $deals = new Deals();
   if($request->has('image_crop_path')){
    $image_name = $this->uploadFileThumbnail($request->input('image_crop_path'),$request->input('img_crop'));
  }else{
    $image_name = '';
  }
    $message = 'Successfully Saved';
    }

    $deals->hotel_id = Session::get('hotel')['id'];
    $deals->title = $request->input('title');
    $deals->short_description = $request->input('short_desc');
    $deals->full_description = $request->input('full_desc');
    $deals->image = $image_name;
    $deals->currency = $request->input('currency');
    $deals->price = $request->input('price');
    $deals->type = $request->input('tip_deal');
    $deals->show_on_home =($request->input('show_on_home') == 'on')?1:0;
    $deals->is_published = ($request->input('is_published') == 'on')?1:0;
    $deals->save();
    Session::flash('success', $message);
    return redirect('directory/deals/list'); 
}

public function postDealsDelete(Request $request){
  $delete_deal = Deals::where('id', $request->input('deal_id'))->delete();
  return response()->json(['response' => 'success']);
}

public function uploadFileThumbnail($file, $crop_size){
  $crop_size = json_decode($crop_size, true);
  $image = explode(';', $file);
  $image_type = explode('/', $image[0]);
  $image_path = explode(',', $image[1]);
  if(!empty($file)) {
    $destinationPath = public_path() . '/hotelfiles/'.Session::get('hotel')['id'].'/'.'images'.'/';
    $data = base64_decode($image_path[1]);
    $filename = date('ymdhis').'.'.$image_type[1];
    $file = $destinationPath . $filename;
    $success = file_put_contents($file, $data);

    $returnData = 'uploads/' . $filename;
    $image = Image::make(file_get_contents($destinationPath. $filename))->save($destinationPath . $filename);
    return  $filename;
  }    
}

public function getRequests(){
  $hotel_id = Session::get('hotel')['id'];
  $hotel = Hotel::where('id', $hotel_id)->first();
  if($hotel->timezone){
    $timezone = $hotel->timezone;
  }else{
    $timezone = 'UTC';
  }
  $request_details = HotelRequest::getRequestDetailsByHotelId($hotel_id, $timezone);
  return view('directory.request.list')->with(array('request_details' => toObject($request_details)));
}

public function getServices(){
  $menu_details = Menu::where('hotel_id', Session::get('hotel')['id'])->orderBy('index')->get();
  return view('directory.service.list')->with(array('menu_details' =>$menu_details));
}
public function getServiceCreate(){
 $category = ['Breakfast','Lunch','Dinner','Drinks','Room Service','Services','Other'];
 return view('directory.service.create')->with(array('category_data' => $category));
}
public function getServicesEdit($service_id){
  $menu_service_detail = Menu::where('id', $service_id)->where('hotel_id', Session::get('hotel')['id'])->first();
  if($menu_service_detail){
    return view('directory.service.create')->with(array('menu_detail' => $menu_service_detail));
  }else{
    $messages = array('error'=>"Oops! page not found");
    return redirect('directory/service/list')->withErrors($messages);
  }
}

public function postServiceIndex(Request $request){
  $index_data = toObject($request->input('data'));
  foreach ($index_data as $data) {
    if($data != ''){
     $service_menu = Menu::where('id', $data->id)->where('category', $data->category)->first();
     $service_menu->index = $data->value;
     $service_menu->save();
   }
 }
 return response()->json(['response' => 'success']);
}

public function postServicesDelete(Request $request){
  $menu_service = Menu::where('id', $request->input('menu_id'))->delete();
  return response()->json(['response' => 'success']);
}
public function getFaqsList(){
  $faq_page_data = FaqPage::where('hotel_id', Session::get('hotel')['id'])->get();
  return view('directory.faq.create')->with(array('faq_page_data' => $faq_page_data));
}
public function postServiceAction(Request $request){
  $request_data = HotelRequest::where('id', $request->input('request_id'))->first();
  $request_data->status = $request->input('action');
  $request_data->save();
  return response()->json(['response' => 'success']);
}
public function postFaqCreate(Request $request){
  if($request->has('faq_id')){
    $faq_page = FaqPage::where('id', $request->input('faq_id'))->first();
    $message = 'Successfully Saved';
  }else{
    $faq_page = new FaqPage();
    $message = 'Successfully Updated';
  }
  $faq_page->hotel_id = Session::get('hotel')['id'];
  $faq_page->question = $request->input('question');
  $faq_page->answer = $request->input('answer');
  $faq_page->save();
  Session::flash('success', $message);
  return redirect()->back();
}

public function postFaqDelete(Request $request){
  $faq_page = FaqPage::where('id', $request->input('faq_id'))->delete();
  return response()->json(['response' => 'success']);
}
public function getNavigationMenuList(){
  $navigation_menu = NavigationMenu::where('hotel_id', Session::get('hotel')['id'])->get();
  return view('directory.navigation.list')->with(array('navigation_menu_data' => $navigation_menu));
}
public function getNavigationMenuCreate(){
  return view('directory.navigation.create');
}
public function postNavigationMenuCreate(Request $request){
  if($request->has('navigation_menu_id')){
    $navigation_menu =  NavigationMenu::where('id', $request->input('navigation_menu_id'))->first();
    $message = 'Successfully Updated';
  }else{
    $navigation_menu = new NavigationMenu();
    $message = 'Successfully Saved';
  }
  $navigation_menu->hotel_id = Session::get('hotel')['id'];
  $navigation_menu->title = $request->input('title');
  $navigation_menu->slug = $this->slugify($request->input('title'));
  $navigation_menu->page_type = $request->input('page_type');
  $navigation_menu->category = json_encode($request->input('category'));
  $navigation_menu->icon = $request->input('icon_name');
  $navigation_menu->is_published = ($request->input('is_published') == 'on')?1:0;
  $navigation_menu->save();
  Session::flash('success', $message);
  return redirect('directory/navigation');
}
public function getNavigationMenuEdit($menu_id){
  $navigation_menu_deatail = NavigationMenu::where('id', $menu_id)->where('hotel_id', Session::get('hotel')['id'])->first();
  if(isset($navigation_menu_deatail)){
    $navigation_menu_deatail['pankaj'] = json_decode($navigation_menu_deatail->category);
  }
  return view('directory.navigation.create')->with(array('navigation_menu_deatail' => $navigation_menu_deatail));
}
public function postNavigationMenuDelete(Request $request){
  NavigationMenu::where('id', $request->input('navigation_menu_id'))->delete();
  return response()->json(['response' => 'success']);
}
public function getMenuEdit($menu_id){
  $menu_detail = Menu::where('id', $menu_id)->first();
  return view('menu.create')->with(array('menu_detail' => $menu_detail));
}

public function postServicesCreate(Request $request){
 if ($request->has('menu_id')) {
  $menu = Menu::where('id', $request->input('menu_id'))->first();
  $message = 'Successfully Updated';
}else{
  $menu = new Menu();
  $message = 'Successfully Saved';
}
$menu->hotel_id = Session::get('hotel')['id'];
$menu->title = $request->input('title');
$menu->sub_text = $request->input('sub_text');
$menu->currency = $request->input('currency');
$menu->price = $request->input('price');
$menu->category = $request->input('category');
$menu->is_published = ($request->input('is_published') == 'on')?1:0;
$menu->save();
Session::flash('success', $message);
return redirect('directory/service/list');
}
public function getGalleryCreate(){
 return view('directory.gallery.create');
}
public function getFloorPlanCreate(){
  $floor_plan_data = FloorPlan::where('hotel_id', Session::get('hotel')['id'])->get();
  return view('directory.floor.list')->with(array('floor_plan_data' => $floor_plan_data));
}
public function postFloorPlanCreate(Request $request){
  if ($request->has('floor_plan_id')) {
    $floor_plan =FloorPlan::where('id', $request->input('floor_plan_id'))->first();
    $message = 'Successfully Updated';
  }else{
    $floor_plan = new FloorPlan();
    $message = 'Successfully Saved';
  }
  $floor_plan->hotel_id = Session::get('hotel')['id'];
  $floor_plan->title = $request->input('title');
  if($request->hasfile('image')){
    $image = $request->file('image');
    $filename = strtotime('now') . $image->getClientOriginalName();
    $is_saved = $request->file('image')->move(public_path('hotelfiles/'.Session::get('hotel')['id'].'/'.'images'.'/'), $filename); 
    $floor_plan->image =  $filename;
  }
  $floor_plan->save();
  Session::flash('success', $message);
  return redirect()->back();
}
public function postFloorPlanDelete(Request $request){
  $floor_plan = FloorPlan::where('id', $request->input('floor_plan_id'))->delete();
  return response()->json(['response' => 'success']);
}
public function getEditHotelPage($page_id){
  $page_data = Pages::where('id', $page_id)->where('hotel_id', Session::get('hotel')['id'])->first();
  if($page_data){
    return view('directory.pages.create')->with(array('page_data' => $page_data));
  }else{
   $messages = array('error'=>"Oops! page not found");
   return redirect('directory/pages/list')->withErrors($messages); 
 }
}
public function getPagesList(){
  $pages_details = Pages::where('hotel_id', Session::get('hotel')['id'])->get();
  return view('directory.pages.list')->with(array('pages_details' => $pages_details)); 
}

public function getCreateHotelPages(){
 return view('directory.pages.create'); 
}
public function postPageCreate(Request $request){
  if($request->has('page_id')){
    $page =Pages::where('id', $request->input('page_id'))->first();
    $message = 'Successfully Updated';
  }else{
    $page = new Pages();
    $message = 'Successfully Saved';
  }
  $slug = $this->slugify($request->input('title'));
  $page->hotel_id = Session::get('hotel')['id'];
  $page->title = $request->input('title');
  $page->description = $request->input('description');
  $page->slug = $slug;
  $page->is_published = ($request->input('is_published') == 'on')? 1: 0;
  $page->page_type_id = 7;
  $page->save();
  Session::flash('success', $message);
  return redirect('directory/pages/list');
}
public function postPageDelete(Request $request){
  $page_delete = Pages::where('id', $request->input('page_id'))->delete();
  return response()->json(['response' => 'success']);
}

public function postGalleryCreate(Request $request){

}
public function getDirectorySetting(){
  return view('hotel.hoteldirectory');
}

private function insertPageMeta($id, $key, $value){
  $page_meta = new PageMeta();
  $page_meta->page_id = $id;
  $page_meta->key = $key;
  $page_meta->value = $value;
  $page_meta->save();
}
private function slugify($text){ 
  $text = preg_replace('~[^\\pL\d]+~u', '-', $text);
  $text = trim($text, '-');
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = strtolower($text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  if (empty($text)){
    return 'n-a';
  }
  return $text;
}
public function postSetting(Request $request){
  if ($request->has('directroy_setting_id')) {
    $directory_setting = DirectorySetting::where('hotel_id', Session::get('hotel')['id'])->first();
    $message = 'Directory Setting Updated successfully';
  }else{
    $directory_setting = new DirectorySetting();
    $message = 'Directory Setting Saved successfully';
  }
  $directory_setting->hotel_id = Session::get('hotel')['id'];
  $directory_setting->title_color = $request->input('title_color');
  $directory_setting->text_color = $request->input('text_color');
  $directory_setting->text_link_color = $request->input('text_link_color');
  $directory_setting->text_link_hover_color = $request->input('text_link_hover_color');
  $directory_setting->icon_color = $request->input('icon_color');
  $directory_setting->button_color = $request->input('button_color');
  $directory_setting->button_hover_color = $request->input('button_hover_color');
  $directory_setting->intro_text = $request->input('intro_text');
  $directory_setting->button_text_color = $request->input('button_text_color');
  $directory_setting->button_text_hover_color = $request->input('button_text_hover_color');
  $directory_setting->include_script = $request->input('include_script');
  $directory_setting->include_css = $request->input('include_css');
  $directory_setting->sub_heading = $request->input('sub_heading');
  $directory_setting->sub_heading_color = $request->input('sub_heading_color');
  $directory_setting->privacy_statement = $request->input('privacy_statement');
  $directory_setting->save();
  Session::flash('success', $message);
  return redirect('directory/setting');
}
}
