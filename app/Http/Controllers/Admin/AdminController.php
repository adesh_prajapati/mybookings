<?php

namespace Mybookings\Http\Controllers\Admin;
use View;
use Config;
use Session;
use System;
use Illuminate\Http\Request;
use Validator;
use Mybookings\Http\Requests;
use Mybookings\Http\Controllers\Controller;
use Mybookings\Http\Middleware\SentinelAuth;
use Cartalyst\Sentinel\Users\UserInterface;
use Intervention\Image\Facades\Image as Image;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Illuminate\Database\QueryException;
use Mybookings\Models\User;
use Mybookings\Models\Chain;
use Mybookings\Models\Hotel;
use Mybookings\Models\Profile;
use Mybookings\Models\HotelMember;
use Mybookings\Models\ChainMember;
class AdminController extends Controller{
    
    public function __construct(){
        $this->middleware('auth');
        $data = Sentinel::getUser();
        if($data){
            View::share('user_data',User::where('id',$data->id)->first());
            View::share('profile_pic',Profile::where('user_id',$data->id)->first());
            if(System::isAdmin()){
              $chain_lists = Chain::all();
              View::share('admin_view_data',$chain_lists);
          }elseif(System::isChainMember()){
              $chains = Chain::currentUserChainData();
              foreach ($chains as $chain) {
                  $chainids[] = $chain->chain_id;
              }
              $chain_lists = Chain::whereIn('id',$chainids)->get();
              View::share('chain_view_data',$chain_lists);
          }elseif (System::isHotelMember()) {
             $hotels = Hotel::currentUserHotelData();
             foreach ($hotels as $hotel) {
                $hotelids[] = $hotel->hotel_id;
            }
            $hotel_lists = Hotel::whereIn('id',$hotelids)->get();
            View::share('hotel_view_data',$hotel_lists);   
        }
        }
    }
    
    public function getDashboardPage(){
        return view('admin.dashboard');
    }
    public function getUsers(){
        $roles = Sentinel::getRoleRepository()->createModel()->select(['id', 'name','slug'])->get();
        $user_data = array();
        $user_list = User::paginate(Config::get('mybooking.pagination.page_limit'));
        return view('admin.users')->with(array('user_list'=>$user_list, 'roles' => $roles));
    }
    public function postCreateUser(Request $request){
        // $role_data = Sentinel::findRoleById($request->input('role'));
        $role_data = Sentinel::findRoleBySlug('user');
        if($request->input('submit')=='create'){
            $rules = array(
            'first_name'             => 'required',                        // just a normal required validation
            'last_name'             => 'required',                        // just a normal required validation
            'password'             => 'required|min:5',                        // just a normal required validation
            'email'            => 'required|email|unique:users',     // required and must be unique in the ducks table
            );
            $validator = Validator::make($request->input(), $rules);
            if ($validator->fails()) {
                $messages = $validator->messages();
                return redirect()->back()->withErrors($validator)->withInput($request->input());
            }else{
              $credentials = [
              'email'    => $request->input('email'),
              'password' => $request->input('password'),
              'first_name'=>$request->input('first_name'),
              'last_name'=>$request->input('last_name'),
              'permissions'=>[$role_data->slug=>true],
              ];
              $user = Sentinel::registerAndActivate($credentials);
              if($user){
                $profile = new  Profile();
                $profile->user_id = $user->id;
                $profile->phone = $request->input('phone');
                $profile->save();
                $role = Sentinel::findRoleBySlug('user');
                $role->users()->attach($user);
            }
            Session::flash('success', 'User successfully created');
            return redirect('users');
        }
    }else{
        $email_used = User::where('email',$request->input('email'))->first();
        $user = Sentinel::findById($request->input('user_id'));
        $credentials = array();
        // $credentials['permissions'] = [$role_data->slug=>true];
        if(!$email_used){
            if($request->input('password')!=''){
                $credentials['password'] = $request->input('password');
            }
            $credentials['email'] = $request->input('email');
            $credentials['first_name'] = $request->input('first_name');
            $credentials['last_name'] = $request->input('last_name');
        }else{
            if($request->input('password')!=''){
                $credentials['password'] = $request->input('password');
            }
            $credentials['first_name'] = $request->input('first_name');
            $credentials['last_name'] = $request->input('last_name');
        }
        $user = Sentinel::update($user, $credentials);
        $user_data = User::where('id',$request->input('user_id'))->first();
        if($user){
            // $role_remove = Sentinel::findRoleByName($user->roles[0]['name']);
            // $role_remove->users()->detach($user);
            // $role_add = Sentinel::findRoleById($request->input('role'));
            // $role_add->users()->attach($user);
            $user_data->profile->phone = $request->input('phone');
            $user_data->profile->save();
        }
         Session::flash('success', 'User successfully updated');
        return redirect('users');
    }
}

public function getUserInfo(Request $request){
    $user = User::where('id',$request->input('user_id'))->first();
    $user_role = $user->getRoles();
    $user_data = array(
        "first_name" => $user->first_name,
        "user_id" => $user->id,
        "last_name" => $user->last_name,
        "phone" => $user->profile->phone,
        "email" => $user->email,
        "role_id" => $user_role[0]['id'],
        );
    return response()->json(['user_data'=> $user_data]);
}

public function postUpdateUserInfo(Request $request){
    $image_name = $this->uploadFileThumbnail($request->input('image_crop_path'),$request->input('img_crop'));
    $user = Sentinel::getUser();
    $profile = Profile::where('user_id', $user->id)->first();
    $profile->image = $image_name;
    $profile->save();
     Session::flash('success', 'successfully Saved');
    return redirect()->back();
}
public function uploadFileThumbnail($file, $crop_size){
  $crop_size = json_decode($crop_size, true);
  $image = explode(';', $file);
  $image_type = explode('/', $image[0]);
  $image_path = explode(',', $image[1]);
  if(!empty($file)) {
    $destinationPath = public_path() . '/uploads/';
    $data = base64_decode($image_path[1]);
    $filename = date('ymdhis') . '_croppedImage' . '.'.$image_type[1];
    $file = $destinationPath . $filename;
    $success = file_put_contents($file, $data);

    $returnData = 'uploads/' . $filename;
    $image = Image::make(file_get_contents($destinationPath. $filename));
    $image->resize(48, 48)->save($destinationPath . $filename);
    return  $filename;
  }    
}
public function postUserDelete(Request $request){
    $user = User::where('id',$request->input('user_id'))->first();
    if($user){
        $user->currentchainmember()->delete();
        $user->currenthotelmember()->delete();
        $user->profile->delete();
        $user->delete();
        Session::flash('success', '');
        return response()->json(['success'=> 'deleted']);
    }else{
        return response()->json(['success'=> 'error']);
    }

}
public function getAssignUser(Request $request){
    if($request->input('submit_role')=='hotel'){
        $role_data = Sentinel::findRoleBySlug('hotel_managers');
            $hotelmember = new  HotelMember();
            $hotelmember->user_id = $request->input('user_id');
            $hotelmember->hotel_id = $request->input('entity_id');
            $hotelmember->role_id = $role_data->id;
            $hotelmember->save();
            Session::flash('success', 'User was successfully added to Hotel');
            return redirect()->back();
    }elseif ($request->input('submit_role')=='chain') {
        $role_data = Sentinel::findRoleBySlug('chain_managers');
        $chainmember = new  ChainMember();
        $chainmember->user_id = $request->input('user_id');
        $chainmember->chain_id = $request->input('entity_id');
        $chainmember->role_id = $role_data->id;
        $chainmember->save();
        Session::flash('success', 'User was successfully added to Chain');
        return redirect()->back();
    }
    }

    public function getRemoveAccessUser(Request $request){
        if($request->input('entity_type')=='hotel'){
            $hotel_member = HotelMember::where('user_id',$request->input('user_id'))->where('role_id',$request->input('role_id'))->where('hotel_id',$request->input('hotel_id'))->delete();
            if($hotel_member){
                return response()->json(['response' => 'success']);
            }
        }elseif ($request->input('entity_type')=='chain') {
            $chain_member = ChainMember::where('user_id',$request->input('user_id'))->where('role_id',$request->input('role_id'))->where('chain_id',$request->input('chain_id'))->delete();
            if($chain_member){
                return response()->json(['response' => 'success']);
            }
        }else{
            $messages = array(
                'error'=>"Oops! something went wrong"
              );
            return redirect('users')->withErrors($messages);
        }
    }
}
