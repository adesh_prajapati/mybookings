<?php

namespace Mybookings\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Validator;
use Mybookings\Http\Requests;
use Mybookings\Http\Controllers\Controller;
use View;
use App;
use System;
use Config;
use Session;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Mybookings\Models\Country;
use Mybookings\Models\Chain;
use Mybookings\Models\Hotel;
use Mybookings\Models\User;
use Mybookings\Models\Profile;

class ChainController extends Controller{
  public function __construct(){
    $this->middleware('auth');
    $data = Sentinel::getUser();
    if($data){
      View::share('user_data',$data);
      View::share('profile_pic',Profile::where('user_id',$data->id)->first());
      View::share('country',Country::lists('country_name', 'country_name')->all());
      View::share('phone_code',Country::OrderBy('phonecode', 'ASC')
        ->lists('phonecode', 'phonecode')->all());
      if(System::isAdmin()){
        $chain_lists = Chain::all();
        View::share('admin_view_data',$chain_lists);
      }elseif(System::isChainMember()){
        $chains = Chain::currentUserChainData();
        foreach ($chains as $chain) {
          $chainids[] = $chain->chain_id;
        }
        $chain_lists = Chain::whereIn('id',$chainids)->get();
        View::share('chain_view_data',$chain_lists);
      }elseif (System::isHotelMember()) {
       $hotels = Hotel::currentUserHotelData();
       foreach ($hotels as $hotel) {
        $hotelids[] = $hotel->hotel_id;
      }
      $hotel_lists = Hotel::whereIn('id',$hotelids)->get();
      View::share('hotel_view_data',$hotel_lists);   
    }
  }
}

public function getChainList(){
  if(System::isAdmin()){
    $chain_lists = Chain::paginate(Config::get('mybooking.pagination.page_limit'));
    return view('chain.list')->with(array('chain_lists'=>$chain_lists));
  }else{
    if(System::isChainMember()){
      $chains = Chain::currentUserChainData();
      foreach ($chains as $chain) {
        $chainids[] = $chain->chain_id;
      }
      $chain_lists = Chain::whereIn('id',$chainids)->paginate(Config::get('mybooking.pagination.page_limit'));
      return view('chain.list')->with(array('chain_lists'=>$chain_lists));
    }else{
     return view('errors.401');
   }
 }
}

public function getChainCreatePage(){
  if(System::isAdmin()){
    return view('chain.create');
  }else{
    return view('errors.401');
  }
}

public function postCreateChain(Request $request){
  $rules = array(
            'name'                  => 'required',                        // just a normal required validation
            'address_1'             => 'required',                        // just a normal required validation
            'postal_code'           => 'required',                        // just a normal required validation
            'city'                  => 'required',                        // just a normal required validation
            'country'               => 'required',                        // just a normal required validation
            'phone'                 => 'required',                        // just a normal required validation
            'website'               => 'required',                        // just a normal required validation
            'email'                 => 'required|email',     // required and must be unique in the ducks table
            );
  $validator = Validator::make($request->input(), $rules);
  if($request->input('update')=='update'){
    if ($validator->fails()) {
      $messages = $validator->messages();
      return redirect()->back()->withErrors($validator)->withInput($request->input());
    }else{
      $chain_info = Chain::where('id',$request->input('chain_id'))->first();
      $chain_info->name  = $request->input('name');
      $chain_info->address_1  = $request->input('address_1');
      $chain_info->address_2  = $request->input('address_2');
      $chain_info->postal_code  = $request->input('postal_code');
      $chain_info->province  = $request->input('province');
      $chain_info->city  = $request->input('city');
      $chain_info->country  = $request->input('country');
      $chain_info->phone  = $request->input('phone_number');
      $chain_info->website  = $request->input('website');
      $chain_info->email  = $request->input('email');
      $chain_info->status  = $request->input('status');
      if($chain_info->save()){
        Session::flash('success', 'Chain successfully updated');
        return redirect('chain/view/'.$request->input('chain_id'));
      }else{
        return redirect('chain/view/'.$request->input('chain_id'));
      }
    }
  }else{
    if ($validator->fails()) {
      $messages = $validator->messages();
      return redirect()->back()->withErrors($validator)->withInput($request->input());
    }else{
      $create_chain = [
      'name'    => $request->input('name'),
      'address_1' => $request->input('address_1'),
      'address_2' => $request->input('address_2'),
      'postal_code' => $request->input('postal_code'),
      'province'=>$request->input('province'),
      'city'=>$request->input('city'),
      'country'=>$request->input('country'),
      'phone'=>$request->input('phone_number'),
      'website'=>$request->input('website'),
      'email'=>$request->input('email'),
      'status'=>$request->input('status'),
      ];
      $create_chain_reaquest = Chain::create($create_chain);
      Session::flash('success', 'Chain successfully created');
      return redirect('chain/list');
    }
  }

}
public function getEditChainPage($chain_id){
  $user_list = array();
  $users = array();
  $flag = true;
  $chain_info = Chain::where('id',$chain_id)->first();
  if(!$chain_info){
    $messages = array(
      'chain_error'=>"Oops! Chain not found"
      );
    return redirect('chain/list')->withErrors($messages);
  }
  foreach ($chain_info->chainmember as $chain) {
    $users[] = $chain->user->id;
  }
  $user_list = User::whereNotIn('id', $users)->lists('first_name','id')->all();
  if(System::isAdmin()){
    $flag = true;
  }elseif(System::isChainMember()){
    $chains = Chain::currentUserChainData();
    foreach ($chains as $chain) {
      $chainids[] = $chain->chain_id;
    }
    if (!in_array($chain_id, $chainids)) {
     $flag = false;
   }
 }else{
  $flag = false;
}
if($flag){
  return view('chain.edit')->with(array('chain_info'=>$chain_info,'user_list'=>$user_list));
}else{
  return view('errors.401');
}
}

public function getChainInfo($chain_id,Request $request){
  $user_list = array();
  $users = array();
  $flag = true;
  $chain_info = Chain::where('id',$chain_id)->first();
  if(!$chain_info){
    $messages = array(
      'chain_error'=>"Oops! Chain not found"
      );
    return redirect('chain/list')->withErrors($messages);
  }
  foreach ($chain_info->chainmember as $chain) {
    $users[] = $chain->user->id;
  }
  $user_list = User::whereNotIn('id', $users)->lists('first_name','id')->all();
  if(System::isAdmin()){
    $flag = true;
  }elseif(System::isChainMember()){
    $chains = Chain::currentUserChainData();
    foreach ($chains as $chain) {
      $chainids[] = $chain->chain_id;
    }
    if (!in_array($chain_id, $chainids)) {
     $flag = false;
   }
 }else{
  $flag = false;
}
if($flag){
  return view('chain.view')->with(array('chain_info'=>$chain_info,'user_list'=>$user_list));
}else{
  return view('errors.401');
}
}

public function postDeleteChain(Request $request){
  $chain_id = $request->input('chain_id');
  $hotel_delete = Hotel::where('chain_id', $chain_id)->delete();
  $chain_info = Chain::where('id', $chain_id)->first();
  $chain_info->chainmember()->delete();
  $chain_info->delete();
  return response()->json(['response' => 'success']);   
}
}
