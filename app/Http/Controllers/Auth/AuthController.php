<?php

namespace Mybookings\Http\Controllers\Auth;
use Session;
use Mybookings\User;
use Validator;
use Mybookings\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Illuminate\Http\Request;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(){
        //$this->middleware('auth', ['except' => 'getLogout','postLoginPage','getLoginPage']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    public function getLoginPage(){
        if(!Sentinel::check()){
            return view('auth.login');
        }else{
           return redirect('dashboard');
        }
    }

    public function postLoginPage(Request $request){
       $credentials = 
       ['email'    => $request->input('email'),
       'password' => $request->input('password'),
       ];
       try{
       $result = Sentinel::authenticate($credentials);
       if($result){
        return redirect('dashboard');
       }else{
                $messages = array(
             'user'=>'Invalid email or password.'
         );
        return redirect('/')->withErrors($messages);
       }
        
       }catch(ThrottlingException $e){
         $messages = array(
             'user'=>$e->getMessage()
         );
         return redirect('/')->withErrors($messages);
       } catch (NotActivatedException $e){
        $messages = array(
             'user'=>$e->getMessage()
         );
         return redirect('/')->withErrors($messages);
    }
   }

   public function getLogout(){
        Session::flush();
        Sentinel::logout();
        return redirect('/');
    }

}
