<?php

namespace Mybookings\Http\Controllers\Directory;

use View;
use Session;
use System;
use Response;
use Route;
use Illuminate\Http\Request;
use Mybookings\Http\Requests;
use Mybookings\Http\Controllers\Controller;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Cartalyst\Sentinel\Activations\EloquentActivation;
use Mybookings\Models\Country;
use Mybookings\Models\User;
use Mybookings\Models\Profile;
use Mybookings\Models\Hotel;
use Mybookings\Models\PageType;
use Mybookings\Models\DirectorySetting;
use Mybookings\Models\Hotspots;
use Mybookings\Models\HotelRequest;
use Mybookings\Models\FaqPage;
use Mybookings\Models\FloorPlan;
use Mybookings\Models\Menu;
use Mybookings\Models\Deals;
use Mybookings\Models\Pages;
use Mybookings\Models\NavigationMenu;

class DirectoryFrontendController extends Controller {

    public function __construct(Request $request) {
        // $this->middleware('auth', ['except' => ['getDirectory','getRequest','postRequestCreate']]);
        $this->hotel_info = Hotel::where('slug', $request->route('hotel_slug'))->first();
        if (!empty($this->hotel_info)) {
            View::share('hotel_data', $this->hotel_info);
            $navigation_menu = NavigationMenu::getAllNavigationMenu($this->hotel_info->id);
            View::share('navigation_menu', toObject($navigation_menu));
            $directory_setting = DirectorySetting::getDirectorySetting($this->hotel_info->id);
            View::share('directory_setting', $directory_setting);
        } else {
            return view('errors.401');
        }
    } 

    public function getDirectory($hotel_slug) {
        pr($hotel_slug);
        die;
        $hotel_data = Hotel::where('id', $this->hotel_info->id)->first();
        if ($hotel_data) {
            $pages_type = PageType::get();
            $pages_data = [];
            foreach ($pages_type as $page_type) {
                $pages_data[] = array(
                    'id' => $page_type->id,
                    'name' => $page_type->name,
                    'icon' => $page_type->icon,
                    'action' => url($hotel_slug . '/directory/' . $page_type->slug)
                );
            }
            $directory_setting = DirectorySetting::getDirectorySetting($this->hotel_info->id);
            $deals_data = Deals::where('hotel_id', $this->hotel_info->id)->get();
            return view('directory.hotelsite')->with(array('deals_data' => $deals_data, 'hotel_data' => toObject($hotel_data), 'pages_data' => toObject($pages_data), 'directory_setting' => toObject($directory_setting)));
        } else {
            return view('errors.401');
        }
    }

    public function getRequest() {
        return view('directory.request.create')->with(array('hotel_id' => $this->hotel_info->id));
    }

    public function getHotSpots() {
        if (\Request::has('category')) {
            $checked_filter_data = explode('|', $_GET['category']);
            $filter_data = explode('|', $_GET['category']);
        } else {
            $filter_data = Hotspots::getAllHotspotsCategory();
            $checked_filter_data = [];
        }
        $hotsposts_category = Hotspots::getAllHotspotsCategory();
        $hotspots_data = Hotspots::where('hotel_id', $this->hotel_info->id)->whereIn('category', $filter_data)->get();
        $locations = [];
        //print_r($hotspots_data);
        foreach ($hotspots_data as $hotspots) {
            $category = strtolower($hotspots->category);
            if ($category == 'bank/atm') {
                $category = 'bank';
            }
            if ($category == 'religious buildings') {
                $category = 'chruch';
            }
            if ($category == 'public transport') {
                $category = 'transport';
            }
            if ($category == 'emergency/medical') {
                $category = 'hospital';
            }
            if ($category == 'grocery store') {
                $category = 'Grocery';
            }
            if ($hotspots->latlong) {
                $latlong = explode(',', $hotspots->latlong);
                $locations[] = array($hotspots->title, $latlong[0], $latlong[1], $hotspots->id, $category, htmlentities($hotspots->description));
            }
        }
        $hotel_info = Hotel::where('id', $this->hotel_info->id)->first();
        //print_r( $locations);
        return view('directory.hotspots.view', array('hotsposts_category' => $hotsposts_category, 'locations' => json_encode($locations), 'hotspots_data' => $hotspots_data, 'hotel_info' => $hotel_info, 'checked_filter_data' => $checked_filter_data));
    }

    public function postFilterHotspots(Request $request) {
        $locations = array();
        $filter_data = $request->input('filter_data');
        $hotspots_data = Hotspots::whereIn('category', $filter_data)->where('hotel_id', $this->hotel_info->id)->get();
        foreach ($hotspots_data as $hotspots) {
            if ($hotspots->latlong) {
                $latlong = explode(',', $hotspots->latlong);
                $locations [] = array($hotspots->title, $latlong[0], $latlong[1], $hotspots->id, $hotspots->category, $hotspots->description);
            }
        }
        return $locations;
    }

    public function postHotspotsInfoById(Request $request) {
        $hotspots_data = Hotspots::where('id', $request->input('hotspots_id'))
                ->where('hotel_id', $this->hotel_info->id)
                ->first();
        return Response::json(array('response' => 'success', 'data' => $hotspots_data));
    }

    public function postRequestCreate(Request $request) {
        $hotel_request = new HotelRequest();
        $hotel_request->hotel_id = $request->input('hotel_id');
        $hotel_request->title = $request->input('title');
        $hotel_request->name = $request->input('name');
        $hotel_request->room_no = $request->input('room_no');
        $hotel_request->status = 'do';
        $hotel_request->created_time = date('Y-m-d H:i:s');
        $hotel_request->save();
        Session::flash('success', 'Thank you for making a request. The request was succesfully sent.');
        return redirect()->back();
    }

    public function getFaqs() {
        $faq_page_data = FaqPage::where('hotel_id', $this->hotel_info->id)->get();
        return view('directory.faq.view')->with(array('faq_page_data' => $faq_page_data));
    }

    public function getFloor() {
        $floor_plan_data = FloorPlan::where('hotel_id', $this->hotel_info->id)->get();
        return view('directory.floor.view')->with(array('floor_plan_data' => $floor_plan_data));
    }

    public function getDealsTips() {
        $category = ['deal', 'tip'];
        if (\Request::has('type')) {
            $category = explode('|', \Request::input('type'));
        } else {
            $category = ['deal', 'tip'];
        }
        $deals_data = Deals::where('hotel_id', $this->hotel_info->id)->whereIn('type', $category)->get();
        return view('directory.deals.view')->with(array('deals_data' => $deals_data));
    }

    public function getPages($id, $slug) {
        $page_data = Pages::where('slug', $slug)->where('hotel_id', $this->hotel_info->id)->first();
        if ($page_data) {
            return view('directory.pages.view')->with(array('page_data' => $page_data));
        } else {
            return view('errors.404');
        }
    }

    public function getGallery() {
        return view('directory.gallery.view');
    }

    public function getService() {
        $menu_data = [];
        $menu_detals = Menu::where('hotel_id', $this->hotel_info->id)
                ->whereIn('category', ['Room Service', 'Services', 'Other'])
                ->get();
        foreach ($menu_detals as $menu_detal) {
            $menu_data[$menu_detal->category][] = array(
                'title' => $menu_detal->title,
                'sub_text' => $menu_detal->sub_text,
                'price' => $menu_detal->price
            );
        }
        return view('directory.service.view')->with(array('menu_data' => toObject($menu_data)));
    }

    public function getMenu() {
        if (\Request::has('category')) {
            $filter_data = explode('|', $_GET['category']);
        } else {
            $filter_data = ['Breakfast', 'Lunch', 'Dinner', 'Drinks'];
        }
        $menu_data = [];
        $menu_detals = Menu::where('hotel_id', $this->hotel_info->id)
                ->whereIn('category', $filter_data)
                ->get();
        foreach ($menu_detals as $menu_detal) {
            $menu_data[$menu_detal->category][] = array(
                'title' => $menu_detal->title,
                'sub_text' => $menu_detal->sub_text,
                'price' => $menu_detal->price
            );
        }
        return view('directory.service.view')->with(array('menu_data' => toObject($menu_data)));
    }

    public function getContacts() {
        if (\Request::has('category')) {
            $filter_data = explode('|', $_GET['category']);
        } else {
            $filter_data = Hotspots::getAllHotspotsCategory();
        }
        $contact_details = [];
        $contacts = Hotspots::where('hotel_id', $this->hotel_info->id)
                ->where('show_as_hotspots', 0)
                ->whereIn('category', $filter_data)
                ->orderBy('title')
                ->get();
        foreach ($contacts as $contact) {
            $contact_details[strtoUpper(substr($contact->title, 0, 1))][] = array(
                'title_word' => strtoUpper(substr($contact->title, 0, 1)),
                'title' => $contact->title,
                'phone' => $contact->phone
            );
        }
        return view('directory.contact.view')->with(array('contact_details' => toObject($contact_details)));
    }

}
