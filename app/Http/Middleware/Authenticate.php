<?php

namespace Mybookings\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    // protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    // public function __construct(Guard $auth)
    // {
    //     $this->auth = $auth;
    // }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next) {

        // First make sure there is an active session
     if(!Sentinel::check()){
        if ($request->ajax()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect()->route('/');
        }
    }
    
    return $next($request);
    }
}
