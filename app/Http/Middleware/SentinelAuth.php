<?php

namespace Mybookings\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Cartalyst\Sentinel\Native\Facades\Sentinel;

class SentinelAuth
{
    
    public function handle($request, Closure $next)
    {
            if(Sentinel::check()){
            return redirect()->route('/dashboard');
        }

        return $next($request);
    }
}