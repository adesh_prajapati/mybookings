<?php

namespace Mybookings\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Mybookings\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Mybookings\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Mybookings\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \Mybookings\Http\Middleware\RedirectIfAuthenticated::class,
        'sentinel.auth' => \Mybookings\Http\Middleware\SentinelAuth::class,
        // 'sentinel.admin' => \App\Http\Middleware\SentinelAdminAccess::class,
    ];
}
