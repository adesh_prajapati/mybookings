<?php

use Illuminate\Database\Seeder;
use Mybookings\Models\Country;
class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	
    	

        Country::create( [
            'id'=>1,
            'country_code'=>'AF',
            'name'=>'AFGHANISTAN',
            'country_name'=>'Afghanistan',
            'iso3'=>'AFG',
            'numcode'=>4,
            'phonecode'=>93
            ] );



        Country::create( [
            'id'=>2,
            'country_code'=>'AL',
            'name'=>'ALBANIA',
            'country_name'=>'Albania',
            'iso3'=>'ALB',
            'numcode'=>8,
            'phonecode'=>355
            ] );



        Country::create( [
            'id'=>3,
            'country_code'=>'DZ',
            'name'=>'ALGERIA',
            'country_name'=>'Algeria',
            'iso3'=>'DZA',
            'numcode'=>12,
            'phonecode'=>213
            ] );



        Country::create( [
            'id'=>4,
            'country_code'=>'AS',
            'name'=>'AMERICAN SAMOA',
            'country_name'=>'American Samoa',
            'iso3'=>'ASM',
            'numcode'=>16,
            'phonecode'=>1684
            ] );



        Country::create( [
            'id'=>5,
            'country_code'=>'AD',
            'name'=>'ANDORRA',
            'country_name'=>'Andorra',
            'iso3'=>'AND',
            'numcode'=>20,
            'phonecode'=>376
            ] );



        Country::create( [
            'id'=>6,
            'country_code'=>'AO',
            'name'=>'ANGOLA',
            'country_name'=>'Angola',
            'iso3'=>'AGO',
            'numcode'=>24,
            'phonecode'=>244
            ] );



        Country::create( [
            'id'=>7,
            'country_code'=>'AI',
            'name'=>'ANGUILLA',
            'country_name'=>'Anguilla',
            'iso3'=>'AIA',
            'numcode'=>660,
            'phonecode'=>1264
            ] );



        Country::create( [
            'id'=>8,
            'country_code'=>'AQ',
            'name'=>'ANTARCTICA',
            'country_name'=>'Antarctica',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>9,
            'country_code'=>'AG',
            'name'=>'ANTIGUA AND BARBUDA',
            'country_name'=>'Antigua and Barbuda',
            'iso3'=>'ATG',
            'numcode'=>28,
            'phonecode'=>1268
            ] );



        Country::create( [
            'id'=>10,
            'country_code'=>'AR',
            'name'=>'ARGENTINA',
            'country_name'=>'Argentina',
            'iso3'=>'ARG',
            'numcode'=>32,
            'phonecode'=>54
            ] );



        Country::create( [
            'id'=>11,
            'country_code'=>'AM',
            'name'=>'ARMENIA',
            'country_name'=>'Armenia',
            'iso3'=>'ARM',
            'numcode'=>51,
            'phonecode'=>374
            ] );



        Country::create( [
            'id'=>12,
            'country_code'=>'AW',
            'name'=>'ARUBA',
            'country_name'=>'Aruba',
            'iso3'=>'ABW',
            'numcode'=>533,
            'phonecode'=>297
            ] );



        Country::create( [
            'id'=>13,
            'country_code'=>'AU',
            'name'=>'AUSTRALIA',
            'country_name'=>'Australia',
            'iso3'=>'AUS',
            'numcode'=>36,
            'phonecode'=>61
            ] );



        Country::create( [
            'id'=>14,
            'country_code'=>'AT',
            'name'=>'AUSTRIA',
            'country_name'=>'Austria',
            'iso3'=>'AUT',
            'numcode'=>40,
            'phonecode'=>43
            ] );



        Country::create( [
            'id'=>15,
            'country_code'=>'AZ',
            'name'=>'AZERBAIJAN',
            'country_name'=>'Azerbaijan',
            'iso3'=>'AZE',
            'numcode'=>31,
            'phonecode'=>994
            ] );



        Country::create( [
            'id'=>16,
            'country_code'=>'BS',
            'name'=>'BAHAMAS',
            'country_name'=>'Bahamas',
            'iso3'=>'BHS',
            'numcode'=>44,
            'phonecode'=>1242
            ] );



        Country::create( [
            'id'=>17,
            'country_code'=>'BH',
            'name'=>'BAHRAIN',
            'country_name'=>'Bahrain',
            'iso3'=>'BHR',
            'numcode'=>48,
            'phonecode'=>973
            ] );



        Country::create( [
            'id'=>18,
            'country_code'=>'BD',
            'name'=>'BANGLADESH',
            'country_name'=>'Bangladesh',
            'iso3'=>'BGD',
            'numcode'=>50,
            'phonecode'=>880
            ] );



        Country::create( [
            'id'=>19,
            'country_code'=>'BB',
            'name'=>'BARBADOS',
            'country_name'=>'Barbados',
            'iso3'=>'BRB',
            'numcode'=>52,
            'phonecode'=>1246
            ] );



        Country::create( [
            'id'=>20,
            'country_code'=>'BY',
            'name'=>'BELARUS',
            'country_name'=>'Belarus',
            'iso3'=>'BLR',
            'numcode'=>112,
            'phonecode'=>375
            ] );



        Country::create( [
            'id'=>21,
            'country_code'=>'BE',
            'name'=>'BELGIUM',
            'country_name'=>'Belgium',
            'iso3'=>'BEL',
            'numcode'=>56,
            'phonecode'=>32
            ] );



        Country::create( [
            'id'=>22,
            'country_code'=>'BZ',
            'name'=>'BELIZE',
            'country_name'=>'Belize',
            'iso3'=>'BLZ',
            'numcode'=>84,
            'phonecode'=>501
            ] );



        Country::create( [
            'id'=>23,
            'country_code'=>'BJ',
            'name'=>'BENIN',
            'country_name'=>'Benin',
            'iso3'=>'BEN',
            'numcode'=>204,
            'phonecode'=>229
            ] );



        Country::create( [
            'id'=>24,
            'country_code'=>'BM',
            'name'=>'BERMUDA',
            'country_name'=>'Bermuda',
            'iso3'=>'BMU',
            'numcode'=>60,
            'phonecode'=>1441
            ] );



        Country::create( [
            'id'=>25,
            'country_code'=>'BT',
            'name'=>'BHUTAN',
            'country_name'=>'Bhutan',
            'iso3'=>'BTN',
            'numcode'=>64,
            'phonecode'=>975
            ] );



        Country::create( [
            'id'=>26,
            'country_code'=>'BO',
            'name'=>'BOLIVIA',
            'country_name'=>'Bolivia',
            'iso3'=>'BOL',
            'numcode'=>68,
            'phonecode'=>591
            ] );



        Country::create( [
            'id'=>27,
            'country_code'=>'BA',
            'name'=>'BOSNIA AND HERZEGOVINA',
            'country_name'=>'Bosnia and Herzegovina',
            'iso3'=>'BIH',
            'numcode'=>70,
            'phonecode'=>387
            ] );



        Country::create( [
            'id'=>28,
            'country_code'=>'BW',
            'name'=>'BOTSWANA',
            'country_name'=>'Botswana',
            'iso3'=>'BWA',
            'numcode'=>72,
            'phonecode'=>267
            ] );



        Country::create( [
            'id'=>29,
            'country_code'=>'BV',
            'name'=>'BOUVET ISLAND',
            'country_name'=>'Bouvet Island',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>30,
            'country_code'=>'BR',
            'name'=>'BRAZIL',
            'country_name'=>'Brazil',
            'iso3'=>'BRA',
            'numcode'=>76,
            'phonecode'=>55
            ] );



        Country::create( [
            'id'=>31,
            'country_code'=>'IO',
            'name'=>'BRITISH INDIAN OCEAN TERRITORY',
            'country_name'=>'British Indian Ocean Territory',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>246
            ] );



        Country::create( [
            'id'=>32,
            'country_code'=>'BN',
            'name'=>'BRUNEI DARUSSALAM',
            'country_name'=>'Brunei Darussalam',
            'iso3'=>'BRN',
            'numcode'=>96,
            'phonecode'=>673
            ] );



        Country::create( [
            'id'=>33,
            'country_code'=>'BG',
            'name'=>'BULGARIA',
            'country_name'=>'Bulgaria',
            'iso3'=>'BGR',
            'numcode'=>100,
            'phonecode'=>359
            ] );



        Country::create( [
            'id'=>34,
            'country_code'=>'BF',
            'name'=>'BURKINA FASO',
            'country_name'=>'Burkina Faso',
            'iso3'=>'BFA',
            'numcode'=>854,
            'phonecode'=>226
            ] );



        Country::create( [
            'id'=>35,
            'country_code'=>'BI',
            'name'=>'BURUNDI',
            'country_name'=>'Burundi',
            'iso3'=>'BDI',
            'numcode'=>108,
            'phonecode'=>257
            ] );



        Country::create( [
            'id'=>36,
            'country_code'=>'KH',
            'name'=>'CAMBODIA',
            'country_name'=>'Cambodia',
            'iso3'=>'KHM',
            'numcode'=>116,
            'phonecode'=>855
            ] );



        Country::create( [
            'id'=>37,
            'country_code'=>'CM',
            'name'=>'CAMEROON',
            'country_name'=>'Cameroon',
            'iso3'=>'CMR',
            'numcode'=>120,
            'phonecode'=>237
            ] );



        Country::create( [
            'id'=>38,
            'country_code'=>'CA',
            'name'=>'CANADA',
            'country_name'=>'Canada',
            'iso3'=>'CAN',
            'numcode'=>124,
            'phonecode'=>1
            ] );



        Country::create( [
            'id'=>39,
            'country_code'=>'CV',
            'name'=>'CAPE VERDE',
            'country_name'=>'Cape Verde',
            'iso3'=>'CPV',
            'numcode'=>132,
            'phonecode'=>238
            ] );



        Country::create( [
            'id'=>40,
            'country_code'=>'KY',
            'name'=>'CAYMAN ISLANDS',
            'country_name'=>'Cayman Islands',
            'iso3'=>'CYM',
            'numcode'=>136,
            'phonecode'=>1345
            ] );



        Country::create( [
            'id'=>41,
            'country_code'=>'CF',
            'name'=>'CENTRAL AFRICAN REPUBLIC',
            'country_name'=>'Central African Republic',
            'iso3'=>'CAF',
            'numcode'=>140,
            'phonecode'=>236
            ] );



        Country::create( [
            'id'=>42,
            'country_code'=>'TD',
            'name'=>'CHAD',
            'country_name'=>'Chad',
            'iso3'=>'TCD',
            'numcode'=>148,
            'phonecode'=>235
            ] );



        Country::create( [
            'id'=>43,
            'country_code'=>'CL',
            'name'=>'CHILE',
            'country_name'=>'Chile',
            'iso3'=>'CHL',
            'numcode'=>152,
            'phonecode'=>56
            ] );



        Country::create( [
            'id'=>44,
            'country_code'=>'CN',
            'name'=>'CHINA',
            'country_name'=>'China',
            'iso3'=>'CHN',
            'numcode'=>156,
            'phonecode'=>86
            ] );



        Country::create( [
            'id'=>45,
            'country_code'=>'CX',
            'name'=>'CHRISTMAS ISLAND',
            'country_name'=>'Christmas Island',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>61
            ] );



        Country::create( [
            'id'=>46,
            'country_code'=>'CC',
            'name'=>'COCOS (KEELING) ISLANDS',
            'country_name'=>'Cocos (Keeling) Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>672
            ] );



        Country::create( [
            'id'=>47,
            'country_code'=>'CO',
            'name'=>'COLOMBIA',
            'country_name'=>'Colombia',
            'iso3'=>'COL',
            'numcode'=>170,
            'phonecode'=>57
            ] );



        Country::create( [
            'id'=>48,
            'country_code'=>'KM',
            'name'=>'COMOROS',
            'country_name'=>'Comoros',
            'iso3'=>'COM',
            'numcode'=>174,
            'phonecode'=>269
            ] );



        Country::create( [
            'id'=>49,
            'country_code'=>'CG',
            'name'=>'CONGO',
            'country_name'=>'Congo',
            'iso3'=>'COG',
            'numcode'=>178,
            'phonecode'=>242
            ] );



        Country::create( [
            'id'=>50,
            'country_code'=>'CD',
            'name'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE',
            'country_name'=>'Congo, the Democratic Republic of the',
            'iso3'=>'COD',
            'numcode'=>180,
            'phonecode'=>242
            ] );



        Country::create( [
            'id'=>51,
            'country_code'=>'CK',
            'name'=>'COOK ISLANDS',
            'country_name'=>'Cook Islands',
            'iso3'=>'COK',
            'numcode'=>184,
            'phonecode'=>682
            ] );



        Country::create( [
            'id'=>52,
            'country_code'=>'CR',
            'name'=>'COSTA RICA',
            'country_name'=>'Costa Rica',
            'iso3'=>'CRI',
            'numcode'=>188,
            'phonecode'=>506
            ] );



        Country::create( [
            'id'=>53,
            'country_code'=>'CI',
            'name'=>'COTE D',
            'country_name'=>'IVOIRE',
            'iso3'=>'Cote D',
            'numcode'=>384,
            'phonecode'=>225,
            ] );



        Country::create( [
            'id'=>54,
            'country_code'=>'HR',
            'name'=>'CROATIA',
            'country_name'=>'Croatia',
            'iso3'=>'HRV',
            'numcode'=>191,
            'phonecode'=>385
            ] );



        Country::create( [
            'id'=>55,
            'country_code'=>'CU',
            'name'=>'CUBA',
            'country_name'=>'Cuba',
            'iso3'=>'CUB',
            'numcode'=>192,
            'phonecode'=>53
            ] );



        Country::create( [
            'id'=>56,
            'country_code'=>'CY',
            'name'=>'CYPRUS',
            'country_name'=>'Cyprus',
            'iso3'=>'CYP',
            'numcode'=>196,
            'phonecode'=>357
            ] );



        Country::create( [
            'id'=>57,
            'country_code'=>'CZ',
            'name'=>'CZECH REPUBLIC',
            'country_name'=>'Czech Republic',
            'iso3'=>'CZE',
            'numcode'=>203,
            'phonecode'=>420
            ] );



        Country::create( [
            'id'=>58,
            'country_code'=>'DK',
            'name'=>'DENMARK',
            'country_name'=>'Denmark',
            'iso3'=>'DNK',
            'numcode'=>208,
            'phonecode'=>45
            ] );



        Country::create( [
            'id'=>59,
            'country_code'=>'DJ',
            'name'=>'DJIBOUTI',
            'country_name'=>'Djibouti',
            'iso3'=>'DJI',
            'numcode'=>262,
            'phonecode'=>253
            ] );



        Country::create( [
            'id'=>60,
            'country_code'=>'DM',
            'name'=>'DOMINICA',
            'country_name'=>'Dominica',
            'iso3'=>'DMA',
            'numcode'=>212,
            'phonecode'=>1767
            ] );



        Country::create( [
            'id'=>61,
            'country_code'=>'DO',
            'name'=>'DOMINICAN REPUBLIC',
            'country_name'=>'Dominican Republic',
            'iso3'=>'DOM',
            'numcode'=>214,
            'phonecode'=>1809
            ] );



        Country::create( [
            'id'=>62,
            'country_code'=>'EC',
            'name'=>'ECUADOR',
            'country_name'=>'Ecuador',
            'iso3'=>'ECU',
            'numcode'=>218,
            'phonecode'=>593
            ] );



        Country::create( [
            'id'=>63,
            'country_code'=>'EG',
            'name'=>'EGYPT',
            'country_name'=>'Egypt',
            'iso3'=>'EGY',
            'numcode'=>818,
            'phonecode'=>20
            ] );



        Country::create( [
            'id'=>64,
            'country_code'=>'SV',
            'name'=>'EL SALVADOR',
            'country_name'=>'El Salvador',
            'iso3'=>'SLV',
            'numcode'=>222,
            'phonecode'=>503
            ] );



        Country::create( [
            'id'=>65,
            'country_code'=>'GQ',
            'name'=>'EQUATORIAL GUINEA',
            'country_name'=>'Equatorial Guinea',
            'iso3'=>'GNQ',
            'numcode'=>226,
            'phonecode'=>240
            ] );



        Country::create( [
            'id'=>66,
            'country_code'=>'ER',
            'name'=>'ERITREA',
            'country_name'=>'Eritrea',
            'iso3'=>'ERI',
            'numcode'=>232,
            'phonecode'=>291
            ] );



        Country::create( [
            'id'=>67,
            'country_code'=>'EE',
            'name'=>'ESTONIA',
            'country_name'=>'Estonia',
            'iso3'=>'EST',
            'numcode'=>233,
            'phonecode'=>372
            ] );



        Country::create( [
            'id'=>68,
            'country_code'=>'ET',
            'name'=>'ETHIOPIA',
            'country_name'=>'Ethiopia',
            'iso3'=>'ETH',
            'numcode'=>231,
            'phonecode'=>251
            ] );



        Country::create( [
            'id'=>69,
            'country_code'=>'FK',
            'name'=>'FALKLAND ISLANDS (MALVINAS)',
            'country_name'=>'Falkland Islands (Malvinas)',
            'iso3'=>'FLK',
            'numcode'=>238,
            'phonecode'=>500
            ] );



        Country::create( [
            'id'=>70,
            'country_code'=>'FO',
            'name'=>'FAROE ISLANDS',
            'country_name'=>'Faroe Islands',
            'iso3'=>'FRO',
            'numcode'=>234,
            'phonecode'=>298
            ] );



        Country::create( [
            'id'=>71,
            'country_code'=>'FJ',
            'name'=>'FIJI',
            'country_name'=>'Fiji',
            'iso3'=>'FJI',
            'numcode'=>242,
            'phonecode'=>679
            ] );



        Country::create( [
            'id'=>72,
            'country_code'=>'FI',
            'name'=>'FINLAND',
            'country_name'=>'Finland',
            'iso3'=>'FIN',
            'numcode'=>246,
            'phonecode'=>358
            ] );



        Country::create( [
            'id'=>73,
            'country_code'=>'FR',
            'name'=>'FRANCE',
            'country_name'=>'France',
            'iso3'=>'FRA',
            'numcode'=>250,
            'phonecode'=>33
            ] );



        Country::create( [
            'id'=>74,
            'country_code'=>'GF',
            'name'=>'FRENCH GUIANA',
            'country_name'=>'French Guiana',
            'iso3'=>'GUF',
            'numcode'=>254,
            'phonecode'=>594
            ] );



        Country::create( [
            'id'=>75,
            'country_code'=>'PF',
            'name'=>'FRENCH POLYNESIA',
            'country_name'=>'French Polynesia',
            'iso3'=>'PYF',
            'numcode'=>258,
            'phonecode'=>689
            ] );



        Country::create( [
            'id'=>76,
            'country_code'=>'TF',
            'name'=>'FRENCH SOUTHERN TERRITORIES',
            'country_name'=>'French Southern Territories',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>77,
            'country_code'=>'GA',
            'name'=>'GABON',
            'country_name'=>'Gabon',
            'iso3'=>'GAB',
            'numcode'=>266,
            'phonecode'=>241
            ] );



        Country::create( [
            'id'=>78,
            'country_code'=>'GM',
            'name'=>'GAMBIA',
            'country_name'=>'Gambia',
            'iso3'=>'GMB',
            'numcode'=>270,
            'phonecode'=>220
            ] );



        Country::create( [
            'id'=>79,
            'country_code'=>'GE',
            'name'=>'GEORGIA',
            'country_name'=>'Georgia',
            'iso3'=>'GEO',
            'numcode'=>268,
            'phonecode'=>995
            ] );



        Country::create( [
            'id'=>80,
            'country_code'=>'DE',
            'name'=>'GERMANY',
            'country_name'=>'Germany',
            'iso3'=>'DEU',
            'numcode'=>276,
            'phonecode'=>49
            ] );



        Country::create( [
            'id'=>81,
            'country_code'=>'GH',
            'name'=>'GHANA',
            'country_name'=>'Ghana',
            'iso3'=>'GHA',
            'numcode'=>288,
            'phonecode'=>233
            ] );



        Country::create( [
            'id'=>82,
            'country_code'=>'GI',
            'name'=>'GIBRALTAR',
            'country_name'=>'Gibraltar',
            'iso3'=>'GIB',
            'numcode'=>292,
            'phonecode'=>350
            ] );



        Country::create( [
            'id'=>83,
            'country_code'=>'GR',
            'name'=>'GREECE',
            'country_name'=>'Greece',
            'iso3'=>'GRC',
            'numcode'=>300,
            'phonecode'=>30
            ] );



        Country::create( [
            'id'=>84,
            'country_code'=>'GL',
            'name'=>'GREENLAND',
            'country_name'=>'Greenland',
            'iso3'=>'GRL',
            'numcode'=>304,
            'phonecode'=>299
            ] );



        Country::create( [
            'id'=>85,
            'country_code'=>'GD',
            'name'=>'GRENADA',
            'country_name'=>'Grenada',
            'iso3'=>'GRD',
            'numcode'=>308,
            'phonecode'=>1473
            ] );



        Country::create( [
            'id'=>86,
            'country_code'=>'GP',
            'name'=>'GUADELOUPE',
            'country_name'=>'Guadeloupe',
            'iso3'=>'GLP',
            'numcode'=>312,
            'phonecode'=>590
            ] );



        Country::create( [
            'id'=>87,
            'country_code'=>'GU',
            'name'=>'GUAM',
            'country_name'=>'Guam',
            'iso3'=>'GUM',
            'numcode'=>316,
            'phonecode'=>1671
            ] );



        Country::create( [
            'id'=>88,
            'country_code'=>'GT',
            'name'=>'GUATEMALA',
            'country_name'=>'Guatemala',
            'iso3'=>'GTM',
            'numcode'=>320,
            'phonecode'=>502
            ] );



        Country::create( [
            'id'=>89,
            'country_code'=>'GN',
            'name'=>'GUINEA',
            'country_name'=>'Guinea',
            'iso3'=>'GIN',
            'numcode'=>324,
            'phonecode'=>224
            ] );



        Country::create( [
            'id'=>90,
            'country_code'=>'GW',
            'name'=>'GUINEA-BISSAU',
            'country_name'=>'Guinea-Bissau',
            'iso3'=>'GNB',
            'numcode'=>624,
            'phonecode'=>245
            ] );



        Country::create( [
            'id'=>91,
            'country_code'=>'GY',
            'name'=>'GUYANA',
            'country_name'=>'Guyana',
            'iso3'=>'GUY',
            'numcode'=>328,
            'phonecode'=>592
            ] );



        Country::create( [
            'id'=>92,
            'country_code'=>'HT',
            'name'=>'HAITI',
            'country_name'=>'Haiti',
            'iso3'=>'HTI',
            'numcode'=>332,
            'phonecode'=>509
            ] );



        Country::create( [
            'id'=>93,
            'country_code'=>'HM',
            'name'=>'HEARD ISLAND AND MCDONALD ISLANDS',
            'country_name'=>'Heard Island and Mcdonald Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>94,
            'country_code'=>'VA',
            'name'=>'HOLY SEE (VATICAN CITY STATE)',
            'country_name'=>'Holy See (Vatican City State)',
            'iso3'=>'VAT',
            'numcode'=>336,
            'phonecode'=>39
            ] );



        Country::create( [
            'id'=>95,
            'country_code'=>'HN',
            'name'=>'HONDURAS',
            'country_name'=>'Honduras',
            'iso3'=>'HND',
            'numcode'=>340,
            'phonecode'=>504
            ] );



        Country::create( [
            'id'=>96,
            'country_code'=>'HK',
            'name'=>'HONG KONG',
            'country_name'=>'Hong Kong',
            'iso3'=>'HKG',
            'numcode'=>344,
            'phonecode'=>852
            ] );



        Country::create( [
            'id'=>97,
            'country_code'=>'HU',
            'name'=>'HUNGARY',
            'country_name'=>'Hungary',
            'iso3'=>'HUN',
            'numcode'=>348,
            'phonecode'=>36
            ] );



        Country::create( [
            'id'=>98,
            'country_code'=>'IS',
            'name'=>'ICELAND',
            'country_name'=>'Iceland',
            'iso3'=>'ISL',
            'numcode'=>352,
            'phonecode'=>354
            ] );



        Country::create( [
            'id'=>99,
            'country_code'=>'IN',
            'name'=>'INDIA',
            'country_name'=>'India',
            'iso3'=>'IND',
            'numcode'=>356,
            'phonecode'=>91
            ] );



        Country::create( [
            'id'=>100,
            'country_code'=>'ID',
            'name'=>'INDONESIA',
            'country_name'=>'Indonesia',
            'iso3'=>'IDN',
            'numcode'=>360,
            'phonecode'=>62
            ] );



        Country::create( [
            'id'=>101,
            'country_code'=>'IR',
            'name'=>'IRAN, ISLAMIC REPUBLIC OF',
            'country_name'=>'Iran, Islamic Republic of',
            'iso3'=>'IRN',
            'numcode'=>364,
            'phonecode'=>98
            ] );



        Country::create( [
            'id'=>102,
            'country_code'=>'IQ',
            'name'=>'IRAQ',
            'country_name'=>'Iraq',
            'iso3'=>'IRQ',
            'numcode'=>368,
            'phonecode'=>964
            ] );



        Country::create( [
            'id'=>103,
            'country_code'=>'IE',
            'name'=>'IRELAND',
            'country_name'=>'Ireland',
            'iso3'=>'IRL',
            'numcode'=>372,
            'phonecode'=>353
            ] );



        Country::create( [
            'id'=>104,
            'country_code'=>'IL',
            'name'=>'ISRAEL',
            'country_name'=>'Israel',
            'iso3'=>'ISR',
            'numcode'=>376,
            'phonecode'=>972
            ] );



        Country::create( [
            'id'=>105,
            'country_code'=>'IT',
            'name'=>'ITALY',
            'country_name'=>'Italy',
            'iso3'=>'ITA',
            'numcode'=>380,
            'phonecode'=>39
            ] );



        Country::create( [
            'id'=>106,
            'country_code'=>'JM',
            'name'=>'JAMAICA',
            'country_name'=>'Jamaica',
            'iso3'=>'JAM',
            'numcode'=>388,
            'phonecode'=>1876
            ] );



        Country::create( [
            'id'=>107,
            'country_code'=>'JP',
            'name'=>'JAPAN',
            'country_name'=>'Japan',
            'iso3'=>'JPN',
            'numcode'=>392,
            'phonecode'=>81
            ] );



        Country::create( [
            'id'=>108,
            'country_code'=>'JO',
            'name'=>'JORDAN',
            'country_name'=>'Jordan',
            'iso3'=>'JOR',
            'numcode'=>400,
            'phonecode'=>962
            ] );



        Country::create( [
            'id'=>109,
            'country_code'=>'KZ',
            'name'=>'KAZAKHSTAN',
            'country_name'=>'Kazakhstan',
            'iso3'=>'KAZ',
            'numcode'=>398,
            'phonecode'=>7
            ] );



        Country::create( [
            'id'=>110,
            'country_code'=>'KE',
            'name'=>'KENYA',
            'country_name'=>'Kenya',
            'iso3'=>'KEN',
            'numcode'=>404,
            'phonecode'=>254
            ] );



        Country::create( [
            'id'=>111,
            'country_code'=>'KI',
            'name'=>'KIRIBATI',
            'country_name'=>'Kiribati',
            'iso3'=>'KIR',
            'numcode'=>296,
            'phonecode'=>686
            ] );



        Country::create( [
            'id'=>112,
            'country_code'=>'KP',
            'name'=>'KOREA, DEMOCRATIC PEOPLE',
            'country_name'=>'S REPUBLIC OF',
            'iso3'=>'Korea, Democratic People',
            'numcode'=>408,
            'phonecode'=>850,
            ] );



        Country::create( [
            'id'=>113,
            'country_code'=>'KR',
            'name'=>'KOREA, REPUBLIC OF',
            'country_name'=>'Korea, Republic of',
            'iso3'=>'KOR',
            'numcode'=>410,
            'phonecode'=>82
            ] );



        Country::create( [
            'id'=>114,
            'country_code'=>'KW',
            'name'=>'KUWAIT',
            'country_name'=>'Kuwait',
            'iso3'=>'KWT',
            'numcode'=>414,
            'phonecode'=>965
            ] );



        Country::create( [
            'id'=>115,
            'country_code'=>'KG',
            'name'=>'KYRGYZSTAN',
            'country_name'=>'Kyrgyzstan',
            'iso3'=>'KGZ',
            'numcode'=>417,
            'phonecode'=>996
            ] );



        Country::create( [
            'id'=>116,
            'country_code'=>'LA',
            'name'=>'LAO PEOPLE',
            'country_name'=>'S DEMOCRATIC REPUBLIC',
            'iso3'=>'Lao People',
            'numcode'=>418,
            'phonecode'=>856
            ] );



        Country::create( [
            'id'=>117,
            'country_code'=>'LV',
            'name'=>'LATVIA',
            'country_name'=>'Latvia',
            'iso3'=>'LVA',
            'numcode'=>428,
            'phonecode'=>371
            ] );



        Country::create( [
            'id'=>118,
            'country_code'=>'LB',
            'name'=>'LEBANON',
            'country_name'=>'Lebanon',
            'iso3'=>'LBN',
            'numcode'=>422,
            'phonecode'=>961
            ] );



        Country::create( [
            'id'=>119,
            'country_code'=>'LS',
            'name'=>'LESOTHO',
            'country_name'=>'Lesotho',
            'iso3'=>'LSO',
            'numcode'=>426,
            'phonecode'=>266
            ] );



        Country::create( [
            'id'=>120,
            'country_code'=>'LR',
            'name'=>'LIBERIA',
            'country_name'=>'Liberia',
            'iso3'=>'LBR',
            'numcode'=>430,
            'phonecode'=>231
            ] );



        Country::create( [
            'id'=>121,
            'country_code'=>'LY',
            'name'=>'LIBYAN ARAB JAMAHIRIYA',
            'country_name'=>'Libyan Arab Jamahiriya',
            'iso3'=>'LBY',
            'numcode'=>434,
            'phonecode'=>218
            ] );



        Country::create( [
            'id'=>122,
            'country_code'=>'LI',
            'name'=>'LIECHTENSTEIN',
            'country_name'=>'Liechtenstein',
            'iso3'=>'LIE',
            'numcode'=>438,
            'phonecode'=>423
            ] );



        Country::create( [
            'id'=>123,
            'country_code'=>'LT',
            'name'=>'LITHUANIA',
            'country_name'=>'Lithuania',
            'iso3'=>'LTU',
            'numcode'=>440,
            'phonecode'=>370
            ] );



        Country::create( [
            'id'=>124,
            'country_code'=>'LU',
            'name'=>'LUXEMBOURG',
            'country_name'=>'Luxembourg',
            'iso3'=>'LUX',
            'numcode'=>442,
            'phonecode'=>352
            ] );



        Country::create( [
            'id'=>125,
            'country_code'=>'MO',
            'name'=>'MACAO',
            'country_name'=>'Macao',
            'iso3'=>'MAC',
            'numcode'=>446,
            'phonecode'=>853
            ] );



        Country::create( [
            'id'=>126,
            'country_code'=>'MK',
            'name'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF',
            'country_name'=>'Macedonia, the Former Yugoslav Republic of',
            'iso3'=>'MKD',
            'numcode'=>807,
            'phonecode'=>389
            ] );



        Country::create( [
            'id'=>127,
            'country_code'=>'MG',
            'name'=>'MADAGASCAR',
            'country_name'=>'Madagascar',
            'iso3'=>'MDG',
            'numcode'=>450,
            'phonecode'=>261
            ] );



        Country::create( [
            'id'=>128,
            'country_code'=>'MW',
            'name'=>'MALAWI',
            'country_name'=>'Malawi',
            'iso3'=>'MWI',
            'numcode'=>454,
            'phonecode'=>265
            ] );



        Country::create( [
            'id'=>129,
            'country_code'=>'MY',
            'name'=>'MALAYSIA',
            'country_name'=>'Malaysia',
            'iso3'=>'MYS',
            'numcode'=>458,
            'phonecode'=>60
            ] );



        Country::create( [
            'id'=>130,
            'country_code'=>'MV',
            'name'=>'MALDIVES',
            'country_name'=>'Maldives',
            'iso3'=>'MDV',
            'numcode'=>462,
            'phonecode'=>960
            ] );



        Country::create( [
            'id'=>131,
            'country_code'=>'ML',
            'name'=>'MALI',
            'country_name'=>'Mali',
            'iso3'=>'MLI',
            'numcode'=>466,
            'phonecode'=>223
            ] );



        Country::create( [
            'id'=>132,
            'country_code'=>'MT',
            'name'=>'MALTA',
            'country_name'=>'Malta',
            'iso3'=>'MLT',
            'numcode'=>470,
            'phonecode'=>356
            ] );



        Country::create( [
            'id'=>133,
            'country_code'=>'MH',
            'name'=>'MARSHALL ISLANDS',
            'country_name'=>'Marshall Islands',
            'iso3'=>'MHL',
            'numcode'=>584,
            'phonecode'=>692
            ] );



        Country::create( [
            'id'=>134,
            'country_code'=>'MQ',
            'name'=>'MARTINIQUE',
            'country_name'=>'Martinique',
            'iso3'=>'MTQ',
            'numcode'=>474,
            'phonecode'=>596
            ] );



        Country::create( [
            'id'=>135,
            'country_code'=>'MR',
            'name'=>'MAURITANIA',
            'country_name'=>'Mauritania',
            'iso3'=>'MRT',
            'numcode'=>478,
            'phonecode'=>222
            ] );



        Country::create( [
            'id'=>136,
            'country_code'=>'MU',
            'name'=>'MAURITIUS',
            'country_name'=>'Mauritius',
            'iso3'=>'MUS',
            'numcode'=>480,
            'phonecode'=>230
            ] );



        Country::create( [
            'id'=>137,
            'country_code'=>'YT',
            'name'=>'MAYOTTE',
            'country_name'=>'Mayotte',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>269
            ] );



        Country::create( [
            'id'=>138,
            'country_code'=>'MX',
            'name'=>'MEXICO',
            'country_name'=>'Mexico',
            'iso3'=>'MEX',
            'numcode'=>484,
            'phonecode'=>52
            ] );



        Country::create( [
            'id'=>139,
            'country_code'=>'FM',
            'name'=>'MICRONESIA, FEDERATED STATES OF',
            'country_name'=>'Micronesia, Federated States of',
            'iso3'=>'FSM',
            'numcode'=>583,
            'phonecode'=>691
            ] );



        Country::create( [
            'id'=>140,
            'country_code'=>'MD',
            'name'=>'MOLDOVA, REPUBLIC OF',
            'country_name'=>'Moldova, Republic of',
            'iso3'=>'MDA',
            'numcode'=>498,
            'phonecode'=>373
            ] );



        Country::create( [
            'id'=>141,
            'country_code'=>'MC',
            'name'=>'MONACO',
            'country_name'=>'Monaco',
            'iso3'=>'MCO',
            'numcode'=>492,
            'phonecode'=>377
            ] );



        Country::create( [
            'id'=>142,
            'country_code'=>'MN',
            'name'=>'MONGOLIA',
            'country_name'=>'Mongolia',
            'iso3'=>'MNG',
            'numcode'=>496,
            'phonecode'=>976
            ] );



        Country::create( [
            'id'=>143,
            'country_code'=>'MS',
            'name'=>'MONTSERRAT',
            'country_name'=>'Montserrat',
            'iso3'=>'MSR',
            'numcode'=>500,
            'phonecode'=>1664
            ] );



        Country::create( [
            'id'=>144,
            'country_code'=>'MA',
            'name'=>'MOROCCO',
            'country_name'=>'Morocco',
            'iso3'=>'MAR',
            'numcode'=>504,
            'phonecode'=>212
            ] );



        Country::create( [
            'id'=>145,
            'country_code'=>'MZ',
            'name'=>'MOZAMBIQUE',
            'country_name'=>'Mozambique',
            'iso3'=>'MOZ',
            'numcode'=>508,
            'phonecode'=>258
            ] );



        Country::create( [
            'id'=>146,
            'country_code'=>'MM',
            'name'=>'MYANMAR',
            'country_name'=>'Myanmar',
            'iso3'=>'MMR',
            'numcode'=>104,
            'phonecode'=>95
            ] );



        Country::create( [
            'id'=>147,
            'country_code'=>'NA',
            'name'=>'NAMIBIA',
            'country_name'=>'Namibia',
            'iso3'=>'NAM',
            'numcode'=>516,
            'phonecode'=>264
            ] );



        Country::create( [
            'id'=>148,
            'country_code'=>'NR',
            'name'=>'NAURU',
            'country_name'=>'Nauru',
            'iso3'=>'NRU',
            'numcode'=>520,
            'phonecode'=>674
            ] );



        Country::create( [
            'id'=>149,
            'country_code'=>'NP',
            'name'=>'NEPAL',
            'country_name'=>'Nepal',
            'iso3'=>'NPL',
            'numcode'=>524,
            'phonecode'=>977
            ] );



        Country::create( [
            'id'=>150,
            'country_code'=>'NL',
            'name'=>'NETHERLANDS',
            'country_name'=>'Netherlands',
            'iso3'=>'NLD',
            'numcode'=>528,
            'phonecode'=>31
            ] );



        Country::create( [
            'id'=>151,
            'country_code'=>'AN',
            'name'=>'NETHERLANDS ANTILLES',
            'country_name'=>'Netherlands Antilles',
            'iso3'=>'ANT',
            'numcode'=>530,
            'phonecode'=>599
            ] );



        Country::create( [
            'id'=>152,
            'country_code'=>'NC',
            'name'=>'NEW CALEDONIA',
            'country_name'=>'New Caledonia',
            'iso3'=>'NCL',
            'numcode'=>540,
            'phonecode'=>687
            ] );



        Country::create( [
            'id'=>153,
            'country_code'=>'NZ',
            'name'=>'NEW ZEALAND',
            'country_name'=>'New Zealand',
            'iso3'=>'NZL',
            'numcode'=>554,
            'phonecode'=>64
            ] );



        Country::create( [
            'id'=>154,
            'country_code'=>'NI',
            'name'=>'NICARAGUA',
            'country_name'=>'Nicaragua',
            'iso3'=>'NIC',
            'numcode'=>558,
            'phonecode'=>505
            ] );



        Country::create( [
            'id'=>155,
            'country_code'=>'NE',
            'name'=>'NIGER',
            'country_name'=>'Niger',
            'iso3'=>'NER',
            'numcode'=>562,
            'phonecode'=>227
            ] );



        Country::create( [
            'id'=>156,
            'country_code'=>'NG',
            'name'=>'NIGERIA',
            'country_name'=>'Nigeria',
            'iso3'=>'NGA',
            'numcode'=>566,
            'phonecode'=>234
            ] );



        Country::create( [
            'id'=>157,
            'country_code'=>'NU',
            'name'=>'NIUE',
            'country_name'=>'Niue',
            'iso3'=>'NIU',
            'numcode'=>570,
            'phonecode'=>683
            ] );



        Country::create( [
            'id'=>158,
            'country_code'=>'NF',
            'name'=>'NORFOLK ISLAND',
            'country_name'=>'Norfolk Island',
            'iso3'=>'NFK',
            'numcode'=>574,
            'phonecode'=>672
            ] );



        Country::create( [
            'id'=>159,
            'country_code'=>'MP',
            'name'=>'NORTHERN MARIANA ISLANDS',
            'country_name'=>'Northern Mariana Islands',
            'iso3'=>'MNP',
            'numcode'=>580,
            'phonecode'=>1670
            ] );



        Country::create( [
            'id'=>160,
            'country_code'=>'NO',
            'name'=>'NORWAY',
            'country_name'=>'Norway',
            'iso3'=>'NOR',
            'numcode'=>578,
            'phonecode'=>47
            ] );



        Country::create( [
            'id'=>161,
            'country_code'=>'OM',
            'name'=>'OMAN',
            'country_name'=>'Oman',
            'iso3'=>'OMN',
            'numcode'=>512,
            'phonecode'=>968
            ] );



        Country::create( [
            'id'=>162,
            'country_code'=>'PK',
            'name'=>'PAKISTAN',
            'country_name'=>'Pakistan',
            'iso3'=>'PAK',
            'numcode'=>586,
            'phonecode'=>92
            ] );



        Country::create( [
            'id'=>163,
            'country_code'=>'PW',
            'name'=>'PALAU',
            'country_name'=>'Palau',
            'iso3'=>'PLW',
            'numcode'=>585,
            'phonecode'=>680
            ] );



        Country::create( [
            'id'=>164,
            'country_code'=>'PS',
            'name'=>'PALESTINIAN TERRITORY, OCCUPIED',
            'country_name'=>'Palestinian Territory, Occupied',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>970
            ] );



        Country::create( [
            'id'=>165,
            'country_code'=>'PA',
            'name'=>'PANAMA',
            'country_name'=>'Panama',
            'iso3'=>'PAN',
            'numcode'=>591,
            'phonecode'=>507
            ] );



        Country::create( [
            'id'=>166,
            'country_code'=>'PG',
            'name'=>'PAPUA NEW GUINEA',
            'country_name'=>'Papua New Guinea',
            'iso3'=>'PNG',
            'numcode'=>598,
            'phonecode'=>675
            ] );



        Country::create( [
            'id'=>167,
            'country_code'=>'PY',
            'name'=>'PARAGUAY',
            'country_name'=>'Paraguay',
            'iso3'=>'PRY',
            'numcode'=>600,
            'phonecode'=>595
            ] );



        Country::create( [
            'id'=>168,
            'country_code'=>'PE',
            'name'=>'PERU',
            'country_name'=>'Peru',
            'iso3'=>'PER',
            'numcode'=>604,
            'phonecode'=>51
            ] );



        Country::create( [
            'id'=>169,
            'country_code'=>'PH',
            'name'=>'PHILIPPINES',
            'country_name'=>'Philippines',
            'iso3'=>'PHL',
            'numcode'=>608,
            'phonecode'=>63
            ] );



        Country::create( [
            'id'=>170,
            'country_code'=>'PN',
            'name'=>'PITCAIRN',
            'country_name'=>'Pitcairn',
            'iso3'=>'PCN',
            'numcode'=>612,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>171,
            'country_code'=>'PL',
            'name'=>'POLAND',
            'country_name'=>'Poland',
            'iso3'=>'POL',
            'numcode'=>616,
            'phonecode'=>48
            ] );



        Country::create( [
            'id'=>172,
            'country_code'=>'PT',
            'name'=>'PORTUGAL',
            'country_name'=>'Portugal',
            'iso3'=>'PRT',
            'numcode'=>620,
            'phonecode'=>351
            ] );



        Country::create( [
            'id'=>173,
            'country_code'=>'PR',
            'name'=>'PUERTO RICO',
            'country_name'=>'Puerto Rico',
            'iso3'=>'PRI',
            'numcode'=>630,
            'phonecode'=>1787
            ] );



        Country::create( [
            'id'=>174,
            'country_code'=>'QA',
            'name'=>'QATAR',
            'country_name'=>'Qatar',
            'iso3'=>'QAT',
            'numcode'=>634,
            'phonecode'=>974
            ] );



        Country::create( [
            'id'=>175,
            'country_code'=>'RE',
            'name'=>'REUNION',
            'country_name'=>'Reunion',
            'iso3'=>'REU',
            'numcode'=>638,
            'phonecode'=>262
            ] );



        Country::create( [
            'id'=>176,
            'country_code'=>'RO',
            'name'=>'ROMANIA',
            'country_name'=>'Romania',
            'iso3'=>'ROM',
            'numcode'=>642,
            'phonecode'=>40
            ] );



        Country::create( [
            'id'=>177,
            'country_code'=>'RU',
            'name'=>'RUSSIAN FEDERATION',
            'country_name'=>'Russian Federation',
            'iso3'=>'RUS',
            'numcode'=>643,
            'phonecode'=>70
            ] );



        Country::create( [
            'id'=>178,
            'country_code'=>'RW',
            'name'=>'RWANDA',
            'country_name'=>'Rwanda',
            'iso3'=>'RWA',
            'numcode'=>646,
            'phonecode'=>250
            ] );



        Country::create( [
            'id'=>179,
            'country_code'=>'SH',
            'name'=>'SAINT HELENA',
            'country_name'=>'Saint Helena',
            'iso3'=>'SHN',
            'numcode'=>654,
            'phonecode'=>290
            ] );



        Country::create( [
            'id'=>180,
            'country_code'=>'KN',
            'name'=>'SAINT KITTS AND NEVIS',
            'country_name'=>'Saint Kitts and Nevis',
            'iso3'=>'KNA',
            'numcode'=>659,
            'phonecode'=>1869
            ] );



        Country::create( [
            'id'=>181,
            'country_code'=>'LC',
            'name'=>'SAINT LUCIA',
            'country_name'=>'Saint Lucia',
            'iso3'=>'LCA',
            'numcode'=>662,
            'phonecode'=>1758
            ] );



        Country::create( [
            'id'=>182,
            'country_code'=>'PM',
            'name'=>'SAINT PIERRE AND MIQUELON',
            'country_name'=>'Saint Pierre and Miquelon',
            'iso3'=>'SPM',
            'numcode'=>666,
            'phonecode'=>508
            ] );



        Country::create( [
            'id'=>183,
            'country_code'=>'VC',
            'name'=>'SAINT VINCENT AND THE GRENADINES',
            'country_name'=>'Saint Vincent and the Grenadines',
            'iso3'=>'VCT',
            'numcode'=>670,
            'phonecode'=>1784
            ] );



        Country::create( [
            'id'=>184,
            'country_code'=>'WS',
            'name'=>'SAMOA',
            'country_name'=>'Samoa',
            'iso3'=>'WSM',
            'numcode'=>882,
            'phonecode'=>684
            ] );



        Country::create( [
            'id'=>185,
            'country_code'=>'SM',
            'name'=>'SAN MARINO',
            'country_name'=>'San Marino',
            'iso3'=>'SMR',
            'numcode'=>674,
            'phonecode'=>378
            ] );



        Country::create( [
            'id'=>186,
            'country_code'=>'ST',
            'name'=>'SAO TOME AND PRINCIPE',
            'country_name'=>'Sao Tome and Principe',
            'iso3'=>'STP',
            'numcode'=>678,
            'phonecode'=>239
            ] );



        Country::create( [
            'id'=>187,
            'country_code'=>'SA',
            'name'=>'SAUDI ARABIA',
            'country_name'=>'Saudi Arabia',
            'iso3'=>'SAU',
            'numcode'=>682,
            'phonecode'=>966
            ] );



        Country::create( [
            'id'=>188,
            'country_code'=>'SN',
            'name'=>'SENEGAL',
            'country_name'=>'Senegal',
            'iso3'=>'SEN',
            'numcode'=>686,
            'phonecode'=>221
            ] );



        Country::create( [
            'id'=>189,
            'country_code'=>'CS',
            'name'=>'SERBIA AND MONTENEGRO',
            'country_name'=>'Serbia and Montenegro',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>381
            ] );



        Country::create( [
            'id'=>190,
            'country_code'=>'SC',
            'name'=>'SEYCHELLES',
            'country_name'=>'Seychelles',
            'iso3'=>'SYC',
            'numcode'=>690,
            'phonecode'=>248
            ] );



        Country::create( [
            'id'=>191,
            'country_code'=>'SL',
            'name'=>'SIERRA LEONE',
            'country_name'=>'Sierra Leone',
            'iso3'=>'SLE',
            'numcode'=>694,
            'phonecode'=>232
            ] );



        Country::create( [
            'id'=>192,
            'country_code'=>'SG',
            'name'=>'SINGAPORE',
            'country_name'=>'Singapore',
            'iso3'=>'SGP',
            'numcode'=>702,
            'phonecode'=>65
            ] );



        Country::create( [
            'id'=>193,
            'country_code'=>'SK',
            'name'=>'SLOVAKIA',
            'country_name'=>'Slovakia',
            'iso3'=>'SVK',
            'numcode'=>703,
            'phonecode'=>421
            ] );



        Country::create( [
            'id'=>194,
            'country_code'=>'SI',
            'name'=>'SLOVENIA',
            'country_name'=>'Slovenia',
            'iso3'=>'SVN',
            'numcode'=>705,
            'phonecode'=>386
            ] );



        Country::create( [
            'id'=>195,
            'country_code'=>'SB',
            'name'=>'SOLOMON ISLANDS',
            'country_name'=>'Solomon Islands',
            'iso3'=>'SLB',
            'numcode'=>90,
            'phonecode'=>677
            ] );



        Country::create( [
            'id'=>196,
            'country_code'=>'SO',
            'name'=>'SOMALIA',
            'country_name'=>'Somalia',
            'iso3'=>'SOM',
            'numcode'=>706,
            'phonecode'=>252
            ] );



        Country::create( [
            'id'=>197,
            'country_code'=>'ZA',
            'name'=>'SOUTH AFRICA',
            'country_name'=>'South Africa',
            'iso3'=>'ZAF',
            'numcode'=>710,
            'phonecode'=>27
            ] );



        Country::create( [
            'id'=>198,
            'country_code'=>'GS',
            'name'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS',
            'country_name'=>'South Georgia and the South Sandwich Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>0
            ] );



        Country::create( [
            'id'=>199,
            'country_code'=>'ES',
            'name'=>'SPAIN',
            'country_name'=>'Spain',
            'iso3'=>'ESP',
            'numcode'=>724,
            'phonecode'=>34
            ] );



        Country::create( [
            'id'=>200,
            'country_code'=>'LK',
            'name'=>'SRI LANKA',
            'country_name'=>'Sri Lanka',
            'iso3'=>'LKA',
            'numcode'=>144,
            'phonecode'=>94
            ] );



        Country::create( [
            'id'=>201,
            'country_code'=>'SD',
            'name'=>'SUDAN',
            'country_name'=>'Sudan',
            'iso3'=>'SDN',
            'numcode'=>736,
            'phonecode'=>249
            ] );



        Country::create( [
            'id'=>202,
            'country_code'=>'SR',
            'name'=>'SURINAME',
            'country_name'=>'Suriname',
            'iso3'=>'SUR',
            'numcode'=>740,
            'phonecode'=>597
            ] );



        Country::create( [
            'id'=>203,
            'country_code'=>'SJ',
            'name'=>'SVALBARD AND JAN MAYEN',
            'country_name'=>'Svalbard and Jan Mayen',
            'iso3'=>'SJM',
            'numcode'=>744,
            'phonecode'=>47
            ] );



        Country::create( [
            'id'=>204,
            'country_code'=>'SZ',
            'name'=>'SWAZILAND',
            'country_name'=>'Swaziland',
            'iso3'=>'SWZ',
            'numcode'=>748,
            'phonecode'=>268
            ] );



        Country::create( [
            'id'=>205,
            'country_code'=>'SE',
            'name'=>'SWEDEN',
            'country_name'=>'Sweden',
            'iso3'=>'SWE',
            'numcode'=>752,
            'phonecode'=>46
            ] );



        Country::create( [
            'id'=>206,
            'country_code'=>'CH',
            'name'=>'SWITZERLAND',
            'country_name'=>'Switzerland',
            'iso3'=>'CHE',
            'numcode'=>756,
            'phonecode'=>41
            ] );



        Country::create( [
            'id'=>207,
            'country_code'=>'SY',
            'name'=>'SYRIAN ARAB REPUBLIC',
            'country_name'=>'Syrian Arab Republic',
            'iso3'=>'SYR',
            'numcode'=>760,
            'phonecode'=>963
            ] );



        Country::create( [
            'id'=>208,
            'country_code'=>'TW',
            'name'=>'TAIWAN, PROVINCE OF CHINA',
            'country_name'=>'Taiwan, Province of China',
            'iso3'=>'TWN',
            'numcode'=>158,
            'phonecode'=>886
            ] );



        Country::create( [
            'id'=>209,
            'country_code'=>'TJ',
            'name'=>'TAJIKISTAN',
            'country_name'=>'Tajikistan',
            'iso3'=>'TJK',
            'numcode'=>762,
            'phonecode'=>992
            ] );



        Country::create( [
            'id'=>210,
            'country_code'=>'TZ',
            'name'=>'TANZANIA, UNITED REPUBLIC OF',
            'country_name'=>'Tanzania, United Republic of',
            'iso3'=>'TZA',
            'numcode'=>834,
            'phonecode'=>255
            ] );



        Country::create( [
            'id'=>211,
            'country_code'=>'TH',
            'name'=>'THAILAND',
            'country_name'=>'Thailand',
            'iso3'=>'THA',
            'numcode'=>764,
            'phonecode'=>66
            ] );



        Country::create( [
            'id'=>212,
            'country_code'=>'TL',
            'name'=>'TIMOR-LESTE',
            'country_name'=>'Timor-Leste',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>670
            ] );



        Country::create( [
            'id'=>213,
            'country_code'=>'TG',
            'name'=>'TOGO',
            'country_name'=>'Togo',
            'iso3'=>'TGO',
            'numcode'=>768,
            'phonecode'=>228
            ] );



        Country::create( [
            'id'=>214,
            'country_code'=>'TK',
            'name'=>'TOKELAU',
            'country_name'=>'Tokelau',
            'iso3'=>'TKL',
            'numcode'=>772,
            'phonecode'=>690
            ] );



        Country::create( [
            'id'=>215,
            'country_code'=>'TO',
            'name'=>'TONGA',
            'country_name'=>'Tonga',
            'iso3'=>'TON',
            'numcode'=>776,
            'phonecode'=>676
            ] );



        Country::create( [
            'id'=>216,
            'country_code'=>'TT',
            'name'=>'TRINIDAD AND TOBAGO',
            'country_name'=>'Trinidad and Tobago',
            'iso3'=>'TTO',
            'numcode'=>780,
            'phonecode'=>1868
            ] );



        Country::create( [
            'id'=>217,
            'country_code'=>'TN',
            'name'=>'TUNISIA',
            'country_name'=>'Tunisia',
            'iso3'=>'TUN',
            'numcode'=>788,
            'phonecode'=>216
            ] );



        Country::create( [
            'id'=>218,
            'country_code'=>'TR',
            'name'=>'TURKEY',
            'country_name'=>'Turkey',
            'iso3'=>'TUR',
            'numcode'=>792,
            'phonecode'=>90
            ] );



        Country::create( [
            'id'=>219,
            'country_code'=>'TM',
            'name'=>'TURKMENISTAN',
            'country_name'=>'Turkmenistan',
            'iso3'=>'TKM',
            'numcode'=>795,
            'phonecode'=>7370
            ] );



        Country::create( [
            'id'=>220,
            'country_code'=>'TC',
            'name'=>'TURKS AND CAICOS ISLANDS',
            'country_name'=>'Turks and Caicos Islands',
            'iso3'=>'TCA',
            'numcode'=>796,
            'phonecode'=>1649
            ] );



        Country::create( [
            'id'=>221,
            'country_code'=>'TV',
            'name'=>'TUVALU',
            'country_name'=>'Tuvalu',
            'iso3'=>'TUV',
            'numcode'=>798,
            'phonecode'=>688
            ] );



        Country::create( [
            'id'=>222,
            'country_code'=>'UG',
            'name'=>'UGANDA',
            'country_name'=>'Uganda',
            'iso3'=>'UGA',
            'numcode'=>800,
            'phonecode'=>256
            ] );



        Country::create( [
            'id'=>223,
            'country_code'=>'UA',
            'name'=>'UKRAINE',
            'country_name'=>'Ukraine',
            'iso3'=>'UKR',
            'numcode'=>804,
            'phonecode'=>380
            ] );



        Country::create( [
            'id'=>224,
            'country_code'=>'AE',
            'name'=>'UNITED ARAB EMIRATES',
            'country_name'=>'United Arab Emirates',
            'iso3'=>'ARE',
            'numcode'=>784,
            'phonecode'=>971
            ] );



        Country::create( [
            'id'=>225,
            'country_code'=>'GB',
            'name'=>'UNITED KINGDOM',
            'country_name'=>'United Kingdom',
            'iso3'=>'GBR',
            'numcode'=>826,
            'phonecode'=>44
            ] );



        Country::create( [
            'id'=>226,
            'country_code'=>'US',
            'name'=>'UNITED STATES',
            'country_name'=>'United States',
            'iso3'=>'USA',
            'numcode'=>840,
            'phonecode'=>1
            ] );



        Country::create( [
            'id'=>227,
            'country_code'=>'UM',
            'name'=>'UNITED STATES MINOR OUTLYING ISLANDS',
            'country_name'=>'United States Minor Outlying Islands',
            'iso3'=>NULL,
            'numcode'=>NULL,
            'phonecode'=>1
            ] );



        Country::create( [
            'id'=>228,
            'country_code'=>'UY',
            'name'=>'URUGUAY',
            'country_name'=>'Uruguay',
            'iso3'=>'URY',
            'numcode'=>858,
            'phonecode'=>598
            ] );



        Country::create( [
            'id'=>229,
            'country_code'=>'UZ',
            'name'=>'UZBEKISTAN',
            'country_name'=>'Uzbekistan',
            'iso3'=>'UZB',
            'numcode'=>860,
            'phonecode'=>998
            ] );



        Country::create( [
            'id'=>230,
            'country_code'=>'VU',
            'name'=>'VANUATU',
            'country_name'=>'Vanuatu',
            'iso3'=>'VUT',
            'numcode'=>548,
            'phonecode'=>678
            ] );



        Country::create( [
            'id'=>231,
            'country_code'=>'VE',
            'name'=>'VENEZUELA',
            'country_name'=>'Venezuela',
            'iso3'=>'VEN',
            'numcode'=>862,
            'phonecode'=>58
            ] );



        Country::create( [
            'id'=>232,
            'country_code'=>'VN',
            'name'=>'VIET NAM',
            'country_name'=>'Viet Nam',
            'iso3'=>'VNM',
            'numcode'=>704,
            'phonecode'=>84
            ] );



        Country::create( [
            'id'=>233,
            'country_code'=>'VG',
            'name'=>'VIRGIN ISLANDS, BRITISH',
            'country_name'=>'Virgin Islands, British',
            'iso3'=>'VGB',
            'numcode'=>92,
            'phonecode'=>1284
            ] );



        Country::create( [
            'id'=>234,
            'country_code'=>'VI',
            'name'=>'VIRGIN ISLANDS, U.S.',
            'country_name'=>'Virgin Islands, U.s.',
            'iso3'=>'VIR',
            'numcode'=>850,
            'phonecode'=>1340
            ] );



        Country::create( [
            'id'=>235,
            'country_code'=>'WF',
            'name'=>'WALLIS AND FUTUNA',
            'country_name'=>'Wallis and Futuna',
            'iso3'=>'WLF',
            'numcode'=>876,
            'phonecode'=>681
            ] );



        Country::create( [
            'id'=>236,
            'country_code'=>'EH',
            'name'=>'WESTERN SAHARA',
            'country_name'=>'Western Sahara',
            'iso3'=>'ESH',
            'numcode'=>732,
            'phonecode'=>212
            ] );



        Country::create( [
            'id'=>237,
            'country_code'=>'YE',
            'name'=>'YEMEN',
            'country_name'=>'Yemen',
            'iso3'=>'YEM',
            'numcode'=>887,
            'phonecode'=>967
            ] );



        Country::create( [
            'id'=>238,
            'country_code'=>'ZM',
            'name'=>'ZAMBIA',
            'country_name'=>'Zambia',
            'iso3'=>'ZMB',
            'numcode'=>894,
            'phonecode'=>260
            ] );



        Country::create( [
            'id'=>239,
            'country_code'=>'ZW',
            'name'=>'ZIMBABWE',
            'country_name'=>'Zimbabwe',
            'iso3'=>'ZWE',
            'numcode'=>716,
            'phonecode'=>263
            ] );



    }
}
