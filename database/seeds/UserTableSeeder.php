<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Checkpoints\ThrottlingException;
use Cartalyst\Sentinel\Checkpoints\NotActivatedException;
use Cartalyst\Sentinel\Native\Facades\Sentinel;
use Mybookings\Models\Profile;
class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $this->createRoles();
        $role_data = Sentinel::findRoleBySlug('admin');
        $credentials = [
	        'email'    	=> 'pieter@mybookings.com',
	        'password' 	=> 's33derpass_change_me!',
	        'first_name'=> 'Pieter',
	        'last_name'	=> 'Bayens',
	        'permissions' => [$role_data->slug => true],
        ];
        $user = Sentinel::registerAndActivate($credentials);
        $profile = new  Profile();
        $profile->user_id = $user->id;
        $profile->phone = '06-13217363';
        $profile->save();
        $role = Sentinel::findRoleBySlug('admin');
        $role->users()->attach($user);
    }

    public function createRoles(){
    	$role = Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Administrators',
		    'slug' => 'admin',
		]);
		$role = Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Hotel Managers',
		    'slug' => 'hotel_managers',
		]);
		$role = Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'Chain Managers',
		    'slug' => 'chain_managers',
		]);

		$role = Sentinel::getRoleRepository()->createModel()->create([
		    'name' => 'User',
		    'slug' => 'user',
		]);
    }
}
