<?php

use Illuminate\Database\Seeder;
use Mybookings\Models\PageType;

class PageTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
    	PageType::create([
    		'id' => 1,
    		'name' => 'Places', 
    		'icon' => 'fa fa-map-marker', 
    		'slug' => 'hotspots' 
    		]);
    	PageType::create([
    		'id' => 2,
    		'name' => 'Deals & Tips', 
    		'icon' => 'fa fa-gift', 
    		'slug' => 'deals'
    		]);
    	PageType::create([
    		'id' => 3,
    		'name' => 'Requests', 
    		'icon' => 'fa fa-user', 
    		'slug' => 'requests'
    		]);
    	PageType::create([
    		'id' => 4,
    		'name' => 'FAQ', 
    		'icon' => 'fa fa-question', 
    		'slug' => 'faq'
    		]);
    	PageType::create([
    		'id' => 5,
    		'name' => 'Menu\'s', 
    		'icon' => 'fa fa-cutlery', 
    		'slug' => 'menu'
    		]);
    	PageType::create([
    		'id'   => 6, 
    		'name' => 'Photos', 
    		'icon' => 'fa fa-camera', 
    		'slug' => 'gallery'
    		]);
    	PageType::create([
    		'id'   => 7,
    		'name' => 'Plain text', 
    		'icon' => 'fa fa-file-text', 
    		'slug' => 'pages'
    		]);
    	PageType::create([
    		'id'   => 8,
    		'name' => 'Floor plan', 
    		'icon' => 'fa fa-map', 
    		'slug' => 'floor'
    		]);
    }
}
