<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelsChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {   
      Schema::create('chains', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('address_1');
        $table->string('address_2');
        $table->string('postal_code');
        $table->string('province');
        $table->string('city');
        $table->string('country');
        $table->string('phone');
        $table->string('email');
        $table->string('website');
        $table->enum('status', array('Active', 'Inactive'))->default('Active');
        $table->timestamps();
        $table->engine = 'InnoDB';
      });

      Schema::create('hotels', function(Blueprint $table) {
        $table->increments('id');
        $table->string('name');
        $table->string('slug');
        $table->integer('chain_id')->unsigned()->nullable();
        $table->string('address_1');
        $table->string('address_2');
        $table->string('postal_code');
        $table->string('province');
        $table->string('city');
        $table->string('country');
        $table->string('phone');
        $table->string('email');
        $table->string('website');
        $table->enum('status', array('Active', 'Inactive'))->default('Active');
        $table->timestamps();
        $table->engine = 'InnoDB';
        $table->foreign('chain_id')->references('id')->on('chains');
        $table->unique('slug');
      });

      Schema::create('hotel_members', function(Blueprint $table) {
        $table->integer('user_id')->unsigned();
        $table->integer('hotel_id')->unsigned();
        $table->integer('role_id')->unsigned();
        $table->timestamps();
        $table->engine = 'InnoDB';
        $table->index(['role_id', 'hotel_id','user_id']);
        $table->unique(['role_id', 'hotel_id','user_id']);
        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('hotel_id')->references('id')->on('hotels');
        $table->foreign('role_id')->references('id')->on('roles');
      });

      Schema::create('chain_members', function(Blueprint $table) {
        $table->integer('user_id')->unsigned();
        $table->integer('chain_id')->unsigned();
        $table->integer('role_id')->unsigned();
        $table->timestamps();
        $table->engine = 'InnoDB';
        $table->index(['role_id', 'chain_id','user_id']);
        $table->unique(['role_id', 'chain_id','user_id']);
        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('chain_id')->references('id')->on('chains');
        $table->foreign('role_id')->references('id')->on('roles');
      });

      Schema::create('profile', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('user_id')->unsigned();
        $table->string('phone');
        $table->timestamps();
        $table->engine = 'InnoDB';
        $table->foreign('user_id')->references('id')->on('users');
      });

      Schema::create('countries', function(Blueprint $table) {
        $table->increments('id');
        $table->string('country_code');
        $table->string('country_name');
        $table->string('name');
        $table->string('iso3')->nullable();
        $table->integer('numcode')->nullable();
        $table->integer('phonecode');
        $table->timestamps();
        $table->engine = 'InnoDB';
      });

      Schema::create('hotspots', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('hotel_id');
        $table->string('title');
        $table->string('address');
        $table->text('description');
        $table->string('website');
        $table->string('category');
        $table->string('latlong');
        $table->string('phone');
        $table->tinyInteger('show_as_hotspots');
        $table->tinyInteger('is_published');
        $table->timestamps();
        $table->engine = 'InnoDB';
      });
      Schema::create('deals', function (Blueprint $table) {
        $table->increments('id');
        $table->integer('hotel_id');

        $table->string('title');
        $table->text('short_description');
        $table->text('full_description');
        $table->string('image');
        $table->string('currency');
        $table->integer('price');
        $table->tinyInteger('tip_deal');
        $table->tinyInteger('show_on_home');
        $table->tinyInteger('is_published');
        $table->timestamps();
        $table->engine = 'InnoDB';
      });

      Schema::create('floor_plan', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->string('title');
       $table->string('image');
       $table->timestamps();
       $table->engine = 'InnoDB';
     });
      Schema::create('page_type', function(Blueprint $table) {
       $table->increments('id');
       $table->string('name');
       $table->string('icon');
       $table->string('slug');
       $table->timestamps();
       $table->engine = 'InnoDB';
     });

      Schema::create('directory_setting', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->string('title_color')->nullable();
       $table->string('text_color')->nullable();
       $table->string('text_link_color')->nullable();
       $table->string('icon_color')->nullable();
       $table->string('button_color')->nullable();
       $table->string('intro_text')->nullable();
       $table->string('include_script')->nullable();
       $table->string('include_css')->nullable();
       $table->string('privacy_statement')->nullable();
       $table->timestamps();
       $table->engine = 'InnoDB';
     });

      Schema::create('request', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->string('title');
       $table->string('name');
       $table->string('room_no');
       $table->enum('status', array('do', 'doing','done'));
       $table->dateTime('created_time');
       $table->timestamps();
       $table->engine = 'InnoDB';
     });

      Schema::create('pages', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->string('title');
       $table->text('description');
       $table->string('slug');
       $table->integer('page_type_id');
       $table->tinyInteger('is_published');
       $table->timestamps();
       $table->engine = 'InnoDB';
     }); 

      Schema::create('menu', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->string('title');
       $table->string('sub_text');
       $table->string('currency');
       $table->string('price');
       $table->string('category');
       $table->tinyInteger('is_published');
       $table->timestamps();
       $table->engine = 'InnoDB';
     });
      Schema::create('faq_page', function(Blueprint $table) {
       $table->increments('id');
       $table->integer('hotel_id');
       $table->text('question');
       $table->text('answer');
       $table->timestamps();
       $table->engine = 'InnoDB';
     }); 
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
      Schema::drop('hotel_members');
      Schema::drop('hotels');
      Schema::drop('chain_members');
      Schema::drop('chains');
      Schema::drop('profile');
      Schema::drop('countries');
      Schema::drop('hotspots');
      Schema::drop('deals');
      Schema::drop('floor_plan');
      Schema::drop('page_type');
      Schema::drop('directory_setting');
      Schema::drop('request');
      Schema::drop('pages');
      Schema::drop('menu');
      Schema::drop('faq_page');
    }
  }
