<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('hotels', function(Blueprint $table) {
           $table->string('latlong');
           $table->string('timezone');
           $table->string('social_facebook');
           $table->string('social_twitter');
           $table->string('social_instagram');
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
