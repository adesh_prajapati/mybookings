<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterDirectorySettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::table('directory_setting', function(Blueprint $table) {
         $table->string('sub_heading')->nullable();
         $table->integer('text_link_hover_color')->nullable();
         $table->integer('button_hover_color')->nullable();
         $table->integer('button_text_hover_color')->nullable();
         $table->integer('sub_heading_color')->nullable();
     });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
