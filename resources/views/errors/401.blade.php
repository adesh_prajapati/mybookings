@extends('layouts.public')
    @section('content')
    <div class="middle-box text-center animated fadeInDown">
        <h1>401</h1>
        <h3 class="font-bold">Access denied.</h3>

        <div class="error-desc">
            You are not authorized to access this page<br/>
            You can go back to main page: <br/><a href="{{ URL::route('/')}}" class="btn btn-primary m-t">Continue to Homepage</a>
        </div>
    </div>
@stop