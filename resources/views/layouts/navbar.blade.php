<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <span class="m-r-sm"><img src="{{ URL::asset('asset/img/MyBookings-logo-wh.png')}}" alt="MyBookings Logo" class="topLogo" /></span>
            
                <div class="dropdown profile-element"> 
                    <span class="admin-image" data-toggle="modal" data-target="#myModal5">
                        @if($profile_pic['image'] != '')
                        <img id="admin_image" alt="image" class="img-circle" src="{{URL::asset('uploads/'.$profile_pic['image'])}}" />
                        @else
                        <img id="admin_image" alt="image" class="img-circle" src="http://dummyimage.com/50x50/e0e0e0/444.png&text={{substr($user_data->first_name, 0,1) .substr($user_data->last_name, 0,1)}}" />
                        @endif
                        <span style="display: none;"><i class="fa fa-pencil fa-lg"></i></span>
                    </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs"><strong class="font-bold">{{ $user_data->first_name }}</strong>
                            </span> <span class="text-muted text-xs block">{{ $user_data->roles['0']['name'] }} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="javascript:void(0);">Profile</a></li>
                        <li><a href="javascript:void(0);">Contacts</a></li>
                        <li><a href="javascript:void(0);">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ URL::route('logout')}}">Logout</a></li>
                    </ul>
                </div>
                <div class="logo-element">
                    IN+
                </div>
            </li>
            <li class="{{ isActiveRoute(['dashboard']) }}">
                <a href="{{ URL::route('dashboard')}}"><span class="nav-menu-icon"><i class="fa fa-diamond"></i></span><span class="nav-label">Dashboard</span></a>
            </li>
            <li>
                <a href="javascript:void(0);">
                    <span class="nav-menu-icon"><i class="fa fa-flask"></i></span> 
                    <span class="nav-label">Reviews</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0);">Manage reviews</a></li>
                    <li><a href="javascript:void(0);">Settings</a></li>
                </ul>
            </li>
            @if(System::isHotelMember() || System::isAdmin() ||System::isChainMember())
            <li class="hotelSelectRequired {{ areActiveRoutes(['directory/service/edit','directory/navigation','directory/navigation/create','directory/navigation/edit', 'directory/setting','manage/page','directory/hotspots/list','directory/hotspots/create','directory/hotspots/edit','directory/deals/list','directory/deals/create','directory/deals/edit','directory/service/list','directory/service/create','directory/faq/list','directory/service','directory/service/create', 'directory/gallery/list','directory/pages/list', 'directory/pages/create','directory/pages/edit','directory/floor/list']) }}">
                <a href="javascript:void(0);"><span class="nav-menu-icon"><i class="fa fa-table"></i></span> <span class="nav-label">Hotel Directory</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li  class="{{ isActiveRoute(['directory/service/edit','directory/navigation','directory/navigation/create','directory/navigation/edit'])}}">
                        <a id="directory_menu" href="{{(System::isHotelSession()) ? route('directory/navigation') : '#'}}">
                            <span class="nav-menu-icon"><i class="fa fa-bars"></i></span>
                            Navigation Menu</a>
                    </li>
                    <li class="{{ isActiveRoute(['directory/pages/list', 'directory/pages/create','directory/pages/edit','manage/page','directory/hotspots/list','directory/hotspots/create','directory/hotspots/edit','directory/deals/list','directory/deals/create','directory/deals/edit','directory/service/list','directory/service/create','directory/faq/list','directory/gallery/list','directory/floor/list'])}}">
                        <a id="manage_page" href="{{ (System::isHotelSession()) ? route('manage/page', ['id' => Session::get('hotel')['id'] ]) : '#'}} ">
                            <span class="nav-menu-icon"><i class="fa fa-book"></i></span>
                            <span class="nav-label">Content</span>
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/pages/list','directory/pages/create','directory/pages/edit'])}}">
                                <a href="
                                   {{ (System::isHotelSession()) ? route('directory/pages/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-file-text"></i></span>
                                    Plain Text</a>
                            </li>
                        </ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/gallery/list'])}}">
                                <a href="
                                   {{ (System::isHotelSession()) ? route('directory/gallery/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-camera"></i></span>
                                    Photo Gallery</a>
                            </li>
                        </ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/hotspots/list','directory/hotspots/create','directory/hotspots/edit'])}}">
                                <a href=" {{ (System::isHotelSession()) ? route('directory/hotspots/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-map-marker"></i></span>
                                    Places</a>
                            </li>
                        </ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/deals/list','directory/deals/create','directory/deals/edit'])}}">
                                <a href="
                                   {{ (System::isHotelSession()) ? route('directory/deals/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-gift"></i></span>
                                    Deal/Tips</a>
                            </li>
                        </ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/faq/list'])}}">
                                <a href="{{ (System::isHotelSession()) ? route('directory/faq/list') : '#'}} "><span class="nav-menu-icon">
                                        <i class="fa fa-question"></i></span>
                                    FAQs</a></li></ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/floor/list'])}}">
                                <a href="{{ (System::isHotelSession()) ? route('directory/floor/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-map"></i></span>
                                    Floor Plan</a>
                            </li>
                        </ul>
                        <ul class="nav nav-third-level">
                            <li class="{{ isActiveRoute(['directory/service/list','directory/service/create','directory/service/edit'])}}">
                                <a href="{{ (System::isHotelSession()) ? route('directory/service/list') : '#'}} ">
                                    <span class="nav-menu-icon"><i class="fa fa-cutlery"></i></span>
                                    Menu/Service</a>
                            </li>
                        </ul>
                    </li>
                    <li  class="{{ isActiveRoute(['directory/setting'])}}">
                        <a id="directory_setting" href="{{ (System::isHotelSession()) ? route('directory/setting') : '#'}}">
                            <span class="nav-menu-icon"><i class="fa fa-cogs"></i></span>
                            Settings</a>
                    </li>
                </ul>
            </li>
            @endif
            @if(System::isHotelMember() || System::isAdmin() ||System::isChainMember())
            <li class="hotelSelectRequired {{ isActiveRoute(['directory/requests/list']) }}">
                <a href="{{ (System::isHotelSession()) ? route('directory/requests/list') : '#'}}">
                    <span class="nav-menu-icon"><i class="fa fa-diamond"></i></span>
                    <span class="nav-label">Requests</span></a>
            </li>
            @endif
            <li >
                <a href="javascript:void(0);">
                    <span class="nav-menu-icon"><i class="fa fa-globe"></i></span> 
                    <span class="nav-label">Insights</span>
                    <span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li><a href="javascript:void(0);">Bookings</a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-external-link"></i>Website</a></li>
                    <li><a href="javascript:void(0);">Portal</a></li>
                </ul>
            </li>
            <li class="{{ areActiveRoutes(['users','hotel/list', 'chain/list', 'hotel/create', 'hotel/edit','chain/edit','chain/create']) }}">
                <a href="javascript:void(0);"><i class="fa fa-desktop"></i> <span class="nav-label">Administration</span><span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    @if(System::isHotelMember() || System::isChainMember())
                    <li class="{{ isActiveRoute(['hotel/list','hotel/create'])}}">
                        <a href="{{ URL::route('hotel/list')}}">
                            <span class="nav-menu-icon"><i class="fa fa-bed"></i></span>
                            Hotels</a>
                    </li>
                    @elseif(System::isAdmin())
                    <li class="{{ isActiveRoute(['hotel/list','hotel/create','hotel/edit'])}}">
                        <a href="{{ URL::route('hotel/list')}}"> 
                            <span class="nav-menu-icon"><i class="fa fa-bed"></i></span>
                            Hotels</a>
                    </li>
                    @else

                    @endif
                    @if(System::isChainMember())
                    <li class="{{ isActiveRoute(['chain/list','chain/create'])}}">
                        <a href="{{ URL::route('chain/list')}}">
                            <span class="nav-menu-icon"><i class="fa fa-link"></i></span>
                            Chains</a>
                    </li>
                    @elseif(System::isAdmin())
                    <li class="{{ isActiveRoute(['chain/list','chain/create','chain/edit'])}}">
                        <a href="{{ URL::route('chain/list')}}">
                            <span class="nav-menu-icon"><i class="fa fa-link"></i></span>
                            Chains</a>
                    </li>
                    <li class="{{ isActiveRoute(['users'])}}">
                        <a href="{{ URL::route('users')}}">
                            <span class="nav-menu-icon"><i class="fa fa-users"></i></span>
                            Users</a>
                    </li>
                    @else

                    @endif
                </ul>

            </li>
        </ul>
    </div>
</nav>
<div class="modal inmodal fade" id="myModal5" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Profile Image</h4>
            </div>
            <form id="userform" method="post"  enctype="multipart/form-data" action="{{ URL::route('update/user')}}">
                <div class="modal-body">

                        <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label title="Upload image file" for="inputImage" class="btn btn-primary modal-profile-image-btn" style="margin-top: 50%;">
                                            <input type="hidden" id="img_crop" name="img_crop" value="" />
                                            <input type="file" accept="image/*" name="preview_img" 
                                                   id="inputImage" class="hide">
                                            Upload new image
                                        </label>
                                        <input type="hidden" id="image_crop_path" name="image_crop_path" value=""/> 
                                    </div>
                                </div>
                            </div>
                        </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    <input type="submit" value="Update" class="btn btn-primary"/>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal inmodal fade" id="myModal6" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">Please Select Hotel</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-sm-3 control-label">{{ trans('form.hotel_list') }}
                    </label>
                    <div class="col-sm-9">   
                        <select data-placeholder="please select hotel" style="width:350px;" class="chosen-select" id="hotel_list">
                        @if(System::isAdmin())
                        @if($admin_view_data->count()>=1)
                        @foreach($admin_view_data as $index => $chain)
                        <optgroup label="{{ $chain->name }}">
                            @if($chain->hotellists->count()>=1)
                            @foreach($chain->hotellists as $hotel)
                            <option data-url= "{{ route('manage/page', ['id' => $hotel->id]) }}" value="{{$hotel->name}}">{{$hotel->name}}</option>
                            @endforeach
                            @endif
                        </optgroup>
                        @endforeach
                        @else
                        <optgroup label="no hotel">
                            <option value="no hotel">no hotel</option>
                        </optgroup>
                        @endif
                        @elseif(System::isChainMember())
                        @if($chain_view_data)
                        @foreach($chain_view_data as $index => $chain)
                        <optgroup label="{{ $chain->name }}">
                            @if($chain->hotellists->count()>=1)
                            @foreach($chain->hotellists as $hotel)
                            <option data-url= "{{ route('manage/page', ['id' => $hotel->id]) }}" value="{{$hotel->name}}">{{$hotel->name}}</option>
                            @endforeach
                            @endif
                        </optgroup>
                        @endforeach
                        @else
                        <option>you have not assign to chain or hotel</option>
                        @endif
                        @elseif(System::isHotelMember())
                        @if($hotel_view_data)
                        @if($hotel_view_data->count()>=1)
                        @foreach($hotel_view_data as $hotel)
                        <option data-url= "{{ route('manage/page', ['id' => $hotel->id]) }}"value="{{$hotel->name}}">{{$hotel->name}}</option>
                        @endforeach
                        @endif
                        @endif
                        @else
                        <option>you have not assign to chain or hotel</option>
                        @endif
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" id="modal_choose_hotel">Choose Hotel</button>
            </div>
        </div>
    </div>
</div>
