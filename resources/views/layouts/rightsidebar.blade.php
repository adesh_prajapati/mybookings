<div id="right-sidebar" role="navigation">
	<div class="sidebar-container">
		<ul class="nav nav-tabs navs-3">
			<li class="active"><a data-toggle="tab" href="#tab-1">
				Hotels
			</a></li>
		</ul>
		<!--<div class="sidebar-title">
			<h3> <i class="fa fa-bed"></i> Hotel List</h3>
			<small></small>
		</div>-->
		<ul class="nav nav-stacked" id="accordion1">
			@if(System::isAdmin())
			@if($admin_view_data->count()>=1)
			@foreach($admin_view_data as $index => $chain)
			<li class="grey-bg">
				<a data-toggle="collapse" data-parent="#accordion1" href="#firstLink-{{$index+1}}" class="collapsed">
					<span class="nav-label"><i class="fa fa-database"></i>
						{{ $chain->name }}</span><span class="fa arrow"></span></a>
						<ul id="firstLink-{{$index+1}}" class="collapse nav nav-second-level">
							@if($chain->hotellists->count()>=1)
							@foreach($chain->hotellists as $hotel)
							<li><a href="{{ route('manage/page', ['id' => $hotel->id]) }}"><i class="fa fa-bed"></i> {{ $hotel->name }}</a></li>
							@endforeach
							@endif
						</ul>
					</li>
					@endforeach
					@else
					<li><a href="javascript:void(0);">You have not yet been assigned to a chain or hotel</a></li>
					@endif
					@elseif(System::isChainMember())
					@if($chain_view_data)
					@foreach($chain_view_data as $index => $chain)
					<li>
						<a data-toggle="collapse" data-parent="#accordion1" href="#firstLink-{{$index+1}}">
							<span class="nav-label"><i class="fa fa-database"></i> {{ $chain->name }}</span><span class="fa arrow"></span></a>
							<ul id="firstLink-{{$index+1}}" class="collapse nav nav-second-level">
								@if($chain->hotellists->count()>=1)
								@foreach($chain->hotellists as $hotel)
								<li><a href="{{ route('manage/page', ['id' => $hotel->id]) }}"><i class="fa fa-bed"></i> {{ $hotel->name }}</a></li>
								@endforeach
								@endif
							</ul>
						</li>
						@endforeach
						@else
						<li><a href="javascript:void(0);">You have not been assigned to a chain or hotel</a></li>
						@endif
						@elseif(System::isHotelMember())
						@if($hotel_view_data)
						@if($hotel_view_data->count()>=1)
						@foreach($hotel_view_data as $hotel)
						<li><a href="{{ route('manage/page', ['id' => $hotel->id]) }}"><i class="fa fa-bed"></i> {{ $hotel->name }}</a></li>
						@endforeach
						@endif
						@endif
						@else
						<li><a href="javascript:void(0);">You have not been assigned to a chain or hotel</a></li>
						@endif
					</ul>
				</div>
			</div>
			<script>
				var config = {
				'.chosen-select'           : {}
				}
				for (var selector in config) {
					$(selector).chosen(config[selector]);
				}
				$(document).on('click', '.chain_list', function(){
					$('.hotel_list').not(this).hide('slow');
					$(this).next('.hotel_list').toggle('slow');
				});
				$(document).on('click', '.hotelSelectRequired a', function(){
					if ($.trim($(this).attr('href')) == '#') { 
						$('#myModal6').modal('show');
						$('#hotel_list_chosen').css('width',300);
					}
				});
				$(document).on('click', '#modal_choose_hotel', function(){
					var url = $('#hotel_list :selected').data('url');
					if(url){
						window.location.replace(url);
					}
				});
				var $image = $(".image-crop > img")
				$($image).cropper({
					aspectRatio: 1.618,
					preview: ".img-preview",
					done: function(data) {
						$('#img_crop').val(JSON.stringify({
							"x": data.x,
							"y": data.y,
							"width": data.width,
							"height":data.height
						}));
					}
				});

				var $inputImage = $("#inputImage");
				if (window.FileReader) {
					$inputImage.change(function() {
						var fileReader = new FileReader(),
						files = this.files,
						file;

						if (!files.length) {
							return;
						}

						file = files[0];

						if (/^image\/\w+$/.test(file.type)) {
							fileReader.readAsDataURL(file);
							fileReader.onload = function () {
								$inputImage.val("");
								$image.cropper("reset", true).cropper("replace", this.result);
								$('#image_crop_path').val(this.result);
							};
						} else {
							showMessage("Please choose an image file.");
						}
					});
				} else {
					$inputImage.addClass("hide");
				}
			</script>
			<style type="text/css">
				.grey-bg:nth-child(2n+1){
					background: #f9f9f9;
				}
			</style>
