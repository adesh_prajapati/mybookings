 <section id="contact" class="gray-section contact">
  <div class="container">
    <div class="row m-b-lg">
      <div class="col-lg-12 text-center">
        <div class="navy-line"></div>
        <h1>Contact Us</h1>
        <p>{{(isset($directory_setting)? strip_tags($directory_setting->intro_text):'')}}</p>
      </div>
    </div>
    <div class="row m-b-lg">
      <div class="col-lg-3 col-lg-offset-3">
        <address>
          <strong><span class="navy">{{$hotel_data->name}}</span></strong><br/>
          {{$hotel_data->address_1}}<br/>
          {{$hotel_data->city}}, {{$hotel_data->country}} {{$hotel_data->postal_code}}<br/>
          <abbr title="Phone">P:</abbr> {{$hotel_data->phone}}
        </address>
      </div>
      <div class="col-lg-4">
        <p class="text-color">
          Consectetur adipisicing elit. Aut eaque, totam corporis laboriosam veritatis quis ad perspiciatis, totam corporis laboriosam veritatis, consectetur adipisicing elit quos non quis ad perspiciatis, totam corporis ea,
        </p>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-12 text-center">
        <a href="mailto:test@email.com" class="btn btn-primary directory-button">Send us mail</a>
        <p class="m-t-sm">
          Or follow us on social media
        </p>
        <ul class="list-inline social-icon">
          @if($hotel_data->social_facebook)
          <li><a href="{{$hotel_data->social_facebook}}" target="_blank"><i class="fa fa-facebook"></i></a>
          </li>
          @endif
          @if($hotel_data->social_twitter)
          <li><a href="{{$hotel_data->social_twitter}}" target="_blank"><i class="fa fa-twitter"></i></a>
          </li>
          @endif
          @if($hotel_data->social_instagram)
          <li><a href="{{$hotel_data->social_instagram}}" target="_blank"><i class="fa fa-instagram"></i></a>
          </li>
          @endif
        </ul>
      </div>
    </div>
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2 text-center m-t-lg m-b-lg">
          <p><strong>&copy; <?php echo date('Y');?> <a href="http://www.mybookings.com" title="MyBookings: online marketing for hotels">MyBookings.com</a> & {{ $hotel_data->name }}</strong><br /><a href="#">Disclaimer & Privacy Statement</a></p>
      </div>
    </div>
  </div>
</section>