<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>MyBookings.com</title>
    
    <!-- Main scripts -->
    <script src="{{ URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="{{ URL::asset('asset/js/fontawesome-iconpicker.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/parsley.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/chosen/chosen.jquery.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>
     
    <!-- Bootstrap core CSS -->
    <link href="{{ URL::asset('asset/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/animate.css') }}" rel="stylesheet">
    <!-- Animation CSS -->
    <link href="{{ URL::asset('asset/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="{{ URL::asset('asset/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/additional.css') }}" rel="stylesheet">
</head>