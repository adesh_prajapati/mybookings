<!DOCTYPE html>
<html>
@include('layouts.head')
	<body class="gray-bg">
	@yield('content')
		<script src="{{ URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
		<script src="{{ URL::asset('asset/js/bootstrap.min.js') }}"></script>
	</body>
</html>