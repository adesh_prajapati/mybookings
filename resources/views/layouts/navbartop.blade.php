<div class="row border-bottom">
    <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <!-- <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a> -->
        </div>
        <ul class="nav navbar-top-links navbar-right">
            <li>
                <a class="right-sidebar-toggle">
                    <strong>
                    @if (System::isHotelSession()) 
                    {{ Session::get('hotel')['name'] }}  
                    @else
                    Please select a hotel 
                    @endif
                    </strong> <i class="fa fa-tasks"></i>
                </a>
            </li>
            <li>
            <a href="{{ URL::route('logout')}}">
                    <i class="fa fa-sign-out"></i> Log out
                </a>
            </li>
        </ul>
    </nav>
</div>