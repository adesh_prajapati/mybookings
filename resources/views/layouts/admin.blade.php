<!DOCTYPE html>
<html>
@include('layouts.head')
  <body class="no-skin-config">
   <div id="wrapper">
    @include('layouts.navbar')
    <div id="page-wrapper" class="gray-bg">
    @yield('content')
    @include('layouts.footer')
    </div>
    @include('layouts.rightsidebar')
  </div>
  </body>
</html>