<head>
    
    <!-- Main scripts -->
    <script src="{{ URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="{{ URL::asset('asset/js/fontawesome-iconpicker.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/parsley.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/chosen/chosen.jquery.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/summernote/summernote.min.js') }}"></script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyBookings</title>
    <!--<link href="{{ URL::asset('asset/css/bootstrap.min.css') }}" rel="stylesheet">-->
    <link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">
    <link href="{{ URL::asset('asset/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/steps/jquery.steps.css') }}">
    <link href="{{ URL::asset('asset/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/style.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/summernote/summernote.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/summernote/summernote-bs3.css') }}">
    <link href="{{ URL::asset('asset/css/plugins/chosen/chosen.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/plugins/switchery/switchery.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('asset/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

    <link href="{{ URL::asset('asset/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/intelinput/intlTelInput.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/footable/footable.core.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/dropzone/basic.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/dropzone/dropzone.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/colorpicker/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/cropper/cropper.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/toastr/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('asset/css/overrides.css') }}">
</head>