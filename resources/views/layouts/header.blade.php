<div class="navbar-wrapper">
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a class="page-scroll" href="{{url($hotel_data->slug)}}">Home</a></li>
                        @foreach($navigation_menu as $menu)
                        <li><a class="page-scroll" href="{{$menu->action}}">{{$menu->title}}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </nav>
    </div>
    @if(isset($directory_setting))
    <style type="text/css">
        @if($directory_setting->button_color)
        .directory-button, .landing-page .directory-button{
            background: {{$directory_setting->button_color}};
            color: {{$directory_setting->text_link_color}};
        }
        .landing-page .btn-primary.btn-xxl{
            background: {{$directory_setting->button_color}};
        }
        @endif

        @if($directory_setting->button_text_color)
        .landing-page .filter-text{
            color: {{$directory_setting->button_text_color}};
        }
        .landing-page .services h2{
            color: {{$directory_setting->button_text_color}};
        }
        .mainbuttons .fa {
            color: {{$directory_setting->button_text_color}};
        }
        .mainbuttons .col-sm-4>a{
            color: {{$directory_setting->button_text_color}};
        }
        @endif
        @if($directory_setting->button_text_hover_color)
        .landing-page .services h2:hover{
            color: {{$directory_setting->button_text_hover_color}};
        }
        .mainbuttons .fa:hover {
            color: {{$directory_setting->button_text_hover_color}};
        }
        .mainbuttons .col-sm-4> a:hover{
        color: {{$directory_setting->button_text_hover_color}};
        }
        @endif
        @if($directory_setting->button_hover_color)
        .directory-button, .landing-page .directory-button:hover{
            background: {{$directory_setting->button_hover_color}};
            color: {{$directory_setting->text_link_hover_color}};
        }
        @endif

        @if($directory_setting->text_link_color)
        .landing-page .navbar-default .nav li a{
            color: {{$directory_setting->text_link_color}};
        }
        .landing-page .navbar-scroll.navbar-default .nav li .page-scroll{
            color: {{$directory_setting->text_link_color}};
        }
        .landing-page .navbar-default .nav li .page-scroll:hover{
            color: {{$directory_setting->text_link_hover_color}};
        }
        .landing-page .navbar-scroll.navbar-default .nav li a .page-scroll:hover{
            color: {{$directory_setting->text_link_hover_color}};
        }
        @endif

        @if($directory_setting->text_color)
        .landing-page .col-sm-4 > a h2{
            color: {{$directory_setting->text_color}};
        }
        .landing-page p.text-color{
            color: {{$directory_setting->text_color}};
        }
        .landing-page .carousel-caption > h1{
            color: {{$directory_setting->text_color}};
            background: rgba(0, 0, 0, 0.1) none repeat scroll 0 0;
            padding: 10px;
        }
        .landing-page .carousel-caption > p{
            color: {{$directory_setting->text_color}};
        }
        .landing-page .faq-question{
            color: {{$directory_setting->text_color}};
        }
        .mainbuttons .floor-title{
            color: {{$directory_setting->text_color}};
        }
        @endif

        @if($directory_setting->sub_heading_color)
        .landing-page span.navy{
            color: {{$directory_setting->sub_heading_color}};
        }  
        @endif
        @if($directory_setting->text_color)
        .landing-page .navy-line{
            color: {{$directory_setting->text_color}};
        }
         .landing-page h3{
            color: {{$directory_setting->text_color}};
         }
        .landing-page .text-line{
            color: {{$directory_setting->text_color}};
        }
        .landing-page section p{
          color: {{$directory_setting->text_color}};
      }
      .landing-page address{
        color: {{$directory_setting->text_color}};
      }
      .mainbuttons .forum-item-title{
        color: {{$directory_setting->text_color}};
      }
    @endif


    @if($directory_setting->sub_heading_color)
    .mainbuttons .forum-sub-title{
        color: {{$directory_setting->sub_heading_color}};
    }
    @endif
    @if($directory_setting->title_color)
    .landing-page h1{
        color: {{$directory_setting->title_color}};
    }
    .landing-page .col-lg-12.text-center > h1{
        color: {{$directory_setting->title_color}};
    }
    .mainbuttons .forum-title{
        color: {{$directory_setting->title_color}};
    }
    @endif
    @if($directory_setting->icon_color)
    .mainbuttons .forum-item.active .fa{
        color: {{$directory_setting->icon_color}};
    }
    @endif
</style>
@endif