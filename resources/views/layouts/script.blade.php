
    <!-- Custom and plugin javascript -->
    <script src="{{ URL::asset('asset/js/inspinia.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/pace/pace.min.js') }}"></script>
    <!-- jQuery UI -->
    <script src="{{ URL::asset('asset/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!--<script src="{{ URL::asset('asset/js/bootstrap.min.js') }}"></script>-->
     
    <script src="{{ URL::asset('asset/js/plugins/intelinput/intlTelInput.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/maskedinput.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/iCheck/icheck.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/switchery/switchery.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/wow/wow.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/cropper/cropper.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/footable/footable.all.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/staps/jquery.steps.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/validate/jquery.validate.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/dropzone/dropzone.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/masonary/masonry.pkgd.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/plugins/toastr/toastr.min.js') }}"></script>
     <script src="{{ URL::asset('asset/js/autocomplete.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/jquery.vSort.js') }}"></script>
     <script src="{{ URL::asset('asset/js/require.js') }}"></script>
     
     
     