@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h2>Add Hotspots / Contact</h2>
			</div>
			<div class="ibox-content">
				<form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post" action="{{ URL::route('directory/hotspots/create')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<h1>Create Hotspots</h1>
					<fieldset>
						<div class="row">
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.title') }}
								</label>
								<div class="col-sm-10">
									<input type="hidden" id="hotspots_id" name="hotspots_id" value="{{(isset($hotspots_data)? $hotspots_data->id: '')}}" />

									<input id="title" placeholder="{{ trans('form.title') }}" name="title" type="text" class="form-control"value="{{(isset($hotspots_data)? $hotspots_data->title: '')}}" >
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.address') }}
								</label>
								<div class="col-sm-10">
									<div class="input-group">
										<input type="hidden" name="latlong" id="lat_long" value="{{(isset($hotspots_data)? $hotspots_data->latlong: '')}}"/>
										<input name="address" id="address" type="text" class="form-control" placeholder="{{ trans('form.address') }}" value="{{(isset($hotspots_data)? $hotspots_data->address: '')}}"> 
										<span class="input-group-btn search-map"> 
											<button id="find_address" class="btn btn-primary" 
											type="button" data-toggle="modal">
											<i class="fa fa-search"></i>
										</button> 
									</span>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">{{ trans('form.description') }}
							</label>
							<div class="col-sm-10">
								<div class="ibox float-e-margins">
									<div class="ibox-content no-padding">
										<textarea name="description" class="summernote">
											{{(isset($hotspots_data)? $hotspots_data->description: '')}}
										</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">{{ trans('form.website') }}</label>
							<div class="col-sm-4">
								<input type="text" id="urlBanner" data-parsley-type="url" name="website" data-parsley-required-message="Website is required" data-parsley-trigger="change focusout"  value="{{(isset($hotspots_data)? $hotspots_data->website: '')}}" placeholder="{{ trans('form.website') }}" class="form-control">
							</div>
							<label class="col-sm-2 control-label">{{ trans('form.category') }}</label>
							<div class="col-sm-4">
								{!! Form::select('category', $hotsposts_category, null,['class' => 'chosen-select1 form-control','data-placeholder'=>'Choose a Category...','value'=>"",'name' => 'category']) !!}
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">
								{{ trans('form.phone') }}</label>
								<div class="col-sm-4">
									<input type="text" id="phone"  name="phone" value="{{(isset($hotspots_data)? $hotspots_data->phone: '')}}"  class="form-control">
								</div>
								<label class="col-sm-1 control-label"></label>
								<div class="col-sm-3">
									<div class="i-checks"><label>
										@if(isset($hotspots_data))
										<input data-rel="hotspots" type="checkbox" id="show_as_hotspot" 
										name="show_as_hotspot" value="on" data-parsley-group="block1" 
										{{($hotspots_data->show_as_hotspots == 1)? 'checked=""': ''}} > <i></i>
										@else
										<input data-rel="hotspots" type="checkbox" id="show_as_hotspot" 
										name="show_as_hotspot" value="on" data-parsley-group="block1"> <i></i>
										@endif 
										{{ trans('form.show_as_hotspots') }}</label></div>
									</div>
									<div class="col-sm-2">
										<div class="i-checks">
											<label> 
												@if(isset($hotspots_data))
												<input data-rel="published" type="checkbox" id="itself-chain" 
												name="is_published" value="on" data-parsley-group="block1"{{($hotspots_data->is_published == 1)? 'checked=""': ''}}> <i></i>
												@else
												<input data-rel="published" type="checkbox" id="itself-chain" 
												name="is_published" 
												value="on" data-parsley-group="block1" checked=""><i></i>
												@endif
												{{ trans('form.published') }}
											</label></div>
										</div>
									</div>
								</div>
							</fieldset>
							<h1>Finish</h1>
							<fieldset>
								<h2>Terms and Conditions</h2>
								<input id="acceptTerms" name="acceptTerms" type="checkbox"> 
								<label for="acceptTerms">I agree with the Terms and Conditions.</label>
							</fieldset>
						</form>
					</div>
					<div class="modal inmodal fade" id="find_hotspots" tabindex="-1" role="dialog"  aria-hidden="true">
						<div class="modal-dialog modal-lg">
							<div class="modal-content">
								<div class="modal-body">
									<div class="ibox-content">
										<div class="google-map" id="map2">

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.script')
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70&libraries=places">
		</script>
		<script>
			$(document).ready(function(){
				$("#wizard").steps();
				$("#form").steps({
					bodyTag: "fieldset",
					onStepChanging: function (event, currentIndex, newIndex){
						if (currentIndex > newIndex){
							return true;
						}
						if (newIndex === 3 && Number($("#age").val()) < 18){
							return false;
						}
						var form = $(this);
						if (currentIndex < newIndex){
							$(".body:eq(" + newIndex + ") label.error", form).remove();
							$(".body:eq(" + newIndex + ") .error", form).removeClass("error");
						}
						form.validate().settings.ignore = ":disabled,:hidden";
						return form.valid();
					},
					onStepChanged: function (event, currentIndex, priorIndex){
						if (currentIndex === 2 && Number($("#age").val()) >= 18){
							$(this).steps("next");
						}
						if (currentIndex === 2 && priorIndex === 3){
							$(this).steps("previous");
						}
					},
					onCanceled:function(){
						window.location = '<?php echo route('directory/hotspots/list'); ?>';
					},
					onFinishing: function (event, currentIndex){
						var form = $(this);
						form.validate().settings.ignore = ":disabled";
						return form.valid();
					},onFinished: function (event, currentIndex){
						var form = $(this);
						form.submit();
					}
				}).validate({
					errorPlacement: function (error, element){
						if(element.attr('id') == 'address'){
							element.parent().after(error);
						}else{
							element.after(error);	
						}
					},
					rules: {
						website: {
							url: true
						},
						phone: {
							number: true
						}
					}
				});
				var elem = document.querySelector('.js-switch');
				var switchery = new Switchery(elem, { color: '#1AB394' });
				$('.summernote').summernote({height: 230});
				$('.i-checks').iCheck({
					checkboxClass: 'icheckbox_square-green'
				});
				var config = {
					'.chosen-select1'           : {},
				}
				for (var selector in config) {
					$(selector).chosen(config[selector]);
				}
				$('#show_as_hotspot').on('ifChecked', function(event){
					$("#address").attr("required", "true");
				});
				$("#urlBanner").change(function() {
					if (!/^http:\/\//.test(this.value)) {
						this.value = "http://" + this.value;
					}
				});
				$('#find_address').click(function () {
					var find_address_latlong = $('#address').val();
					var geocoder = new google.maps.Geocoder();
					geocoder.geocode({
						"address": find_address_latlong
					}, function(results) {
						var lat = results[0].geometry.location.lat();
						var long = results[0].geometry.location.lng();
						init(lat,long);
					});
					if(find_address_latlong){
						$('#find_hotspots').modal('show');
					}
				});
				var inputAddress = document.getElementById('address');
				var places = new google.maps.places.Autocomplete(inputAddress);
				google.maps.event.addListener(places, 'place_changed', function () {
					var place = places.getPlace();
					$('#lat_long').val([place.geometry.location.lat(),place.geometry.location.lng()]);
				});
				var hotspots_id = $('#hotspots_id').val();
				var hotspots_id = $('#hotspots_id').val();
				if(hotspots_id){
					$(".chosen-select1").val('{{isset($hotspots_data->category)? $hotspots_data->category : ''}}').trigger("chosen:updated");
				}
				
			});
function init(lat,long) {
	var mapOptions2 = {
		zoom: 11,
		center: new google.maps.LatLng(lat,long),
		styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
	};
	var mapElement2 = document.getElementById('map2');
	var map2 = new google.maps.Map(mapElement2, mapOptions2);
	var marker = new google.maps.Marker({
		position: new google.maps.LatLng(lat, long),
		map: map2
	});
}
</script>
@stop