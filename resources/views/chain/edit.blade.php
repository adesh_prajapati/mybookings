@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-9" id="edit-chain">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ trans('form.edit') }} Chain</h5>
				</div>
				<div class="ibox-content">
					<form id="update-chain" data-parsley-validate class="form-horizontal" method="post" action="{{ URL::route('add/chain')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" name="chain_id" value="{{ $chain_info->id }}"/>
						<input type="hidden" id="phone-number" value="{{ $chain_info->phone }}" name="phone_number"/>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.email') }}</label>
							<div class="col-lg-10"><input type="email" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" name="email" placeholder="{{ trans('form.email') }}" value="{{ $chain_info->email }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.name') }}</label>

							<div class="col-lg-10"><input type="text" name="name" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout"  placeholder="Name" value="{{ $chain_info->name }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_1') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout"  name="address_1" value="{{ $chain_info->address_1 }}" placeholder="{{ trans('form.address_line_1') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_2') }}</label>

							<div class="col-lg-10"><input type="text" name="address_2" value="{{ $chain_info->address_2 }}" placeholder="{{ trans('form.address_line_2') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.province') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-required-message="Province is required" data-parsley-trigger="change focusout"  name="province" value="{{ $chain_info->province }}" placeholder="{{ trans('form.province') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.postal_code') }}</label>

							<div class="col-lg-10"><input type="text" name="postal_code" data-parsley-required-message="Postal code is required" data-parsley-trigger="change focusout"   value="{{ $chain_info->postal_code }}" placeholder="{{ trans('form.postal_code') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.city') }}</label>

							<div class="col-lg-10"><input type="text" name="city" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" placeholder="{{ trans('form.city') }}" value="{{ $chain_info->city }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.country') }}</label>

							<div class="col-lg-10">
								{!! Form::select('country', $country,null,['class' => 'chosen-select-1 form-control','data-placeholder'=>'Choose a Country...']) !!}
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.phone') }}</label>

							<div class="col-lg-6">
								<input type="input" id="phone"  name="phone" data-parsley-required-message="Phone is required" data-parsley-trigger="change focusout"  value="{{ $chain_info->phone }}"  class="form-control" required="">
								<span class="hide" id="valid-msg">✓ Valid</span>
								<span class="hide" id="error-msg">Invalid number</span>
							</div>
							<div class="col-lg-4">
								<div id="message"></div>
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.website') }}</label>
							<div class="col-lg-10">
							<input id="urlBanner" data-parsley-type="url" type="text" name="website" data-parsley-required-message="Website is required" data-parsley-trigger="change focusout"  value="{{ $chain_info->website }}" placeholder="{{ trans('form.website') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">{{ trans('form.status') }}</label>
							<div class="col-lg-10">
								<div class="radio radio-success">
									<input type="radio" id="inlineRadio1" value="Active" name="status" {{ ($chain_info->status == 'Active') ? 'checked=""' : '' }}>
									<label for="inlineRadio1">Active</label>
								</div>
								<div class="radio radio-danger">
									<input type="radio" value="Inactive" name="status" {{($chain_info->status == 'Inactive') ? 'checked=""' : '' }}>
									<label for="inlineRadio2">Inactive</label>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<a type="button" class="btn btn-white" href="{{route('chain/list')}}"><i class="fa fa-times"> </i> {{ trans('form.cancel') }}</a>
								<button type="submit" name="update" value="update" class="btn btn-primary"><i class="fa fa-save"> </i> {{ trans('form.update') }}</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox">
					<div class="ibox-content">
						@if(System::isAdmin())
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Members</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<form id="add-member" method="post" action="{{ URL::route('assign/user')}}">
								<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
								<input type="hidden" name="entity_id" value="{{ $chain_info->id }}"/>
								<div class="col-lg-8">
									{!! Form::select('user_id', ['' => 'Choose member'] + $user_list,null,['id'=>'choose-member','class' => 'chosen-select form-control','data-placeholder'=>'Choose a Member...']) !!}
								</div>
								<div class="col-lg-4">
									<button type="submit" name="submit_role" value="chain" class="btn btn-primary">{{ trans('form.add') }}</button>
								</div>
								<div class="col-lg-12">
									<label class="error custom-error" style="display:none;"> Please choose a member...</label>
								</div>
							</form>
						</div>
						@endif
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Assign Members</h3>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<tbody>
											@if($chain_info->chainmember->count()!=0)
											@foreach ($chain_info->chainmember as $chain)
											<tr><td>{{ $chain->user->first_name }}
												@if(System::isAdmin())
												<a data-chain="{{ $chain_info->id}}" data-user="{{$chain->user->id}}" data-role="{{$chain->role_id}}" class="close-link pull-right my-cross-link"><i class="fa fa-times"></i></a>
												@endif
											</td></tr>
											@endforeach
											@else
											<td>Not yet assign user</td>
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
	});
	$(".chosen-select-1").val('{{$chain_info->country}}').trigger("chosen:updated");
	var telInput = $("#phone"),
	errorMsg = $("#error-msg"),
	validMsg = $("#valid-msg");
	telInput.intlTelInput({
		utilsScript: "{{ URL::asset('asset/js/plugins/intelinput/utils.js') }}",
		customPlaceholder:function(selectedCountryPlaceholder, selectedCountryData) {
			$('#message').html(selectedCountryPlaceholder);
			return selectedCountryPlaceholder;
		}
	});
	var reset = function() {
		telInput.removeClass("error");
		errorMsg.addClass("hide");
		validMsg.addClass("hide");
	};
// on blur: validate
telInput.blur(function() {
	reset();
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			validMsg.removeClass("hide");
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
		} else {
			errorMsg.removeClass("hide");
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
		}
	}
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
$(document).ready(function(){
	var data = $("#phone").intlTelInput("getSelectedCountryData");
	var phone = '{{ $chain_info->phone }}';
	$('#phone').val(phone.replace('+'+data.dialCode,''));
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-1'           : {},
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}

	$("#urlBanner").change(function() {
		if (!/^http:\/\//.test(this.value)) {
			this.value = "http://" + this.value;
		}
	});
	$( "#add-member" ).submit(function( event ) {
		if($( "#choose-member" ).val()==''){
			$('.custom-error').show();
			event.preventDefault();
			return;
		}
	});
});
function edit(){
	$('#edit-chain').show();
	$('#chain-info').css('display','none');
	$(".chosen-select-1").val('{{$chain_info->country}}').trigger("chosen:updated");
}
function reload(){
	location.reload();
}
function deleteChain(){
	$('#delete-chain').modal('show'); 
}
$('.close-link').click(function () {
	var user_id  = $(this).data('user');
	var role_id = $(this).data('role');
	var chain_id = $(this).data('chain');
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this User!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('remove/access'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {user_id : user_id, chain_id:chain_id,entity_type:'chain',role_id:role_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "User was successfully removed to Chain.", "success");
							window.location.reload();
						}
					}
				});
			} else {
				swal("Cancelled", "Hotel is safe :)", "error");
			}
		});
});
function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
	if (event.keyCode === 8 || event.keyCode === 46
		|| event.keyCode === 37 || event.keyCode === 39) {
		return true;
}
else if ( key < 48 || key > 57 ) {
	return false;
}
else return true;
};
$('#update-chain').parsley().on('form:validate', function (formInstance) {
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
			formInstance.validationResult = true;
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			formInstance.validationResult = false;
		}
	}
});
$("#update-chain").submit( function(eventObj) {
	$('#phone-number').val($("#phone").intlTelInput("getNumber"));
	return true;
});
</script>
<style type="text/css">
	.chosen-container.chosen-container-single{
		width:200px !important;
	}
</style>
@stop