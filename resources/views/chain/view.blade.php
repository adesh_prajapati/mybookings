@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-9" id="chain-info">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									@if(System::isAdmin())
									<a id="delete_chain" class="btn btn-danger pull-right" data-chain="{{$chain_info->id}}"><i class="fa fa-trash"></i> {{ trans('form.delete') }}</a>
									@endif
									<a class="btn btn-primary pull-right m-r-md" href="{{ route('chain/edit', ['id' => $chain_info->id]) }}"><i class="fa fa-edit"></i> {{ trans('form.edit') }}</a>
									<h2>{{ $chain_info->name }} </h2>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<dl class="dl-horizontal">
									<dt>{{ trans('form.name') }}:</dt> <dd>  {{ $chain_info->name }}</dd>
									<dt>{{ trans('form.email') }}:</dt> <dd><a href="mailto:{{ $chain_info->email }}" class="text-navy">{{ $chain_info->email }}</a> </dd>
									<dt>{{ trans('form.phone') }}:</dt> <dd><a href="tel:{{ $chain_info->phone }}" class="text-navy">{{ $chain_info->phone }}</a> </dd>
									<dt>{{ trans('form.city') }}:</dt> <dd>{{ $chain_info->city }}</dd>
									<dt>{{ trans('form.country') }}:</dt> <dd>{{ $chain_info->country }}</dd>
								</dl>
							</div>
							<div class="col-lg-7" id="cluster_info">
								<dl class="dl-horizontal" >
									<dt>{{ trans('form.address') }}:</dt> <dd>{{ $chain_info->address_1 }}</dd>
									<dt>{{ trans('form.postal_code') }}:</dt> <dd>{{ $chain_info->postal_code }}</dd>
									<dt>{{ trans('form.website') }}:</dt> <dd><a href="{{ $chain_info->website }}" target="_blank" class="text-navy">{{ $chain_info->website }}</a></dd>
								</dl>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox">
					<div class="ibox-content">
						@if(System::isAdmin())
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Members</h3>
								</div>
							</div>
						</div>
						<div class="row">
							<form id="add-member" method="post" action="{{ URL::route('assign/user')}}">
								<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
								<input type="hidden" name="entity_id" value="{{ $chain_info->id }}"/>
								<div class="col-lg-8">
									{!! Form::select('user_id', ['' => 'Choose member'] + $user_list,null,['id'=>'choose-member','class' => 'chosen-select form-control','data-placeholder'=>'Choose a Member...']) !!}
								</div>
								<div class="col-lg-4">
									<button type="submit" name="submit_role" value="chain" class="btn btn-primary">{{ trans('form.add') }}</button>
								</div>
								<div class="col-lg-12">
									<label class="error custom-error" style="display:none;"> Please choose a member...</label>
								</div>
							</form>
						</div>
						@endif
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Assign Members</h3>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<tbody>
											@if($chain_info->chainmember->count()!=0)
											@foreach ($chain_info->chainmember as $chain)
											<tr><td>{{ $chain->user->first_name }}
												@if(System::isAdmin())
												<a data-chain="{{ $chain_info->id}}" data-user="{{$chain->user->id}}" data-role="{{$chain->role_id}}" class="close-link pull-right my-cross-link"><i class="fa fa-times"></i></a>
												@endif
											</td></tr>
											@endforeach
											@else
											<td >Not yet assign user</td>
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
	});
	var telInput = $("#phone"),
	errorMsg = $("#error-msg"),
	validMsg = $("#valid-msg");
	telInput.intlTelInput({
		utilsScript: "{{ URL::asset('asset/js/plugins/intelinput/utils.js') }}",
		customPlaceholder:function(selectedCountryPlaceholder, selectedCountryData) {
			$('#message').html(selectedCountryPlaceholder);
			return selectedCountryPlaceholder;
		}
	});
	var reset = function() {
		telInput.removeClass("error");
		errorMsg.addClass("hide");
		validMsg.addClass("hide");
	};
// on blur: validate
telInput.blur(function() {
	reset();
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			validMsg.removeClass("hide");
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
		} else {
			errorMsg.removeClass("hide");
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
		}
	}
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
$(document).ready(function(){
	var data = $("#phone").intlTelInput("getSelectedCountryData");
	var phone = '{{ $chain_info->phone }}';
	$('#phone').val(phone.replace('+'+data.dialCode,''));
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-1'           : {},
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}

	$("#urlBanner").change(function() {
		if (!/^http:\/\//.test(this.value)) {
			this.value = "http://" + this.value;
		}
	});
	$( "#add-member" ).submit(function( event ) {
		if($( "#choose-member" ).val()==''){
			$('.custom-error').show();
			event.preventDefault();
			return;
		}
	});
});
function edit(){
	$('#edit-chain').show();
	$('#chain-info').css('display','none');
	$(".chosen-select-1").val('{{$chain_info->country}}').trigger("chosen:updated");
}
function reload(){
	location.reload();
}
$('#delete_chain').click(function () {
	var chain_id = $(this).data('chain');
	swal({
		title: "Are you sure?",
		text: "This chain will be deleted permanently along with all its hotels!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('chain/delete'); ?>";
				var redirect_route = '<?php echo route('chain/list'); ?>';
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {chain_id : chain_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "Chain successfully deleted.", "success");
							var URL = redirect_route;
							window.location = redirect_route;
						}
					}
				});
			} else {
				swal("Cancelled", "chain is safe :)", "error");
			}
		});
});
$('.close-link').click(function () {
	var user_id  = $(this).data('user');
	var role_id = $(this).data('role');
	var chain_id = $(this).data('chain');
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this User!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel pl!",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('remove/access'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {user_id : user_id, chain_id:chain_id,entity_type:'chain',role_id:role_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "User was successfully removed to Chain.", "success");
							window.location.reload();
						}
					}
				});
			} else {
				swal("Cancelled", "User is safe :)", "error");
			}
		});
});

function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
	if (event.keyCode === 8 || event.keyCode === 46
		|| event.keyCode === 37 || event.keyCode === 39) {
		return true;
}
else if ( key < 48 || key > 57 ) {
	return false;
}
else return true;
};
$('#update-chain').parsley().on('form:validate', function (formInstance) {
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
			formInstance.validationResult = true;
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			formInstance.validationResult = false;
		}
	}
});
$("#update-chain").submit( function(eventObj) {
	$('#phone-number').val($("#phone").intlTelInput("getNumber"));
	return true;
});
</script>
<style type="text/css">
	.chosen-container.chosen-container-single{
		width:200px !important;
	}
</style>
@stop