@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="wrapper wrapper-content animated fadeInUp">

				<div class="ibox">
					<div class="ibox-title">
						<h5>All Chain assigned to this account</h5>
						@if(System::isAdmin())
						<div class="ibox-tools">
							<a href="{{ route('chain/create') }}" class="btn btn-primary btn-sm">
                <i class="fa fa-plus"></i>
                Create new Chain</a>
              </div>
              @endif
            </div>

            @if($chain_lists->count()!=0)
            <div class="ibox-content">
              <div class="project-list">
               <table class="footable table table-stripped toggle-arrow-tiny" 
               data-page-size="15">
               <thead>
                <tr>
                  <th data-toggle="true">Status</th>
                  <th data-hide="name">Name</th>
                  <th data-hide="email">Email</th>
                  <th data-hide="website">Website</th>
                  <th class="text-right" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tbody>
               @foreach ($chain_lists as $index => $chain)
               <tr>
                <td>
                  @if($chain->status == 'Active')
                  <span class="label label-primary">{{$chain->status}}</span>
                  @else
                  <span class="label label-default">{{$chain->status}}</span>
                  @endif
                </td>
                <td>
                 <a href="{{ route('chain/view', ['id' => $chain->id]) }}">{{ $chain->name }}</a>
                 <br/>
               </td>
               <td>
                 <a href="mailto:{{ $chain->email }}" class="text-navy">{{ $chain->email }}</a>
                 <br/>
               </td>
               <td>
                 <a href="{{ $chain->website }}" target="_blank" class="text-navy"><i class="fa fa-sitemap"> </i> {{ $chain->website }}</a>
                 <br/>
               </td>
               <td class="text-right">
                <div class="btn-group">
                 <a href="{{ route('chain/view', ['id' => $chain->id]) }}" class="btn btn-white btn-xs"><i class="fa fa-folder"></i> View </a>
                 <a href="{{ route('chain/edit', ['id' => $chain->id]) }}" class="btn btn-white btn-xs"><i class="fa fa-pencil"></i> Edit </a>
               </div>
             </td>
           </tr>
           @endforeach
         </tbody>
         <tfoot>
          <tr>
            <td colspan="6">
              <ul class="pagination pull-right"></ul>
            </td>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
  @else
  <br/>
  <div class="ibox-content m-b-sm border-bottom">
    <div class="text-center p-lg">
      <h2>No Chain available</h2>

    </div>
  </div>    
  @endif
</div>
</div>
</div>
</div>
</div>
@include('layouts.script')
<script type="text/javascript">
  $(document).ready(function() {
    $('.footable').footable();
  });
</script>
@stop