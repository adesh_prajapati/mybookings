    @extends('layouts.public')
    @section('content')
    <div class="middle-box text-center loginscreen animated fadeInDown">
       @if ($errors->has())
        <div class="alert alert-danger">
            @foreach ($errors->all() as $error)
            {{ $error }}<br>        
            @endforeach
        </div>
        @endif
        <div>
            <div>
                <h1 class="logo-name">MB</h1>
            </div>
            <form class="m-t" role="form" action="{{ URL::route('login')}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
                <div class="form-group">
                    <input type="email" name="email" class="form-control" placeholder="Username" required="">
                </div>
                <div class="form-group">
                    <input type="password" name="password" class="form-control" placeholder="Password" required="">
                </div>
                <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
                <a href="#"><small>Forgot password?</small></a>
            </form>
        </div>
    </div>
    @stop
   
