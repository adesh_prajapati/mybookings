@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="modal inmodal fade in" id="find_hotspots" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="ibox-content">
					<form method="post" class="form-horizontal" action="{{ URL::route('directory/faq/create')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" id="faq_id" name="faq_id" value=""/>
						<div class="form-group">
							<label class="col-sm-2 control-label">{{ trans('form.question') }}</label>
							<div class="col-sm-10">
								<input type="text" id="question" class="form-control" 
								placeholder="title" name="question" required="">
							</div>
						</div>
						<div class="hr-line-dashed"></div>
						<div class="form-group">
							<label class="col-sm-2 control-label">{{ trans('form.answer') }}
							</label>
							<div class="col-sm-10">
								<div class="ibox float-e-margins">
									<div class="ibox-content no-padding border-custom">
										<textarea name="answer" class="form-control summernote">

										</textarea>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-4 col-sm-offset-2">
								<a class="btn btn-white" type="button" href="{{route('directory/faq')}}">Cancel</a>
								<button class="btn btn-primary" type="submit">Save</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<div class="col-sm-4">
			<h2>FAQs</h2>
		</div>
		<div class="col-sm-8">
			<div class="title-action">
				<a class="btn btn-primary btn-sm" title="Create new cluster" data-toggle="modal" id="add_question">
					<i class="fa fa-plus"></i> 
					<span class="bold">Add question</span>
				</a>
			</div>
		</div>
		
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="wrapper wrapper-content animated fadeInRight">
			@if($faq_page_data->count())
			@foreach($faq_page_data as $faq_page)
			<div class="faq-item" id="faq_item_{{$faq_page->id}}">
				<div class="row">
					<div class="col-md-10">
						<a data-toggle="collapse" href="#faq{{$faq_page->id}}" 
							id="faq_ques_{{$faq_page->id}}" class="faq-question" 
							data-val="{{$faq_page->question}}">
							{{$faq_page->question}}
						</a>
					</div>
					<div class="col-md-2">
						<div class="btn-group">
							<a class="btn-white btn btn-xs edit" data-rel="{{$faq_page->id}}">
								<i class="fa fa-edit"></i> Edit</a>
								<a class="btn-white btn btn-xs delete" 
								data-rel="{{$faq_page->id}}">
								<i class="fa fa-trash"></i> Delete</a>
							</div>
								<!-- <a class="pull-right delete" data-rel="{{$faq_page->id}}">
									<i class="fa fa-trash fa-2x"></i></a>
									<a class="pull-right edit" data-rel="{{$faq_page->id}}">
										<i class="fa fa-pencil-square-o fa-2x"></i>
									</a> -->
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12">
									<div id="faq{{$faq_page->id}}" class="panel-collapse collapse ">
										<div class="faq-answer" id="answer_{{$faq_page->id}}" data-val="{{$faq_page->answer}}">
											<p>
												{{$faq_page->answer}}
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endforeach
						@else
						<div class="ibox-content m-b-sm border-bottom">
							<div class="text-center p-lg">
								<h2>No FAQs available</h2>

							</div>
						</div>
						@endif
					</div>
				</div>
			</div>
			@include('layouts.script')
			<script>
				$(document).ready(function(){
					$('.summernote').summernote({height:250});
					$('#add_question').click(function () {
						$('#question').val('');
						$('.note-editable').html('');
						$('#find_hotspots').modal('show');
					});
					$.ajaxSetup({
						headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
					});
					$('.edit').click(function () {
						var rel = $(this).data('rel');
						$('#faq_id').val(rel);
						$('#question').val($.trim($('#faq_ques_'+rel).data('val')));
						$('.note-editable').html($.trim($('#answer_'+rel).data('val')));
						$('#find_hotspots').modal('show');
					});
					$('.delete').click(function () {
						var faq_id = $(this).data('rel');
						swal({
							title: "Are you sure?",
							text: "Your will not be able to recover this FAQs!",
							type: "warning",
							showCancelButton: true,
							confirmButtonColor: "#DD6B55",
							confirmButtonText: "Yes, delete it!",
							cancelButtonText: "No, cancel plx!",
							closeOnConfirm: false,
							closeOnCancel: false },
							function (isConfirm) {
								if (isConfirm) {
									var route = "<?php echo route('directory/faq/delete'); ?>";
									$.ajax({
										url: route,
										type: 'POST',
										dataType: 'json',
										data: {faq_id : faq_id},
										success: function(result) {
											if(result.response == 'success'){
												$('#faq_item_'+faq_id).remove();
												swal("Deleted!", "FAQs has been deleted.", "success");
												window.location.reload();
											}
										}
									});
								} else {
									swal("Cancelled", "FAQs is safe :)", "error");
								}
							});
					});
				});
			</script>
			@stop