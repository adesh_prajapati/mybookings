@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
@if(System::isAdmin())
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins removeclasscol {{ old('first_name')?'':'collapsed'}}">
				<div class="ibox-title custom-collapse" style="cursor:pointer;">
					<h5 id="update-user">Create User</h5>
					<div class="ibox-tools">
						<a class="collapse-link-222">
							<i class="fa fa-chevron-up"></i>
						</a>
					</div>
				</div>
				<div class="ibox-content user-info-admin">
					<form class="" role="form" id="user-info" method="post" action="{{ URL::route('create/user')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" id="user-id" name="user_id" value=""/>
						<div class="row">
							<div class="form-group col-lg-4">
								<label for="first-name">{{ trans('form.first_name') }}</label>
								<input type="text" name="first_name" value="{{ old('first_name') }}" required="" class="form-control" id="first-name" placeholder="Enter {{ trans('form.first_name') }}"  >
							</div>
							<div class="form-group col-lg-4">
								<label for="last-name">{{ trans('form.last_name') }}</label>
								<input type="text" name="last_name" value="{{ old('last_name') }}" required="" class="form-control" id="last-name" placeholder="Enter {{ trans('form.last_name') }}">
							</div>
							<div class="form-group col-lg-4">
								<label for="phone">{{ trans('form.phone') }}</label>
								<input type="text" name="phone" value="{{ old('phone') }}" required="" class="form-control" id="phone" placeholder="{{ trans('form.phone') }}" >
							</div>

							<div class="form-group col-lg-4">
								<label for="email">{{ trans('form.email') }}</label>
								<input type="email" name="email" value="" required="" class="form-control" id="email" placeholder="Enter {{ trans('form.email') }}" autocomplete="false"/>
							</div>
							<div class="form-group col-lg-4">
								<label for="password">{{ trans('form.password') }}</label>
								<input type="password" name="password" required="" class="form-control" id="password" placeholder="{{ trans('form.password') }}" autocomplete="false"/>
							</div>
						</div>
						<div class="row">
							<button type="submit" id="submit-btn" class="btn btn-primary pull-right" value="create" name="submit">{{ trans('form.create') }} User</button>
							<button type="button" id="cancel-btn-admin" class="btn btn-white pull-right m-r-md"><i class="fa fa-times"> </i> {{ trans('form.cancel') }}</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		@if($user_list->count() > 1)
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-content">
					<input type="text" class="form-control input-sm m-b-xs" id="filter"
					placeholder="Search in table">
					<table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
						<thead>
							<tr>
								<th></th>
								<th>Name</th>
								<th>Phone No.</th>
								<th>Email</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($user_list as $index => $user)
							@if($user->roles['0']['slug']!='admin')
							<tr class="gradeX">
								<td class="client-avatar">
									@if($user->profile->image)
									<img alt="image" src="{{ URL::asset('uploads/'.$user->profile->image) }}"> 
									@else
									<img alt="image" src="http://dummyimage.com/50x50/e0e0e0/444.png&text={{substr($user->first_name, 0,1) .substr($user->last_name, 0,1)}}"> 
									@endif
								</td>
								<td><a data-toggle="tab" href="#contact-{{ $index }}" class="client-link">{{ $user->first_name }} {{ $user->last_name }}</a></td>
								<td><i class="fa fa-phone"></i> {{ $user->profile->phone }}</td>
								<td><i class="fa fa-envelope"></i> {{ $user->email }}</td>
								<td class="center"><span class="label label-primary">Active</span></td>
							</tr>
							@endif
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="5">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>
					
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox ">
				<div class="ibox-content">
					<div class="tab-content">
						@if($user_list)
						@foreach ($user_list as $index => $user)
						@if($user->roles['0']['slug']!='admin')
						<div id="contact-{{ $index }}" class="tab-pane @if ($index==1) {{'active'}} @endif">
							<div class="row m-b-lg">
								<div class="col-lg-4 text-center">
									<meta name="_token" content="{!! csrf_token() !!}"/>
									<h2>{{ $user->first_name }} {{ $user->last_name }}</h2>

									<div class="m-b-sm">
										@if($user->profile->image)
										<img alt="image" class="img-circle" src="{{ URL::asset('uploads/'.$user->profile->image) }}" style="width: 62px">
										@else
										<img alt="image" class="img-circle" src="http://dummyimage.com/50x50/e0e0e0/444.png&text={{substr($user->first_name, 0,1) .substr($user->last_name, 0,1)}}" style="width: 62px">
										@endif
									</div>
								</div>
								<div class="col-lg-8">
									<strong>
										Phone Number
									</strong>
									<p>
										{{ $user->profile->phone }}
									</p>
									<strong>
										Contact Email
									</strong>
									<p>
										{{ $user->email }}
									</p>
									<button type="button" class="btn btn-primary" onclick="geteditUserInfo({{$user->id}})">
										<i class="fa fa-pencil-square-o"></i> {{ trans('form.edit') }}
									</button>
									<button data-rel="{{$user->id}}" data-name="{{$user->first_name}}" type="button"  class="btn btn-danger delete-user" id="">
										<i class="fa fa-trash"></i> {{ trans('form.delete') }}
									</button>
									<div class="modal inmodal fade" id="confirm-user" tabindex="-1" role="dialog"  aria-hidden="true">
										<div class="modal-dialog modal-sm">
											<div class="modal-content">
												<div class="modal-body">
													<p><strong>{{ trans('validation.confirm_delete_user', ['name'=> $user->first_name]) }}</strong></p>
												</div>
												<div class="modal-footer">
													<input type="hidden" id="user-id">
													<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('form.cancel') }}</button>
													<button type="button" class="btn btn-danger" id="confirm-delete">{{ trans('form.delete') }}</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						@endif
						@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
		@else
		<div class="col-lg-12">
			<div class="ibox-content m-b-sm border-bottom">
				<div class="text-center p-lg">
					<h2>No Users available</h2>

				</div>
			</div>
		</div>
		
		@endif
	</div>
</div>
@else
<div class="wrapper wrapper-content">
	@if ($errors->has())
	<div class="alert alert-danger alert-dismissable">
		<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
		@foreach ($errors->all() as $error)
		{{ $error }}<br>        
		@endforeach
	</div>
	@endif
	@if (session('success'))
	<div class="alert alert-success alert-dismissable">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
		{{ session('success') }}
	</div>
	@endif
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins update-info-user  removeclasscol " style="display:none">
				<div class="ibox-title">
					<h5 id="update-user">Update User</h5>
				</div>
				<div class="ibox-content">
					<form class="" role="form" id="user-info" method="post" action="{{ URL::route('create/user')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" id="user-id" name="user_id" value=""/>
						<div class="row">
							<div class="form-group col-lg-4">
								<label for="first-name">{{ trans('form.first_name') }}</label>
								<input type="text" name="first_name" value="{{ old('first_name') }}" required="" class="form-control" id="first-name" placeholder="Enter {{ trans('form.first_name') }}"  >
							</div>
							<div class="form-group col-lg-4">
								<label for="last-name">{{ trans('form.last_name') }}</label>
								<input type="text" name="last_name" value="{{ old('last_name') }}" required="" class="form-control" id="last-name" placeholder="Enter {{ trans('form.last_name') }}">
							</div>
							<div class="form-group col-lg-4">
								<label for="phone">{{ trans('form.phone') }}</label>
								<input type="text" name="phone" value="{{ old('phone') }}" required="" class="form-control" id="phone" placeholder="{{ trans('form.phone') }}" >
							</div>

							<div class="form-group col-lg-4">
								<label for="email">{{ trans('form.email') }}</label>
								<input type="email" name="email" value="{{ old('email') }}" required="" class="form-control" id="email" placeholder="Enter {{ trans('form.email') }}" >
							</div>
							<div class="form-group col-lg-4">
								<label for="password">{{ trans('form.password') }}</label>
								<input type="password" name="password" required="" class="form-control" id="password" placeholder="{{ trans('form.password') }}" >
							</div>
						</div>
						<div class="row">
							<button type="submit" id="submit-btn" class="btn btn-primary pull-right" value="create" name="submit">{{ trans('form.create') }} User</button>
							<button type="button" id="cancel-btn" class="btn btn-white pull-right m-r-md"><i class="fa fa-times"> </i> {{ trans('form.cancel') }}</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-8">
			<div class="ibox">
				<div class="ibox-content">
					<div class="clients-list">
						<ul class="nav nav-tabs">
							<li class="active"><a data-toggle="tab" href="#tab-1"><i class="fa fa-user"></i> Contacts</a></li>
						</ul>
						<div class="tab-content">
							<div id="tab-1" class="tab-pane active">
								<div class="full-height-scroll">
									<div class="table-responsive">
										<table class="table table-striped table-hover">
											<tbody>
												<tr>
													<td class="client-avatar"><img alt="image" src="{{ URL::asset('asset/img/a2.jpg') }}"> </td>
													<td>
														<a data-toggle="tab" href="#contact-{{ $user_data->id }}" class="client-link">{{ $user_data->first_name }} {{ $user_data->last_name }}</a>
													</td>
													<td class="contact-type"><i class="fa fa-phone"> </i></td>
													<td>{{ $user_data->profile->phone }}</td>
													<td class="contact-type"><i class="fa fa-envelope"> </i></td>
													<td> {{ $user_data->email }}</td>
													<td class="contact-type"><i class="fa fa-envelope"> </i></td>
													<td> {{ $user_data->roles[0]['name'] }}</td>
													<td class="client-status"><span class="label label-primary">Active</span></td>
												</tr>
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="ibox ">
				<div class="ibox-content">
					<div class="tab-content">
						<div id="contact-{{ $user_data->id }}" class="tab-pane active">
							<div class="row m-b-lg">
								<div class="col-lg-4 text-center">
									<meta name="_token" content="{!! csrf_token() !!}"/>
									<h2>{{ $user_data->first_name }} {{ $user_data->last_name }}</h2>
									<div class="m-b-sm">
										@if($user_data->profile->image)
										<img alt="image" class="img-circle" src="{{ URL::asset('uploads/'.$user_data->profile->image) }}"
										style="width: 62px">
										@else
										<img alt="image" class="img-circle" src="http://dummyimage.com/50x50/e0e0e0/444.png&text={{substr($user_data->first_name, 0,1) .substr($user_data->last_name, 0,1)}}"
										style="width: 62px">
										@endif
									</div>
								</div>
								<div class="col-lg-8">
									<strong>
										Contact Info
									</strong>
									<p>
										Contact Number {{ $user_data->profile->phone }}
									</p>
									<br/>
									<br/>
									<br/>
									<button type="button" class="btn btn-primary" onclick="geteditUserInfo({{$user_data->id}})">
										<i class="fa fa-pencil-square-o"></i> {{ trans('form.edit') }}
									</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
@include('layouts.script')
<script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>
<script>
	$(document).ready(function() {
		$('.footable').footable();
	});
	$('#cancel-btn-admin').click(function(){
		$('.removeclasscol').addClass('collapsed');
		$('.user-info-admin').hide();
		$('#first-name').val('');
		$('#last-name').val('');
		$('#first-name').val('');
		$('#phone').val('');
		$('#email').val('');
		document.getElementById("password").required = true;
		$('#password').val('');
		$('#submit-btn').val('create');
		$('#user-id').val('');
		$('#submit-btn').html('Create');
		$('#update-user').html('Create User');
	});
	$('.delete-user').click(function(){
		var user_id = $(this).data('rel');
		var name = $(this).data('name');
		swal({
			title: "Are you sure?",
			text: "You will not be able to recover this User!",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Yes, delete",
			cancelButtonText: "No, cancel",
			closeOnConfirm: false,
			closeOnCancel: false },
			function (isConfirm) {
				if (isConfirm) {
					var route = "<?php echo route('user/delete'); ?>";
					$.ajax({
						url: route,
						type: 'POST',
						dataType: 'json',
						data: {user_id : user_id},
						success: function(result) {
							swal("Deleted!", "User was successfully deleted.", "success");
							window.location.reload(); 
						}
					});
				} else {
					swal("Cancelled", "user is safe :)", "error");
				}
			});
	});
	$('#cancel-btn').click(function(){
		$('.removeclasscol').addClass('collapsed');
		$('.update-info-user').hide();
		$('#first-name').val('');
		$('#last-name').val('');
		$('#first-name').val('');
		$('#phone').val('');
		$('#email').val('');
		document.getElementById("password").required = true;
		$('#submit-btn').val('create');
		$('#password').val('');
		$('#user-id').val('');
		$('#submit-btn').html('Create');
		$('#update-user').html('Create User');
	});
	$('.custom-collapse').click(function(){
		var ibox = $(this).closest('div.ibox');
		var button = $(this).find('i');
		var content = ibox.find('div.ibox-content');
		content.slideToggle(200);
		button.toggleClass('fa-chevron-up').toggleClass('fa-chevron-down');
		ibox.toggleClass('').toggleClass('border-bottom');
		setTimeout(function () {
			ibox.resize();
			ibox.find('[id^=map-]').resize();
		}, 50);
	});
	function geteditUserInfo(user_id){
		var route = "<?php echo route('user/info'); ?>";
		$.ajax({
			url: route,
			type: 'POST',
			dataType: 'json',
			data: {user_id : user_id},
			success: function(result) {
				var data = result.user_data;
				$('.removeclasscol').removeClass('collapsed');
				$('.float-e-margins').show();
				$('.user-info-admin').show();
				$('#first-name').val(data.first_name);
				$('#last-name').val(data.last_name);
				$('#first-name').val(data.first_name);
				$('#phone').val(data.phone);
				$('#email').val(data.email);
				document.getElementById("password").required = false;
				$('#submit-btn').val('update');
				$('#user-id').val(data.user_id);
				$('#submit-btn').html('Update');
				$('#update-user').html('Update User');
        // document.getElementById('role').value = data.role_id;
    }
});
	}
</script>
@stop