	@extends('layouts.admin')
	@section('content')
	@include('layouts.navbartop')
	<div class="wrapper wrapper-content">
	<div class="row">
	@if ($errors->has())
	<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	@foreach ($errors->all() as $error)
	{{ $error }}<br>        
	@endforeach
	</div>
	@endif
	@if (session('success'))
	<div class="alert alert-success alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
	{{ session('success') }}
	</div>
	@endif
	<div class="wrapper wrapper-content animated fadeInRight">
	<div class="row">
	<div class="Col-lg-12">
	<div class="wrapper wrapper-content animated fadeInRight">
	<div class="ibox">
	<div class="ibox-title">
	<h2>Add Deal</h2>
	</div>
	<div class="ibox-content">
	<div class="row">
	<form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post"  enctype="multipart/form-data" action="{{ URL::route('directory/deals/create')}}">
	<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
	<input type="hidden" name="deal_id" value="{{(isset($deals_data) ? $deals_data->id : '')}}"/>
	<h1>Create Deal</h1>
	<fieldset>
	<div class="row">
	<div class="col-lg-12">
	<div class="form-group">
	<label class="col-sm-2 control-label">
	{{ trans('form.title') }}
	</label>
	<div class="col-sm-10">
	<input id="title" placeholder="{{ trans('form.title') }}" name="title" 
	type="text" value="{{(isset($deals_data) ? $deals_data->title : '')}}" class="form-control" required="">
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-2 control-label">
	{{ trans('form.short_desc') }}
	</label>
	<div class="col-sm-10">
	<input id="short_desc" placeholder="{{ trans('form.short_desc') }}" name="short_desc" type="text" value="{{(isset($deals_data) ? $deals_data->short_description : '')}}" class="form-control">
	</div>
	</div>
	<div class="form-group">
	<label class="col-sm-2 control-label">
	{{ trans('form.full_desc') }}
	</label>
	<div class="col-sm-10">
	<div class="ibox float-e-margins">
		<div class="ibox-content no-padding">
			<textarea name="full_desc" class="summernote">
				{{(isset($deals_data) ? $deals_data->full_description : '')}}
			</textarea>
		</div>
	</div>
	</div>
	</div>
	</div>
	</di	v>
	<div class="row">
	<div class="col-lg-12">
	<div class="form-group">
	<label class="col-sm-2 control-label">
	{{ trans('form.image_cropper') }}</label>
	<div class="col-sm-10">
		<div class="ibox float-e-margins">
			<div class="ibox-content">
				<div class="row">
					<div class="col-md-6">
						<div class="image-crop" 
						style="max-width:100%;">
						@if(isset($deals_data->image))
						<img src="{{Url::asset('/hotelfiles/'.$deals_data->hotel_id.'/images/'.$deals_data->image)}}">
						@else
						<img src="{{Url::asset('/asset/img/p3.jpg')}}">
						@endif
					</div>
				</div>
				<div class="col-md-6">
					<h4>Preview image</h4>
					<div class="img-preview img-preview-sm"></div>
					<div class="btn-group">
						<label title="Upload image file" for="inputImage" class="btn btn-primary">
							<input type="hidden" id="img_crop" name="img_crop" value="" />
							<input type="file" accept="image/*" name="preview_img" 
							id="inputImage" class="hide">
							Upload new image
						</label>
						<input type="hidden" id="image_crop_path" name="image_crop_path" value=""/>				
						<label title="download image" id="download" 
						class="btn btn-primary">Download</label>
					</div>
					<div class="btn-group">
						<button class="btn btn-white" id="zoomIn" type="button">Zoom In</button>
						<button class="btn btn-white" id="zoomOut" type="button">Zoom Out</button>
						<button class="btn btn-white" id="rotateLeft" type="button">Rotate Left</button>
						<button class="btn btn-white" id="rotateRight" type="button">Rotate Right</button>
						<button class="btn btn-warning" id="setDrag" type="button">New crop</button>
					</div>
				</div>
			</div>
		</div>
	</div> 
	</div>
	</div>
	</div>
	</div>
	<div class="row">
	<div class="col-lg-12">
	<div class="form-group">
	<div id="currency_price" class="form-group" style="display:none">
	<label class="col-sm-2 control-label">
	{{ trans('form.currency') }}</label>
	<div class="col-sm-2">
		{!! Form::select('currency', ['USD','JPY','EUR','GBP','AUD','CHF'],null,['class' => 'chosen-select1 form-control','data-placeholder'=>'Choose a Currency...','value'=>"{{ old('country') }}"]) !!}
	</div>
	<label class="col-sm-2 control-label">
		{{ trans('form.price') }}</label>
		<div class="col-sm-6">
			<input id="price" name="price" placeholder="{{ trans('form.price') }}" type="text" 
			value="{{(isset($deals_data) ? $deals_data->price : '')}}" class="form-control">
		</div>
	</div>
	<div class="form-group">
		<label class="col-sm-2 control-label">
		</label>
		<div class="col-sm-2">
			<div class="radio radio-info radio-inline">
				<input type="radio" class="radio-deal-tips" id="inlineRadio1" value="option1" name="Deals">
				<label for="inlineRadio1">Deal</label>
			</div>
			<div class="radio radio-inline">
				<input type="radio" class="radio-deal-tips" id="inlineRadio2" value="option2" name="Tips" checked="">
				<label for="inlineRadio2">Tip</label>
			</div>
		</div>
		<label class="col-sm-2 control-label"></label>
		<div class="col-sm-2">
			<div class="i-checks">
				<label>
					@if(isset($deals_data) )
					<input type="checkbox" {{($deals_data->show_on_home == 1)?'checked=""': ''}} 
					id="itself-chain" name="show_on_home" value="on" data-parsley-group="block1" data-parsley-group="block1">
					@else
					<input type="checkbox" id="itself-chain" 
					name="show_on_home" value="on" data-parsley-group="block1" data-parsley-group="block1">
					@endif 
					<i></i> <i></i>{{ trans('form.show_on_home') }}</label>
				</div>
			</div>
			<div class="col-sm-2">
				<div class="i-checks"><label> 
					@if(isset($deals_data))
					<input type="checkbox" {{($deals_data->is_published == 1)?'checked=""': ''}} id="itself-chain" name="is_published" value="on" data-parsley-group="block1"> <i></i>
					@else
					<input type="checkbox" id="itself-chain" name="is_published" value="on" data-parsley-group="block1" checked=""> <i></i>
					@endif
					{{ trans('form.published') }}</label></div>
				</div>
			</div>
		</div>
	</div>
	</div>
	</fieldset>
	<h1>Finish</h1>
	<fieldset>
	<div class="row">
		<div class="col-sm-12 header_slider_preview border">
			@if(isset($deals_data) )
			<img id="slider_preview_image" src="{{Url::asset('/hotelfiles/'.$deals_data->hotel_id.'/images/'.$deals_data->image)}}"/>
			@else
			<img id="slider_preview_image" src=""/>
			@endif
			<p>
				<span>Deal of the day:</span> 
				<span class="preview-title"></span>
				<br/>
				<span class="preview-short-desc"></span>
			</p>
			<span class="btn btn-w-m btn-primary"> More Info </span>

			<div id="deal_price">
				<div id="star"><span class="star-price">5</span>
				</div>
			</div>
		</div>
	</div>
	<br />
	<br />
	<div class="row">
		<div class="col-sm-12 border header_second_preview_image">
			<img class="second-preview-image" src=""/>
			<p>
				<span>Deal of the day:</span> 
				<span class="preview-title"></span>
				<br/>
				<span class="preview-short-desc"></span>
			</p>

		</div>
	</div>
	</fieldset>
	</form>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	</div>
	@include('layouts.script')
	<script>
	$(document).ready(function(){
	$("#form").steps({
	bodyTag: "fieldset",
	onStepChanging: function (event, currentIndex, newIndex){
	if (currentIndex > newIndex){
	return true;
	}

	if (newIndex === 3 && Number($("#age").val()) < 18){
	return false;
	}

	var form = $(this);

	if (currentIndex < newIndex){
	$(".body:eq(" + newIndex + ") label.error", form).remove();
	$(".body:eq(" + newIndex + ") .error", form).removeClass("error");
	}

	form.validate().settings.ignore = ":disabled,:hidden";

	return form.valid();
	},
	onStepChanged: function (event, currentIndex, priorIndex){
	var img_url = $('#image_crop_path').val();
	if(img_url){
		$("#slider_preview_image").attr("src",img_url);
		$(".second-preview-image").attr("src",img_url);	
	}else{
		$("#slider_preview_image").attr("src",$('#image_crop_path').val());
		$(".second-preview-image").attr("src",$('#image_crop_path').val());		
	}

	$('.preview-title').html($('#title').val());
	$('.preview-short-desc').html($('#short_desc').val());
	var price = $('#price').val(); 
	$('.star-price').html('Only €'+ price);
	if (currentIndex === 2 && Number($("#age").val()) >= 18){
	$(this).steps("next");
	}


	if (currentIndex === 2 && priorIndex === 3){
		$(this).steps("previous");
	}
	},
	onFinishing: function (event, currentIndex){
	var form = $(this);

	form.validate().settings.ignore = ":disabled";

	return form.valid();
	},
	onCanceled:function(){
		window.location = '<?php echo route('directory/deals/list'); ?>';
	},
	onFinished: function (event, currentIndex){
	var form = $(this);
	form.submit();
	}
	}).validate({
	errorPlacement: function (error, element){
	element.before(error);
	},
	rules: {
	confirm: {
	equalTo: "#password"
	}
	}
	});

	$('.summernote').summernote({height: 300});
	var $image = $(".image-crop > img")
	$($image).cropper({
	aspectRatio: 1.618,
	preview: ".img-preview",
	done: function(data) {
		$('#image_crop_path').val($image.cropper("getDataURL"));
		$('#img_crop').val(JSON.stringify({
									"x": data.x,
									"y": data.y,
									"width": data.width,
									"height":data.height
								}));
	}	
});

	var $inputImage = $("#inputImage");
	if (window.FileReader) {
	$inputImage.change(function() {
	var fileReader = new FileReader(),
	files = this.files,
	file;
	if (!files.length) {
		return;
	}

	file = files[0];
	if (/^image\/\w+$/.test(file.type)) {
		fileReader.readAsDataURL(file);
		fileReader.onload = function () {
			$inputImage.val("");
			$image.cropper("reset", true).cropper("replace", this.result);
			setTimeout(function(){
				var naturalWidth = $image.cropper('getData').width;
				console.log(naturalWidth);
				if(naturalWidth < 800){
					var url = "<?php echo Url::asset('/asset/img/p3.jpg'); ?>";
					$image.cropper("replace", url).destroy();
					toastr.error('The uploaded image should be at least 800 pixels in width');
					return false;
				}
				$('#image_crop_path').val($image.cropper("getDataURL"));
			}, 1000);
			
			
			
			
		};
	} else {
		showMessage("Please choose an image file.");
	}
	});
	} else {
		$inputImage.addClass("hide");
	}
	$("#download").click(function() {
	window.open($image.cropper("getDataURL"));
	});

	$("#zoomIn").click(function() {
		$image.cropper("zoom", 0.1);
	});

	$("#zoomOut").click(function() {
		$image.cropper("zoom", -0.1);
	});

	$("#rotateLeft").click(function() {
		$image.cropper("rotate", 45);
	});

	$("#rotateRight").click(function() {
		$image.cropper("rotate", -45);
	});

	$("#setDrag").click(function() {

	});

	$('.i-checks').iCheck({
		checkboxClass: 'icheckbox_square-green'
	});
	var config = {
		'.chosen-select1'           : {},
		'.chosen-select-phonecode'           : {},
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}
	$("#wizard").steps();
	$(".radio-deal-tips").click(function () {
		if($(this).attr('id') == 'inlineRadio2'){
			$('#currency_price').hide('slow');
		}else{
			$('#currency_price').show('slow');
			$('.chosen-container').css('width', 280);
		}
	});
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
	});
	</script>
	@stop