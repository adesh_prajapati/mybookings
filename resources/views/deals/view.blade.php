<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<body id="page-top" class="landing-page">
	@include('layouts.header')
    <style type="text/css">
        .star-six { 
           width: 0; 
           height: 0;
           border-left: 50px solid transparent; 
           border-right: 50px solid transparent; 
           border-bottom: 100px solid red; 
           position: relative; 
       } .star-six:after { 
        width: 0; 
        height: 0; 
        border-left: 50px solid transparent; 
        border-right: 50px solid transparent; 
        border-top: 100px solid red;
        position: absolute; 
        content: ""; 
        top: 30px; 
        left: -50px; 
    }
    .star-six .star-price{color: #fff;
        margin: 44px -9px;
        position: absolute;
        z-index: 9999;}
    </style>
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
       <div class="row">
          <div class="col-lg-12 text-center">
             <div class="navy-line"></div>
             <h1>ebookstore<br/> <span class="navy">Deals</span> </h1>
         </div>
     </div>
     @if($deals_data->count())
     <div class="wrapper wrapper-content animated fadeInRight">
        @foreach($deals_data as $deals)
        @if($deals->type == 'Deals')
        <div class="col-lg-12">
            <div class="row">
                <img style="width:auto; height:250px;" class="pull-right" src="{{ URL::asset('hotelfiles/2/images/'.$deals->image) }}">
                <div class="jumbotron">
                    <h1>Deal Of the day:</h1>
                    <h3>{{$deals->title}}</h3>
                    <p>{{$deals->short_description}}</p>
                    <p><a class="btn btn-primary btn-lg directory-button" role="button">More info</a>
                    </p>
                    <div class="star-six pull-right" style="position: absolute;right: 10px;top: 10px;"><span class="star-price">Only $ {{$deals->price}}</span></div>
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-12">
            <div class="row">
                <img style="width:auto; height:250px;" class="pull-right" src="{{ URL::asset('hotelfiles/2/images/'.$deals->image) }}">
                <div class="jumbotron">
                    <h1>Tip Of the day:</h1>
                    <h3>{{$deals->title}}</h3>
                    <p>{{$deals->short_description}}</p>
                    <p><a class="btn btn-primary btn-lg directory-button" role="button">More info</a>
                    </p>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    @else
    <div class="ibox-content m-b-sm border-bottom">
        <div class="text-center p-lg">
            <h2>No Deals available</h2>

        </div>
    </div>
    @endif
</section>
@include('layouts.sectionfooter')
@include('layouts.script')
<script>
    $(window).load(function() {
        $('.navbar-default').addClass('navbar-scroll');
    });
</script>
</body>
</html>
