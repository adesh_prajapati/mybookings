<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body id="page-top" class="landing-page">
	@include('layouts.header')
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
    	<div class="row">
    		<div class="col-lg-12 text-center">
    			<div class="navy-line"></div>
    			<h1>ebookstore<br/> <span class="navy">STATIC TEXT</span> </h1>
    		</div>
    	</div>
    	<div class="wrapper wrapper-content animated fadeInRight">
    		<div class="row">
    			<div class="col-lg-12">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5 class="title">{{$page_data->title}}</h5>
                        </div>
                        <div class="ibox-content">
                          {{$page_data->description}}
                      </div>
                  </div>
              </div>
          </div>
      </section>
      @include('layouts.sectionfooter')
      @include('layouts.script')
      <script type="text/javascript">
        $(document).ready(function(){
           $('.navbar-default').addClass('navbar-scroll');
       });
   </script>
</body>
</html>
