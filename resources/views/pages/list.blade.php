@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>All Pages for {{Session::get('hotel')['name']}}</h5>
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="ibox-tools">
						<a class="btn btn-primary btn-sm" href="{{ URL::route('directory/pages/create')}}">
							<i class="fa fa-plus"></i>
							Create new Pages</a>
						</div>
					</div>
					@if($pages_details->count())
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny" 
						data-page-size="15">
						<thead>
							<tr>
								<th data-toggle="true">Title</th>
								<th data-hide="slug">Slug</th>
								<th data-hide="status">Status</th>
								<th class="text-right" data-sort-ignore="true">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($pages_details as $pages_detail)
							<tr class="sortitem" id="page_data_{{$pages_detail->id}}">
								<td>
									{{$pages_detail->title}}
								</td>
								<td>
									{{$pages_detail->slug}}
								</td>
								<td>
									@if($pages_detail->is_published == 1)
									<span class="badge badge-primary">Published</span>
									@else
									<span class="badge">UnPublished</span>
									@endif
								</td>
								<td class="text-right">
									<div class="btn-group">
										<a class="btn-white btn btn-xs" 
										href="{{ route('directory/page/edit', ['id' => $pages_detail->id]) }}">
										<i class="fa fa-edit"></i> Edit</a>
										<a class="btn-white btn btn-xs delete" 
										data-rel="{{$pages_detail->id}}">
										<i class="fa fa-trash"></i> Delete</a>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>

				</div>
				@else
				<br/>
				<div class="ibox-content m-b-sm border-bottom">
					<div class="text-center p-lg">
					<h2>No Pages available</h2>

					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$('.footable').footable();
		$('.delete').click(function () {
			var page_id = $(this).data('rel');
			swal({
				title: "Are you sure?",
				text: "Your will not be able to recover this Page!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete it!",
				cancelButtonText: "No, cancel plx!",
				closeOnConfirm: false,
				closeOnCancel: false },
				function (isConfirm) {
					if (isConfirm) {
						var route = "<?php echo route('directory/page/delete'); ?>";
						$.ajax({
							url: route,
							type: 'POST',
							dataType: 'json',
							data: {page_id : page_id},
							success: function(result) {
								if(result.response == 'success'){
									$('#page_data_'+page_id).remove();
									swal("Deleted!", "Your page has been deleted.", "success");
									window.location.reload();
								}
							}
						});
					} else {
						swal("Cancelled", "Your page is safe :)", "error");
					}
				});
		});
	});

</script>
@stop