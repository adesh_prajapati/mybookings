<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<link rel="stylesheet" href="{{ URL::asset('asset/css/plugins/blueimp/css/blueimp-gallery.min.css') }}">
<body id="page-top" class="landing-page">
  @include('layouts.header')
  <style>
    .grid .ibox {
        margin-bottom: 0;
    }
    .grid-item {
        margin-bottom: 25px;
        width: 350px;
    }
</style>
<section id="features" class="container services mainbuttons">
 <div class="row">
  <div class="col-lg-12 text-center">
   <div class="navy-line"></div>
   <h1><span class="navy"> Floor Plan</span> </h1>
</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
    @if($floor_plan_data->count())
    <div class="grid" >
        @foreach($floor_plan_data as $floor_plan)
        <div class="grid-item floor-plan-{{$floor_plan->id}}">
            <div class="ibox">
                <div class="ibox-content ">
                    <h2 class="font-bold floor-title" id="floor_title_{{$floor_plan->id}}" 
                        data-val="{{$floor_plan->title}}">{{$floor_plan->title}}</h2>
                        <div class="lightBoxGallery">
                            <a id="floor_img_{{$floor_plan->id}}" href="{{URL::asset('hotelfiles/'.$floor_plan->hotel_id.'/'.'images'.'/'.$floor_plan->image)}}" 
                                title="{{$floor_plan->title}}" data-gallery="">
                                <img class="floor_img" src="{{URL::asset('hotelfiles/'.$floor_plan->hotel_id.'/'.'images'.'/'.$floor_plan->image)}}">
                            </a>
                            <div id="blueimp-gallery" class="blueimp-gallery">
                                <div class="slides"></div>
                                <h3 class="title"></h3>
                                <a class="prev">‹</a>
                                <a class="next">›</a>
                                <a class="close">×</a>
                                <a class="play-pause"></a>
                                <ol class="indicator"></ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @else
        <div class="ibox-content m-b-sm border-bottom">
            <div class="text-center p-lg">
                <h2>No Floor Plan available</h2>

            </div>
        </div>
        @endif
    </div>
</section>
@include('layouts.sectionfooter')
@include('layouts.script')
<script src="{{ URL::asset('asset/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
<script>
    $(window).load(function() {
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 350,
            gutter: 25
        });
        $('.navbar-default').addClass('navbar-scroll');
    });
    </script
</body>
</html>
