@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="modal inmodal fade in" id="find_hotspots" tabindex="-1" role="dialog"  aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-body">
				<div class="ibox-content">
					<form enctype="multipart/form-data" method="post" class="form-horizontal" 
					files="true" action="{{ URL::route('directory/floor/create')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="form-group">
						<label class="col-sm-2 control-label">{{ trans('form.title') }}</label>
						<div class="col-sm-10">
							<input type="hidden" id="floor_plan_id" name="floor_plan_id" value=""/>
							<input type="text" id="modal_title" class="form-control" 
							placeholder="title" name="title" required="">
						</div>
					</div>
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<label class="col-sm-2 control-label">{{ trans('form.image') }}
						</label>
						<div class="col-sm-10">
							<div class="fileUpload btn btn-primary">
								<button type="button" class="btn btn-primary btn-block" id="upload_btn">Browse files</button>
									<input class="hide" id="uploadBtn" type="file" class="upload" name="image" />
							</div>
						</div>
					</div>
					<div class="hr-line-dashed"></div>
					<div class="form-group">
						<div class="col-sm-4 col-sm-offset-2">
							<a class="btn btn-white" type="button" href="{{route('directory/floor')}}">Cancel</a>
							<input class="btn btn-primary" type="submit" value="Save"/>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</div>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-sm-12">
		<div class="col-sm-4">
			<h2>Floor Plan</h2>
		</div>
		<div class="col-sm-8">
			<div class="title-action">
				<button class="btn btn-primary btn-sm" title="Create new cluster" data-toggle="modal" id="add_floor_plan">
					<i class="fa fa-plus"></i> 
					<span class="bold">Add Floor Plan</span>
				</button>
			</div>
		</div>
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight">
	@if($floor_plan_data->count())
	<div class="grid" >
		@foreach($floor_plan_data as $floor_plan)
		<div class="grid-item floor-plan-{{$floor_plan->id}}">
			<div class="ibox">
				<div class="ibox-content ">
					<a class="pull-right delete" data-rel="{{$floor_plan->id}}">
						<i class="fa fa-trash fa-2x"></i></a>
						<a class="pull-right edit" data-rel="{{$floor_plan->id}}">
							<i class="fa fa-pencil-square-o fa-2x"></i>
						</a>
						<h4 class="font-bold" id="floor_title_{{$floor_plan->id}}" 
							data-val="{{$floor_plan->title}}">{{$floor_plan->title}}</h4>
							<div class="lightBoxGallery">
								<a id="floor_img_{{$floor_plan->id}}" href="{{URL::asset('hotelfiles/'.$floor_plan->hotel_id.'/'.'images'.'/'.$floor_plan->image)}}" 
									title="Image from Unsplash" data-gallery="">
									<img class="floor_img" src="{{URL::asset('hotelfiles/'.$floor_plan->hotel_id.'/'.'images'.'/'.$floor_plan->image)}}">
								</a>
							</div>
							<div id="blueimp-gallery" class="blueimp-gallery">
								<div class="slides"></div>
								<h3 class="title"></h3>
								<a class="prev">‹</a>
								<a class="next">›</a>
								<a class="close">×</a>
								<a class="play-pause"></a>
								<ol class="indicator"></ol>
							</div>
						</div>
					</div>
				</div>
				@endforeach
			</div>
			@else
			<div class="ibox-content m-b-sm border-bottom">
				<div class="text-center p-lg">
					<h2>No Floor Plan available</h2>

				</div>
			</div>
			@endif
		</div>
		<style>
			.grid .ibox {
				margin-bottom: 0;
			}
			.grid-item {
				margin-bottom: 25px;
				width: 300px;
			}
		</style>
		@include('layouts.script')
		<script type="text/javascript">
			$.ajaxSetup({
				headers: { 'X-CSRF-Token' : $('input[name="_token"]').val() }
			});
		</script>
		<script>
			$(window).load(function() {
				$('.grid').masonry({
					itemSelector: '.grid-item',
					columnWidth: 300,
					gutter: 25
				});
			});
			$(document).ready(function(){
				$('#upload_btn').click(function () {
					$('#uploadBtn').trigger('click');
				});
				
				$('#add_floor_plan').click(function () {
					$('#floor_plan_id').val('');
					$('#modal_title').val('');
					$('#find_hotspots').modal('show');
				});
				$('.delete').click(function () {
					var floor_plan_id = $(this).data('rel');
					swal({
						title: "Are you sure?",
						text: "You will not be able to recover this Floor Plan!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, delete",
						cancelButtonText: "No, cancel",
						closeOnConfirm: false,
						closeOnCancel: false },
						function (isConfirm) {
							if (isConfirm) {
								var route = "<?php echo route('directory/floor/delete'); ?>";
								$.ajax({
									url: route,
									type: 'POST',
									dataType: 'json',
									data: {floor_plan_id : floor_plan_id},
									success: function(result) {
										if(result.response == 'success'){
											$('.floor-plan-'+floor_plan_id).empty();
											swal("Deleted!", "Floor Plan has been deleted.", "success");
											window.location.reload();
										}
									}
								});
							} else {
								swal("Cancelled", "Floor Plan is safe :)", "error");
							}
						});
				});
				$('.edit').click(function () {
					var rel = $(this).data('rel');
					$('#floor_plan_id').val(rel);
					$('#modal_title').val($.trim($('#floor_title_'+rel).data('val')));
					$('#find_hotspots').modal('show');
				});
				function reload(){
					location.reload();
				}
			});
		</script>
		@stop