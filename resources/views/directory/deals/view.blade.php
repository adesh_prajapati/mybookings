<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<body id="page-top" class="deals landing-page ">
	@include('layouts.header')
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
       <div class="row">
          <div class="col-lg-12 text-center">
             <div class="navy-line"></div>
             <h2 class="navy">Deals</h2>
         </div>
     </div>
     @if($deals_data->count())
     <div class="wrapper wrapper-content animated fadeInRight">
        @foreach($deals_data as $deals)
        @if($deals->type == 'Deals')
        <div class="col-lg-12">
            <div class="row">
                
                <div class="jumbotron">
                    <div class="col-md-4">
                        <h2>{{$deals->title}}</h2>
                        <p>{{$deals->short_description}}</p>
                        <p><a class="btn btn-primary btn-lg directory-button" role="button">More info</a>
                        </p>
                    </div>
                    <div class="pull-right col-md-4">
                        <img style="width:auto;" class="pull-right" src="{{ URL::asset('hotelfiles/'.$hotel_data->id.'/images/'.$deals->image) }}">
                    </div>
                    <div class="star-six pull-right" style="position: absolute;right: 10px;top: 10px;"><span class="star-price">Only $ {{$deals->price}}</span></div>
                </div>
            </div>
        </div>
        @else
        <div class="col-lg-12">
            <div class="row">
                <div class="jumbotron">
                    <div class="col-md-8">
                        <h2>{{$deals->title}}</h2>
                        <p>{{$deals->short_description}}</p>
                        <p><a class="btn btn-primary btn-lg directory-button" role="button">More info</a>
                        </p>
                    </div>
                    <div class="pull-right col-md-4">
                        <img style="width:100%;" class="pull-right" src="{{ URL::asset('hotelfiles/'.$hotel_data->id.'/images/'.$deals->image) }}">
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>
    @else
    <div class="ibox-content m-b-sm border-bottom">
        <div class="text-center p-lg">
            <h2>No Deals available</h2>

        </div>
    </div>
    @endif
</section>
@include('layouts.sectionfooter')
@include('layouts.script')
<script>
    $(window).load(function() {
        $('.navbar-default').addClass('navbar-scroll');
    });
</script>
</body>
</html>
