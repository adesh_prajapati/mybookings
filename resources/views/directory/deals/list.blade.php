@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>All Deals for {{Session::get('hotel')['name']}}</h5>
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="ibox-tools">
						<a class="btn btn-primary btn-sm" href="{{ URL::route('directory/deals/create')}}">
							<i class="fa fa-plus"></i>
							Create new Deal or Tip</a>
						</div>
					</div>
					@if($deals_data->count())
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny" 
						data-page-size="15">
						<thead>
							<tr>
								<th>Image</th>
								<th data-hide="phone">Title</th>
								<th data-hide="price">Price</th>
								<th data-hide="show">Type</th>
								<th data-hide="show">Show On Home</th>
								<th data-hide="phone,tablet" >Published</th>
								<th class="text-right" data-sort-ignore="true">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($deals_data as $deal)
							<tr id="deal_data_{{$deal->id}}">
								<td
                                                                    <a class="" href="{{ route('directory/deals/edit', ['id' => $deal->id]) }}">
									<img style="max-width: 100px;" src="{{Url::asset('/hotelfiles/'.$deal->hotel_id.'/'.'images/'.$deal->image)}}">	
                                                                    </a>
								</td>
								<td>
                                                                    <a class="" href="{{ route('directory/deals/edit', ['id' => $deal->id]) }}">
									{{$deal->title}}
                                                                    </a>
								</td>
								<td>
                                                                    @if($deal->type == 'deal')
									{{$deal->currency}} {{$deal->price}}
                                                                    @else
                                                                        -
                                                                    @endif
								</td>
								<td>{{$deal->type}}</td>
								<td>
									@if($deal->show_on_home == '1')
									<span class="badge badge-primary">Yes</span>
									@else
									<span class="badge">No</span>
									@endif
								</td>
								<td>
									@if($deal->is_published == '1')
									<span class="badge badge-primary">Published</span>
									@else
									<span class="badge">Unpublished</span>
									@endif
								</td>
								<td class="text-right">
									<div class="btn-group">
										<a class="btn-white btn btn-xs" 
										href="{{ route('directory/deals/edit', ['id' => $deal->id]) }}">
										<i class="fa fa-edit"></i> Edit</a>
										<a class="btn-white btn btn-xs delete" 
										data-rel="{{$deal->id}}">
										<i class="fa fa-trash"></i> Delete</a>
									</div>
								</td>
							</tr>
							@endforeach

						</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>

				</div>
				@else
				<br/>
				<div class="ibox-content m-b-sm border-bottom">
					<div class="text-center p-lg">
                                            <h2>No Deals or Tips created yet</h2>
                                            <a class="btn btn-primary btn-sm" href="{{ URL::route('directory/deals/create')}}">
                                                            <i class="fa fa-plus"></i>
                                                            Create new Deal or Tip
                                            </a>

					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$('.footable').footable();
		$('.delete').click(function () {
			var deal_id = $(this).data('rel');
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this Deals!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete",
				cancelButtonText: "No, cancel",
				closeOnConfirm: false,
				closeOnCancel: false },
				function (isConfirm) {
					if (isConfirm) {
						var route = "<?php echo route('directory/deals/delete'); ?>";
						$.ajax({
							url: route,
							type: 'POST',
							dataType: 'json',
							data: {deal_id : deal_id},
							success: function(result) {
								if(result.response == 'success'){
									$('#deal_data_'+deal_id).remove();
									swal("Deleted!", "Deals has been deleted.", "success");
									window.location.reload();
								}
							}
						});
					} else {
						swal("Cancelled", "Deals is safe :)", "error");
					}
				});
		});
	});

</script>
@stop