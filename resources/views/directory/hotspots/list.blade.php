@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>All Hotspots for {{Session::get('hotel')['name']}}</h5>
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="ibox-tools">
						<a class="btn btn-primary btn-sm" href="{{ URL::route('directory/hotspots/create')}}">
							<i class="fa fa-plus"></i>
							Create new Hotspots</a>
						</div>
					</div>
					@if($hotspots_details->count())
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny" 
						data-page-size="15">
						<thead>
							<tr>
								<th data-toggle="true">Title</th>
								<th data-hide="phone">Address</th>
								<th data-hide="all">Description</th>
								<th data-hide="all">Website</th>
								<th data-hide="phone,tablet" >Category</th>
								<th data-hide="phone">Phone</th>
								<th class="text-right" data-sort-ignore="true">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($hotspots_details as $hotspots_detail)
							<tr id="hotspots_data_{{$hotspots_detail->id}}">
								<td>
									<a href="{{ route('directory/hotspots/edit', ['id' => $hotspots_detail->id]) }}">{{$hotspots_detail->title}}</a>
								</td>
								<td>
									<i class="fa fa-map-marker"></i> {{$hotspots_detail->address}}
								</td>
								<td>
									{{$hotspots_detail->description}}
								</td>
								<td>
									<a target="_blank" 
									href="{{$hotspots_detail->website}}"><i class="fa fa-sitemap">
								</i>{{$hotspots_detail->website}}</a>
							</td>
							<td>
								{{$hotspots_detail->category}}
							</td>
							<td>
								<i class="fa fa-phone"></i> {{$hotspots_detail->phone}}
							</td>
							<td class="text-right">
								<div class="btn-group">
									<a class="btn-white btn btn-xs" 
									href="{{ route('directory/hotspots/edit', ['id' => $hotspots_detail->id]) }}">
									<i class="fa fa-edit"></i> Edit</a>
									<a class="btn-white btn btn-xs delete" 
									data-rel="{{$hotspots_detail->id}}">
									<i class="fa fa-trash"></i> Delete</a>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<ul class="pagination pull-right"></ul>
							</td>
						</tr>
					</tfoot>
				</table>
			</div>
			@else
			<br/>
			<div class="ibox-content m-b-sm border-bottom">
				<div class="text-center p-lg">
					<h2>No Hotspots created yet</h2>
                                        <a class="btn btn-primary btn-sm" href="{{ URL::route('directory/hotspots/create')}}">
                                            <i class="fa fa-plus"></i>
                                            Create new Hotspot
                                        </a>

				</div>
			</div>
			@endif
		</div>
	</div>
</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$('.footable').footable();
		$('.delete').click(function () {
			var hotspots_id = $(this).data('rel');
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this Hotspot!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete",
				cancelButtonText: "No, cancel",
				closeOnConfirm: false,
				closeOnCancel: false },
				function (isConfirm) {
					if (isConfirm) {
						var route = "<?php echo route('directory/hotspots/delete'); ?>";
						$.ajax({
							url: route,
							type: 'POST',
							dataType: 'json',
							data: {hotspots_id : hotspots_id},
							success: function(result) {
								if(result.response == 'success'){
									$('#hotspots_data_'+hotspots_id).remove();
									swal("Deleted!", "Hotspot has been deleted.", "success");
									window.location.reload();
								}
							}
						});
					} else {
						swal("Cancelled", "Hotspots is safe :)", "error");
					}
				});
		});
	});

</script>
@stop

