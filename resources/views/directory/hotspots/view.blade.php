<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body id="page-top" class="landing-page places">
	@include('layouts.header')
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
    	<div class="row">
    		<div class="col-lg-12 text-center">
    			<div class="navy-line"></div>
    			<h1>ebookstore<br/> <span class="navy">Hotspots</span> </h1>
    		</div>
    	</div>
    	<div class="wrapper wrapper-content animated fadeInRight">
    		<div class="row">
    			<div class="col-lg-12">
                 <div class="row">
                    <div class="col-lg-8 map">
                        <div class="ibox ">
                            <div class="ibox-title">
                                <h2>Hotspots</h2>
                            </div>
                            <div class="ibox-content">
                                <div class="google-map" id="map1" style="min-height:570px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 hidden-md">
                        <div id="category_info" class="ibox float-e-margins">
                            <div class="ibox-title">
                                <h2>Filter by Category</h2>
                            </div>
                            <div class="showRightPush">
                                <div class="ibox-content">
                                    <ul class="todo-list m-t small-list">
                                        @foreach($hotsposts_category as $hotspost)
                                        <li>
                                        @if( in_array($hotspost, $checked_filter_data))
                                            <a href="#" class="check-link" data-rel="{{$hotspost}}">
                                                <i class="fa fa-check-square" data-category="{{$hotspost}}"></i> 
                                                <span class="m-l-xs filter-text">{{$hotspost}}</span>
                                            </a>
                                            
                                            @else
                                             <a href="#" class="check-link" data-rel="{{$hotspost}}">
                                                <i class="fa fa-square-o" data-category="{{$hotspost}}"></i> 
                                                <span class="m-l-xs filter-text">{{$hotspost}}</span>
                                            </a>
                                            @endif
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="widget lazur-bg p-xl hotspot-info" style="display:none;">
                            <h2 class="hotspots-title">
                            </h2>
                            <ul class="list-unstyled m-t-md">
                                <li>
                                    <label><i class="fa fa-phone m-r-xs"></i>Website:</label>
                                    <a class="text-primary hotspots-website" target="_blank" 
                                    href="http://www.hashbyte.com"><i class="fa fa-sitemap"> </i></a>
                                </li>
                                <li>
                                    <label><i class="fa fa-home m-r-xs"></i>Address:</label>
                                    <span class="hotspots-address"></span>
                                </li>
                                <li>
                                    <label><i class="fa fa-envelope m-r-xs"></i>phone:</label>
                                    <span class="hotspots-phone"></span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <div class="theme-config">
        <div class="theme-config-box">
            <div class="spin-icon">
                <i class="fa fa-filter"></i>
            </div>
            <div class="skin-setttings">
                <div class="title">Filter by Category</div>
                <div id="category_info" class="ibox float-e-margins">
                    <div class="showRightPush">
                        <div class="">
                            <ul class="todo-list small-list">
                                @foreach($hotsposts_category as $hotspost)
                                <li>
                                @if( in_array($hotspost, $checked_filter_data))
                                    <a href="#" class="check-link" data-rel="{{$hotspost}}">
                                        <i class="fa fa-check-square" data-category="{{$hotspost}}"></i> 
                                        <span class="m-l-xs filter-text">{{$hotspost}}</span>
                                    </a>
                                    
                                    @else
                                     <a href="#" class="check-link" data-rel="{{$hotspost}}">
                                        <i class="fa fa-square-o" data-category="{{$hotspost}}"></i> 
                                        <span class="m-l-xs filter-text">{{$hotspost}}</span>
                                    </a>
                                    
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    @include('layouts.sectionfooter')
    @include('layouts.script')
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpXj82d8UpCi97wzo_nKXL7nYrd4G70&libraries=places">
</script>
<script type="text/javascript">
    
    
    // SKIN Select
    $('.spin-icon').click(function (){
        $(".theme-config-box").toggleClass("show");
    });
    var val = 'hgsdh';
    var url_image = "{{ URL::asset('asset/img/mapmarker/') }}";
    console.log(url_image);
    var locations = [];
    var latlong = "<?php echo $hotel_info->latlong; ?>" ;
    var locations = {!! $locations !!};
    var string = locations;
    var data = latlong.split(',');
    google.maps.event.addDomListener(window, 'load', init);
    function init(locations) {
        var mapOptions1 = {
            zoom: 11,
            center: new google.maps.LatLng(data[0],data[1]),
            styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
        };
        var mapElement1 = document.getElementById('map1');
        var map = new google.maps.Map(mapElement1, mapOptions1);
        var markerImage = "{{ URL::asset('asset/img/mapmarker/hotel.png') }}";
        var marker = new google.maps.Marker({
            icon:markerImage,
            position: new google.maps.LatLng(data[0], data[1]),
            map: map,
            title: 'Hello World!'
        });
        marker.setMap(map); 
        var infowindow = new google.maps.InfoWindow();
        $.each(string, function( key, value ) {
          var contentString =
          '<div id ="infowindow">' +
          '<h4>' + value[0] + '</h4>' +
          '<p>' + value[5] + '</p>' +
          '<a class="btn btn-default btn-blue" data-rel="'+value[3]+'"><i class="fa fa-info"></i></a>' +
          '</div>';
          infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 400
          });
          var markerImageurl = "{{ URL::asset('asset/img/mapmarker/') }}";
          var imagename = value[4];
          var img_url = markerImageurl+'/'+imagename+'.png';
          console.log(img_url);
          marker1 = new google.maps.Marker({
            position: new google.maps.LatLng(value[1], value[2]),
            map: map,
            icon: img_url
        });
          google.maps.event.addListener(marker1, 'click', (function(marker1, i) {
            return function() {
                infowindow.setContent(contentString);
                infowindow.open(map, marker1);
            }
        })(marker1, key));
      });
        google.maps.event.addListener(infowindow, 'closeclick', function() {  
            $('#category_info').show();
            $('.hotspot-info').hide();
        });   
    }

    $(document).ready(function(){
       $('.navbar-default').addClass('navbar-scroll');
       $.ajaxSetup({
        headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
    });
       $(".check-link").click(function() {
        var filter_data = {};
        $('.small-list li .check-link i').each( function(i, v) {
            if($(this).attr('class') == 'fa fa-check-square'){
                var category = $(this).data('category');
                filter_data[i]= {category};
            }
        });
        
        var route = '{{ url($hotel_data->slug.'/directory/hotspots/filter') }}';
        $.ajax({
            url: route,
            type: 'POST',
            dataType: 'json',
            data: {filter_data : filter_data},
            success: function(result) {
                var locations = [];
                var latlong = "<?php echo $hotel_info->latlong; ?>" ;
                var locations = result;
                var string = locations;
                var data = latlong.split(',');
                var mapOptions1 = {
                    zoom: 11,
                    center: new google.maps.LatLng(data[0],data[1]),
                    styles: [{"featureType":"water","stylers":[{"saturation":43},{"lightness":-11},{"hue":"#0088ff"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"hue":"#ff0000"},{"saturation":-100},{"lightness":99}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#808080"},{"lightness":54}]},{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#ece2d9"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#ccdca1"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#767676"}]},{"featureType":"road","elementType":"labels.text.stroke","stylers":[{"color":"#ffffff"}]},{"featureType":"poi","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#b8cb93"}]},{"featureType":"poi.park","stylers":[{"visibility":"on"}]},{"featureType":"poi.sports_complex","stylers":[{"visibility":"on"}]},{"featureType":"poi.medical","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","stylers":[{"visibility":"simplified"}]}]
                };
                
                var mapElement1 = document.getElementById('map1');
                var map = new google.maps.Map(mapElement1, mapOptions1);
                var markerImage = "{{ URL::asset('asset/img/mapmarker/hotel.png') }}";
                var marker = new google.maps.Marker({
                    icon:markerImage,
                    position: new google.maps.LatLng(data[0], data[1]),
                    map: map,
                    title: 'Hello World!'
                });
                marker.setMap(map); 
                var infowindow = new google.maps.InfoWindow();
                $.each(string, function( key, value ) {
                    var contentString =
                    '<div id ="infowindow">' +
                    '<h4>' + value[0] + '</h4>' +
                    '<p>' + value[5] + '</p>' +
                    '<a class="btn btn-default btn-blue" data-rel="'+value[3]+'"><i class="fa fa-info"></i></a>' +
                    '</div>';
                    infowindow = new google.maps.InfoWindow({
                      content: contentString,
                      maxWidth: 400
                    });
                    var markerImageurl = "{{ URL::asset('asset/img/mapmarker/') }}";
                    var imagename = value[4];
                    var img_url = markerImageurl+'/'+imagename.toLowerCase()+'.png';
                    console.log(img_url);
                    marker1 = new google.maps.Marker({
                      position: new google.maps.LatLng(value[1], value[2]),
                      map: map,
                      icon: img_url
                    });
                    google.maps.event.addListener(marker1, 'click', (function(marker1, i) {
                        return function() {
                            infowindow.setContent(contentString);
                            infowindow.open(map, marker1);
                        }
                    })(marker1, key));
                });
                /*
                $.each(result, function( key, value ) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(value[1], value[2]),
                        map: map
                    });
                    google.maps.event.addListener(marker, 'click', (function(marker, i) {
                        return function() {
                            infowindow.setContent(value[0]);
                            infowindow.open(map, marker);
                        }
                    })(marker, key));
                });*/
                google.maps.event.addListener(infowindow, 'closeclick', function() {  
                    $('#category_info').show();
                    $('.hotspot-info').hide();
                });  
            }
        });
    });
       $(document).on('click', '.btn-blue', function(){
         var hotspots_id = $(this).data('rel');
         var route = '{{ url($hotel_data->slug.'/directory/hotspots/info') }}';
         $.ajax({
            url: route,
            type: 'POST',
            dataType: 'json',
            data: {hotspots_id : hotspots_id},
            success: function(result) {
             if(result.response == 'success'){
                $('.hotspots-title').html('');
                $('.hotspots-address').html('');
                $('.hotspots-website').text('');
                $('.hotspots-phone').html('');
                $('#category_info').hide();
                $('.hotspot-info').show();
                $('.hotspots-title').html(result.data.title);
                $('.hotspots-address').append(result.data.address);
                $('.hotspots-website').text(result.data.website);
                $('.hotspots-website').attr('href',result.data.website);
                $('.hotspots-phone').append(result.data.phone);
            } 

        }
    });
     });
   });
</script>
</body>
</html>
