<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body id="page-top" class="landing-page">
	@include('layouts.header')
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
    	<div class="row">
    		<div class="col-lg-12 text-center">
    			<div class="navy-line"></div>
    			<h1>ebookstore<br/> <span class="navy">GALLERY</span> </h1>
    		</div>
    	</div>
    	<div class="wrapper wrapper-content animated fadeInRight">
    		<div class="row">
    			<div class="col-lg-12">
                <h2 class="text-center">Coming Soon</h2>
                </div>
            </div>
        </section>
        @include('layouts.sectionfooter')
        @include('layouts.script')
    </body>
    </html>
