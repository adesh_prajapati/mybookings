@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<style>
	.wizard > .content > .body  position: relative; }
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h2>Add Pages</h2>
			</div>
			<div class="ibox-content">
				<form id="form" class="form-horizontal wizard-big" method="post" action="{{ URL::route('directory/pages/create')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}">
					<input type="hidden" name="page_id" value="{{(isset($page_data)? $page_data->id: '')}}">
					<h1>Create Pages</h1>
					<fieldset>
						<div class="row">
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.title') }}
								</label>
								<div class="col-sm-10">
									<input id="title" placeholder="{{ trans('form.title') }}" 
									type="text" name="title" 
									value="{{(isset($page_data)? $page_data->title: '')}}" class="form-control" required="">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.content') }}
								</label>
								<div class="col-sm-10">
									<div class="ibox float-e-margins">
										<div class="ibox-content no-padding">
											<textarea name="description" class="summernote">
												{{(isset($page_data)? $page_data->description: '')}}
											</textarea>
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-sm-2 col-sm-offset-2">
									<div class="i-checks">
										<label>
											@if(isset($page_data)) 
											<input type="checkbox" id="itself-chain" 
											name="is_published" value="on" 
											data-parsley-group="block1" {{($page_data->is_published == 1)? 'checked=""': ''}}>
											@else
											<input type="checkbox" id="itself-chain" 
											name="is_published" value="on" 
											data-parsley-group="block1" checked="">
											@endif 
											<i></i>{{ trans('form.published') }}</label></div>
										</div>
									</div>
								</fieldset>
								<h1>Finish</h1>
								<fieldset>
									<h2>Terms and Conditions</h2>
									<input id="acceptTerms" name="acceptTerms" type="checkbox"> 
									<label for="acceptTerms">I agree with the Terms and Conditions.</label>
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
			@include('layouts.script')
			<script>
				$(document).ready(function(){
					$("#wizard").steps();
					$("#form").steps({
						bodyTag: "fieldset",
						onStepChanging: function (event, currentIndex, newIndex){
							// Always allow going backward even if the current step contains invalid fields!
							if (currentIndex > newIndex){
								return true;
							}

							// Forbid suppressing "Warning" step if the user is to young
							if (newIndex === 3 && Number($("#age").val()) < 18)
							{
								return false;
							}

							var form = $(this);

							// Clean up if user went backward before
							if (currentIndex < newIndex)
							{
								// To remove error styles
								$(".body:eq(" + newIndex + ") label.error", form).remove();
								$(".body:eq(" + newIndex + ") .error", form).removeClass("error");
							}

							// Disable validation on fields that are disabled or hidden.
							form.validate().settings.ignore = ":disabled,:hidden";

							// Start validation; Prevent going forward if false
							return form.valid();
						},
						onStepChanged: function (event, currentIndex, priorIndex)
						{
							// Suppress (skip) "Warning" step if the user is old enough.
							if (currentIndex === 2 && Number($("#age").val()) >= 18){
								$(this).steps("next");
							}

							// Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
							if (currentIndex === 2 && priorIndex === 3){
								$(this).steps("previous");
							}
						},
						onCanceled:function(){
							window.location = '<?php echo route('directory/pages'); ?>';
						},
						onFinishing: function (event, currentIndex){
							var form = $(this);
                                                        $('textarea.summernote').val($('.note-editable').html());
                                                        console.log($('.note-editable').html());
							// Disable validation on fields that are disabled.
							// At this point it's recommended to do an overall check (mean ignoring only disabled fields)
							form.validate().settings.ignore = ":disabled";

							// Start validation; Prevent form submission if false
							return form.valid();
						},onFinished: function (event, currentIndex){
							var form = $(this);
							// Submit form input
							form.submit();
						}
					}).validate({
						errorPlacement: function (error, element){
							element.after(error);
						},
						rules: {
							confirm: {
								equalTo: "#password"
							}
						}
					});
var elem = document.querySelector('.js-switch');
var switchery = new Switchery(elem, { color: '#1AB394' });
$('.i-checks').iCheck({
	checkboxClass: 'icheckbox_square-green'
});
});
</script>
<script src="{{ URL::asset('asset/js/generic_scripts.js') }}"></script>
@stop