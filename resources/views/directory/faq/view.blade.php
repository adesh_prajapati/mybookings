<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<body id="page-top" class="landing-page">
    @include('layouts.header')
    <section id="features" class="container services">
    	<div class="row">
    		<div class="col-lg-12 text-center">
    			<div class="navy-line"></div>
    			<h1><span class="navy">[INSERT MENU TITLE HERE]</span> </h1>
    		</div>
    	</div>
    	<div class="row">
            <div class="col-lg-12">
                <div class="wrapper wrapper-content animated fadeInRight">
                    @if($faq_page_data->count())
                    @foreach($faq_page_data as $faq_page)
                    <div class="faq-item" id="faq_item_{{$faq_page->id}}">
                        <div class="row">
                            <div class="col-md-10">
                                <a data-toggle="collapse" href="#faq{{$faq_page->id}}" 
                                    id="#faq{{$faq_page->id}}" class="faq-question" >
                                    {{$faq_page->question}}
                                </a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div id="faq{{$faq_page->id}}" class="panel-collapse collapse ">
                                    <div class="faq-answer" id="answer_{{$faq_page->id}}" data-val="">
                                        <div>
                                            {!! $faq_page->answer !!} 
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @else
                    <div class="ibox-content m-b-sm border-bottom">
                        <div class="text-center p-lg">
                            <h2>No FAQs available</h2>

                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </section>
    @include('layouts.sectionfooter')
    @include('layouts.script')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.navbar-default').addClass('navbar-scroll');
        });
    </script>
</body>
</html>
