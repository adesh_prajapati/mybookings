<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<body id="page-top" class="landing-page">
  @include('layouts.header')
  <section id="features" class="container services mainbuttons">
   <div class="row">
    <div class="col-lg-12 text-center">
     <div class="navy-line"></div>
     <h1>ebookstore<br/> <span class="navy">Request</span> </h1>
   </div>
 </div>
 <div class="wrapper wrapper-content animated fadeInRight">
   <div class="row">
    @if ($errors->has())
    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      @foreach ($errors->all() as $error)
      {{ $error }}<br>        
      @endforeach
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
      {{ session('success') }}
    </div>
    @endif
  </div>
  <div class="row">
   <div class="col-lg-12">
    <div class="ibox float-e-margins">
     <div class="ibox-content">
      <div class="row">
       <div class="col-sm-12 b-r">
        <h2 class="m-t-none text-center">Request</h2>
        <form method="post" action="{{ url($hotel_data->slug.'/directory/requests/create')}}">
         <div class="form-group">
          <label class="col-sm-2 control-label">
           {{ trans('form.request') }}
         </label>
         <div class="col-sm-10">
           <input type="hidden" name="hotel_id" value="{{$hotel_id}}">
           <textarea class="form-control" name="title" placeholder="Request" data-parsley-id="16" required></textarea>
         </div>
         <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
       </div>
       <br/>
       <br/>
       <br/>
       <br/>
       <br/>
       <div class="form-group">
        <label class="col-sm-2 control-label">{{trans('form.name')}}</label>
        <div class="col-sm-4">
         <input type="text" placeholder="Enter name" class="form-control" name="name" required>
       </div>
       <label class="col-sm-2 control-label">{{trans('form.room_no')}}</label>
       <div class="col-sm-4">
         <input type="text" placeholder="Enter Room No." class="form-control" name="room_no" required>
       </div>
     </div>
     <div class="form-group">
      <div class="col-lg-offset-2 col-lg-10">
       <button class="btn btn-sm btn-primary pull-right m-t-md directory-button" type="submit"><strong>Make Request</strong></button>
     </div>
   </div>
 </form>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
@include('layouts.sectionfooter')
@include('layouts.script')
<script type="text/javascript">
  $(window).load(function() {
    $('.navbar-default').addClass('navbar-scroll');
  });
</script>
</body>
</html>
