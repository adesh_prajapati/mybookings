@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
	<div class="row">
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-content">
					<h3>To-do</h3>
					<p class="small"><i class="fa fa-hand-o-up"></i> Drag request between list</p>
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="input-group">
						<input type="text" placeholder="Add new request. " class="input input-sm form-control">
						<span class="input-group-btn">
							<button type="button" class="btn btn-sm btn-white"> <i class="fa fa-plus"></i> Add request</button>
						</span>
					</div>
					<ul class="sortable-list connectList agile-list" id="todo">
						@foreach($request_details as $request_detail)
						@if($request_detail->status == 'do')
						<li class=" warning-element" id="task{{$request_detail->id}}" data-rel="{{$request_detail->id}}">
							{{$request_detail->title}}
							<div class="agile-detail">
								<span class="pull-right">
									<i class="fa fa-clock-o task-duration" style="color:{{$request_detail->icon_color}}"></i> 
									{{$request_detail->duration}} minutes ago
								</span>
								<span>{{$request_detail->name}} & {{$request_detail->room_no}}</span>
							</div>
						</li>
						@endif
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-content">
					<h3>In Progress</h3>
					<p class="small"><i class="fa fa-hand-o-up"></i> Drag request between list</p>
					<ul class="sortable-list connectList agile-list" id="inprogress">
						@foreach($request_details as $request_detail)
						@if($request_detail->status == 'doing')
						<li class="warning-element" id="task{{$request_detail->id}}" data-rel="{{$request_detail->id}}">
							{{$request_detail->title}}
							<div class="agile-detail">
								<i class="fa fa-clock-o pull-right" style="color:{{$request_detail->icon_color}}"></i> {{$request_detail->duration}} minutes ago
								<!-- <a href="#" class=" btn btn-xs btn-white">Tag</a> -->
							</div>
						</li>
						@endif
						@endforeach
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-4">
			<div class="ibox">
				<div class="ibox-content">
					<h3>Completed</h3>
					<p class="small"><i class="fa fa-hand-o-up"></i> Drag request between list</p>
					<ul class="sortable-list connectList agile-list" id="completed">
						@foreach($request_details as $request_detail)
						@if($request_detail->status == 'done')
						<li class="warning-element" id="task{{$request_detail->id}}" data-rel="{{$request_detail->id}}">
							{{$request_detail->title}}
							<div class="agile-detail">
								
								<i class="fa fa-clock-o pull-right"></i> 
								<span>{{$request_detail->name}} & {{$request_detail->room_no}}</span>
								<!-- <a href="#" class="btn btn-xs btn-white">Tag</a> -->
							</div>
						</li>
						@endif
						@endforeach
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function(){
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$("#todo").sortable({
			connectWith: ".connectList",
			receive: function( event, ui ) {
				var request_id = ui.item.context.dataset.rel;
				var action = ui.item.context.dataset.action;
				var route = "<?php echo route('directory/service/action'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {request_id : request_id, action : 'do'},
					success: function(result) {
						if(result.response == 'success'){
							
						}
					}
				});
			}
		}).disableSelection();
		$("#inprogress").sortable({
			connectWith: ".connectList",
			receive: function( event, ui ) {
				var request_id = ui.item.context.dataset.rel;
				var route = "<?php echo route('directory/service/action'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {request_id : request_id, action : 'doing'},
					success: function(result) {
						if(result.response == 'success'){
							
						}
					}
				});
			}
		}).disableSelection();
		$("#completed").sortable({
			connectWith: ".connectList",
			receive: function( event, ui ) {
				var request_id = ui.item.context.dataset.rel;
				var route = "<?php echo route('directory/service/action'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {request_id : request_id, action : 'done'},
					success: function(result) {
						if(result.response == 'success'){

						}
					}
				});
			}
		}).disableSelection();
		setInterval(function () {
			var route = "<?php echo route('directory/request/status'); ?>";
				$.ajax({
					url: route,
					type: 'GET',
					dataType: 'json',
					data: {request_id : request_id, action : 'do'},
					success: function(result) {
						if(result.response == 'success'){
							
						}
					}
				});
		}, 60000);
	});
</script>
@stop