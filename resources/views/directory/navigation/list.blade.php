@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="wrapper wrapper-content animated fadeInRight ecommerce">
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox">
				<div class="ibox-title">
					<h5>All Navigation Menu for {{Session::get('hotel')['name']}}</h5>
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<div class="ibox-tools">
						<a class="btn btn-primary btn-sm" href="{{ URL::route('directory/navigation/create')}}">
							<i class="fa fa-plus"></i>
							Create new Menu</a>
						</div>
					</div>
					@if($navigation_menu_data->count())
					<div class="ibox-content">
						<table class="footable table table-stripped toggle-arrow-tiny" 
						data-page-size="15">
						<thead>
							<tr>
								<th>Menu Title</th>
								<th data-hide="phone">Type</th>
								<th data-hide="price">Status</th>
								<th class="text-right" data-sort-ignore="true">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach($navigation_menu_data as $navigation_menu)
							<tr id="navigation_menu_data_{{$navigation_menu->id}}">
								<td>
									<a href="">{{$navigation_menu->title}}</a>
								</td>
								<td>
									<i class="fa {{$navigation_menu->icon}}"></i> {{$navigation_menu->page_type}}
								</td>
								<td>
									@if($navigation_menu->is_published == 1)
									<span class="badge badge-primary">Published</span>
									@else
									<span class="badge">UnPublished</span>
									@endif
								</td>
							<td class="text-right">
								<div class="btn-group">
									<a class="btn-white btn btn-xs" 
									href="{{ route('directory/navigation/edit',  ['id' => $navigation_menu->id]) }}">
									<i class="fa fa-edit"></i> Edit</a>
									<a class="btn-white btn btn-xs delete" 
									data-rel="{{$navigation_menu->id}}">
									<i class="fa fa-trash"></i> Delete</a>
								</div>
							</td>
						</tr>
						@endforeach
					</tbody>
						<tfoot>
							<tr>
								<td colspan="6">
									<ul class="pagination pull-right"></ul>
								</td>
							</tr>
						</tfoot>
					</table>

				</div>
				@else
				<br/>
				<div class="ibox-content m-b-sm border-bottom">
					<div class="text-center p-lg">
					<h2>No Navigation Menu available</h2>

					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$('.footable').footable();
		$('.delete').click(function () {
			var navigation_menu_id = $(this).data('rel');
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this Navigation Menu!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, delete",
				cancelButtonText: "No, cancel",
				closeOnConfirm: false,
				closeOnCancel: false },
				function (isConfirm) {
					if (isConfirm) {
						var route = "<?php echo route('directory/navigation/delete'); ?>";
						$.ajax({
							url: route,
							type: 'POST',
							dataType: 'json',
							data: {navigation_menu_id : navigation_menu_id},
							success: function(result) {
								if(result.response == 'success'){
									$('#navigation_menu_data_'+navigation_menu_id).remove();
									swal("Deleted!", "Navigation Menu has been deleted.", "success");
									window.location.reload();
								}
							}
						});
					} else {
						swal("Cancelled", "Navigation Menu is safe :)", "error");
					}
				});
		});
	});

</script>
@stop