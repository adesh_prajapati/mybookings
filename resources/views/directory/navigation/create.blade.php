@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
<link href="{{ URL::asset('asset/css/fontawesome-iconpicker.min.css') }}" rel="stylesheet">
<div class="wrapper wrapper-content">
  <div class="row">
    @if ($errors->has())
    <div class="alert alert-danger alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>
      @foreach ($errors->all() as $error)
      {{ $error }}<br>        
      @endforeach
    </div>
    @endif
    @if (session('success'))
    <div class="alert alert-success alert-dismissable">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">Ã—</button>
      {{ session('success') }}
    </div>
    @endif
  </div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="ibox">
      <div class="ibox-title">
        <h2>Add Navigation Menu</h2>
      </div>
      <div class="ibox-content">
        <form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post" action="{{ URL::route('directory/navigation/create')}}">
          <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
          <input type="hidden" name="navigation_menu_id" value="{{(isset($navigation_menu_deatail)? $navigation_menu_deatail->id: '')}}"/>
          <input type="hidden" id="hotel_id" value="{{Session::get('hotel')['id']}}">
          <h1>Create Page</h1>
          <fieldset>
            <div class="row">
              <div class="form-group">
                <label class="col-sm-2 control-label">{{ trans('form.title') }}
                </label>
                <div class="col-sm-10">
                  <input placeholder="{{ trans('form.title') }}" 
                  name="title" onkeyup="createSlug(this, 'created_slug')" 
                  onblur="createSlug(this, 'created_slug')" type="text" 
                  value="{{(isset($navigation_menu_deatail)? $navigation_menu_deatail->title: '')}}" class="form-control" required="">
                  <p class="created_slug"></p>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">{{ trans('form.page_type') }}
                </label>
                <div class="col-sm-4">
                  @if(isset($navigation_menu_deatail))
                  <select data-placeholder="Choose a Page Type..." id="page_type" class="chzn-select" name="page_type" style="width: 301px">
                    <option value="">Select</option>
                    <option value="contact" @if($navigation_menu_deatail->page_type == 'contacts'){{'selected="selected"'}}@endif >Contacts</option>
                    <option value="deals-tips" @if($navigation_menu_deatail->page_type == 'deals-tips'){{'selected="selected"'}}@endif>Deals/Tips</option>
                    <option value="faq" @if($navigation_menu_deatail->page_type == 'faq'){{'selected="selected"'}}@endif>FAQ</option>
                    <option value="floor" @if($navigation_menu_deatail->page_type == 'floor'){{'selected="selected"'}}@endif>Floor Plan</option>
                    <option value="hotspots" @if($navigation_menu_deatail->page_type == 'hotspots'){{'selected="selected"'}}@endif>Hotspots</option>
                    <option value="menu" @if($navigation_menu_deatail->page_type == 'menu'){{'selected="selected"'}}@endif>Menu</option>
                    <option value="service" @if($navigation_menu_deatail->page_type == 'service'){{'selected="selected"'}}@endif>Services</option>
                    <option value="requests" @if($navigation_menu_deatail->page_type == 'requests'){{'selected="selected"'}}@endif>Request</option>
                    <option value="pages" @if($navigation_menu_deatail->page_type == 'Text'){{'selected="selected"'}}@endif>Text</option>
                  </select>
                  @else
                  <select data-placeholder="Choose a Page Type..." id="page_type" class="chzn-select" name="page_type" style="width: 301px">
                    <option value="">Select</option>
                    <option value="contact" >Contacts</option>
                    <option value="deals-tips">Deals/Tips</option>
                    <option value="faq">FAQ</option>
                    <option value="floor">Floor Plan</option>
                    <option value="hotspots">Hotspots</option>
                    <option value="menu">Menu</option>
                    <option value="service">Services</option>
                    <option value="requests">Request</option>
                    <option value="pages">Text</option>
                  </select>
                  @endif
                </div>
                @if(isset($navigation_menu_deatail))
                @if(isset($navigation_menu_deatail->pankaj))
                <label class="col-sm-2 control-label" id="category_label" >
                  {{ trans('form.category') }}
                </label>
                @else
                <label class="col-sm-2 control-label" id="category_label" style="display:none;">
                  {{ trans('form.category') }}
                </label>
                @endif
                @else
                <label class="col-sm-2 control-label" id="category_label" style="display:none;">
                  {{ trans('form.category') }}
                </label>
                @endif
                <div class="col-sm-4">
                  @if(isset($navigation_menu_deatail))
                  <div class="category-select">
                    <select data-placeholder="Choose a Page Type..." class="chzn-select1" multiple id="category" name="category[]" style="width: 300px">
                      @if(isset($navigation_menu_deatail->pankaj))
                      @foreach($navigation_menu_deatail->pankaj as $page_type)
                      <option value="{{$page_type}}" selected="selected">{{$page_type}}</option>
                      @endforeach
                      @endif
                    </select>
                  </div>
                  @else
                  <div class="category-select">

                  </div>
                  @endif
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label">
                  {{ trans('form.icon') }}
                </label>
                <div class="col-sm-4">
                  <div class="input-group iconpicker-container">
                    <input type="text" name="icon_name" value="{{(isset($navigation_menu_deatail)? $navigation_menu_deatail->icon: '')}}" class="form-control icp icp-auto iconpicker-element iconpicker-input" data-placement="bottomRight">
                    <span class="input-group-addon"><i class="{{(isset($navigation_menu_deatail)? $navigation_menu_deatail->icon: '')}}"></i></span>
                  </div>
                </div>
                <div class="col-sm-2 col-sm-offset-2">
                  <div class="i-checks">
                    <label>
                      <input type="checkbox" id="itself-chain" 
                      name="is_published" value="on" 
                      data-parsley-group="block1" checked="">
                      <i></i>{{ trans('form.published') }}</label></div>
                    </div>
                  </div>
                </div>
              </fieldset>
              <h1>Finish</h1>
              <fieldset>
                <h2>Terms and Conditions</h2>
                <input id="acceptTerms" name="acceptTerms" type="checkbox"/> 
                <label for="acceptTerms">I agree with the Terms and Conditions.</label>
              </fieldset>
            </form>
          </div>
        </div>
      </div>
    </div>
    @include('layouts.script')
    <script>
      function createSlug(val, type){
        if(val.value.length == 0){
          $('.'+type).html('');
        }else{
          var trimmed = $.trim(val.value);
          $slug = trimmed.replace(/[^a-z0-9-]/gi, '-').replace(/-+/g, '-').replace(/^-|-$/g, '');
          $('.'+type).html($slug.toLowerCase());
        }
      }
      $(document).ready(function(){
        $.ajaxSetup({
          headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
        });
        $("#wizard").steps();
        $("#form").steps({
          bodyTag: "fieldset",
          onStepChanging: function (event, currentIndex, newIndex){
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex){
                      return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                      return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18){
                      $(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3){
                      $(this).steps("previous");
                    }
                },
                onCanceled:function(){
                  window.location = '<?php echo route('directory/navigation'); ?>';
                },
                onFinishing: function (event, currentIndex){
                  var form = $(this);
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },onFinished: function (event, currentIndex){
                  var form = $(this);
                    // Submit form input
                    form.submit();
                }
            }).validate({
              errorPlacement: function (error, element){
                element.after(error);
              }

            });
            $('.i-checks').iCheck({
              checkboxClass: 'icheckbox_square-green'
            });
            var config = {
              '.chzn-select'           : {},
              '.chzn-select1'           : {},
              '.chzn-select2'           : {},
            }
            for (var selector in config) {
              $(selector).chosen(config[selector]);
            }
            $('.chzn-select').chosen().change(function(){
              var value = $('#page_type option:selected').text();
              if(value == 'Request' || value == 'Floor Plan'|| value == 'FAQ'){
                $('#category_label').hide();
                $('.category-select').empty();
              }

              if(value == 'Deals/Tips'){
                $('#category_label').show();
                $('#category_label').html('Type');
                $('.category-select').html('');
                $(".chzn-select1").empty();
                $('.category-select').append('<select data-placeholder="Choose a Page Type..." class="chzn-select1" multiple id="category" name="category[]" style="width: 300px">')
                $('.chzn-select1').append("<option value='tip'>Tips</option>");
                $('.chzn-select1').append("<option value='deal'>Deals</option>");
                $('.chzn-select1').chosen().trigger("chosen:updated");
              }
              if(value == 'Menu' || value == 'Services'){
                $('#category_label').show();
                $(".chzn-select1").empty();
                $('#category_label').html('Category');
                $('.category-select').html('');
                $('.category-select').append('<select data-placeholder="Choose a Page Type..." class="chzn-select1" multiple id="category" name="category[]" style="width: 300px">')
                var menu_data = ['Breakfast','Lunch','Dinner','Drinks','Room Service','Services','Other'];
                $('').append('<select data-placeholder="Choose a Page Type..." class="chzn-select1" multiple id="category" name="category[]" style="width: 300px">')
                $.each(menu_data, function( index, value ) {
                  $('.chzn-select1').append("<option value="+value+">"+value+"</option>");
                });
                $('.chzn-select1').chosen().trigger("chosen:updated");
              }
              if(value == 'Hotspots' || value == 'Contacts'){
                $('#category_label').show();
                $(".chzn-select1").empty();
                $('.category-select').html('');
                $('.category-select').append('<select data-placeholder="Choose a Page Type..." class="chzn-select1" multiple id="category" name="category[]" style="width: 300px">')
                var hotspots_data = ['Airport', 'Amusement','Bank/ATM','Casino','Embassy','Emergency / Medical','Grocery store','Gym','Movie Theater','Museum','Nightlife','Park','Public transport','Religious buildings','Restaurant','Spa'];
                $.each(hotspots_data, function( index, value ) {
                  $('.chzn-select1').append("<option value="+value+">"+value+"</option>");
                });
                $('.chzn-select1').chosen().trigger("chosen:updated");
              }
              if(value == 'Text'){
                var hotel_id = $('#hotel_id').val();
                var route = "<?php echo route('directory/static/text'); ?>";
                $.ajax({
                  url: route,
                  type: 'POST',
                  data: {hotel_id: hotel_id},
                  dataType: 'json',
                  success: function(result) {
                    $('#category_label').show();
                    $('.category-select').html('');
                    $(".chzn-select2").empty();
                    $('.category-select').append('<select data-placeholder="Choose a Page Type..." class="chzn-select1" id="category" name="category" style="width: 300px">')
                    $.each(result, function( index, value ) {
                      $('.chzn-select1').append("<option value='"+ index +"''>"+value+"</option>");
                    });
                    $('.chzn-select1').chosen().trigger("chosen:updated");
                  }
                });
              }
            });
});
</script>
<script>
  $(function() {
    $('.icp').iconpicker();
  });
</script>
@stop