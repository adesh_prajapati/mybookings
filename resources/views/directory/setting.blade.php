@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Hotel Directory Setting</h5>
				</div>
				<div class="ibox-content">
					<form  data-parsley-validate class="form-horizontal" 
					method="post" action="{{ URL::route('directory/setting/create')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<input type="hidden" name="directroy_setting_id" value="{{(isset($directory_data)? $directory_data->id : '')}}"/>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.title_color') }}
						</label>
						<div class="col-sm-9 input-group demo1">
							<input type="text" value="{{(isset($directory_data)? $directory_data->title_color : '')}}" name="title_color" class="form-control colorpickInput" placeholder="Title Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.text_color') }}
						</label>
						<div class="col-sm-9 input-group demo2">
							<input type="text" value="{{(isset($directory_data)? $directory_data->text_color : '')}}" name="text_color" class="form-control colorpickInput" placeholder="Text Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.text_link_color') }}
						</label>
						<div class="col-sm-9 input-group demo3">
							<input type="text" name="text_link_color" value="{{(isset($directory_data)? $directory_data->text_link_color : '')}}" class="form-control colorpickInput" placeholder="Text Link Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.text_link_hover_color') }}
						</label>
						<div class="col-sm-9 input-group demo3">
							<input type="text" name="text_link_hover_color" value="{{(isset($directory_data)? $directory_data->text_link_hover_color : '')}}" class="form-control colorpickInput" 
							placeholder="Text Link Hover Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.icon_color') }}
						</label>
						<div class="col-sm-9 input-group demo4">
							<input type="text" name="icon_color" value="{{(isset($directory_data)? $directory_data->icon_color : '')}}" class="form-control colorpickInput" placeholder="Icon Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.sub_heading_color') }}
						</label>
						<div class="col-sm-9 input-group demo4">
							<input type="text" name="sub_heading_color" value="{{(isset($directory_data)? $directory_data->sub_heading_color : '')}}" class="form-control colorpickInput" placeholder="Sub Heading Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">
							{{ trans('form.button_color') }}
						</label>
						<div class="col-sm-9 input-group demo5">
							<input type="text" name="button_color" value="{{(isset($directory_data)? $directory_data->button_color : '')}}" class="form-control colorpickInput" placeholder="Button Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">
							{{ trans('form.button_hover_color') }}
						</label>
						<div class="col-sm-9 input-group demo5">
							<input type="text" name="button_hover_color" value="{{(isset($directory_data)? $directory_data->button_hover_color : '')}}" class="form-control colorpickInput" placeholder="Button Hover Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">
							{{ trans('form.button_text_color') }}
						</label>
						<div class="col-sm-9 input-group demo6">
							<input type="text" name="button_text_color" value="{{(isset($directory_data)? $directory_data->button_text_color : '')}}" class="form-control colorpickInput" placeholder="Button Text Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-lg-2 control-label">
							{{ trans('form.button_text_hover_color') }}
						</label>
						<div class="col-sm-9 input-group demo6">
							<input type="text" name="button_text_hover_color" value="{{(isset($directory_data)? $directory_data->button_text_hover_color : '')}}" class="form-control colorpickInput" placeholder="Button Text Hover Color" />
							<span class="input-group-addon"><i></i></span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.intro_text') }}
						</label>
						<div class="col-sm-9">
							<div class="ibox float-e-margins">
								<div class="ibox-content no-padding border-custom">
									<textarea class="summernote" name="intro_text">{{(isset($directory_data)? $directory_data->intro_text : '')}}</textarea>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">
							{{ trans('form.include_script') }}
						</label>
						<div class="col-sm-9">
							<textarea placeholder="Incude Script" id="script"
							name="include_script" class="form-control">{{(isset($directory_data)? $directory_data->include_script : '')}}</textarea>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">
						{{ trans('form.include_css') }}
					</label>
					<div class="col-sm-9">
						<textarea placeholder="Incude Css" 
						name="include_css" class="form-control ">{{(isset($directory_data)? $directory_data->include_css : '')}}
					</textarea>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{ trans('form.sub_heading') }}
				</label>
				<div class="col-sm-9">
					<input type="text" value="{{(isset($directory_data)? $directory_data->sub_heading : '')}}" name="sub_heading" class="form-control" placeholder="{{ trans('form.sub_heading') }}" />
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label">
					{{ trans('form.privacy_statement') }}
				</label>
				<div class="col-sm-9">
					<div class="ibox float-e-margins">
						<div class="ibox-content no-padding border-custom">
							<textarea class="summernote" id="privacy_statement" name="privacy_statement">{{(isset($directory_data)? $directory_data->privacy_statement : '')}}
							</textarea>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-lg-offset-2 col-lg-10">
                                    <button type="submit" class="btn btn-primary pull-center"><i class="fa fa-save"></i> {{(isset($directory_data)? trans('form.update') : trans('form.submit'))}}</button>
				</div>
			</div>
		</form>
	</div>
</div>
</div>
</div>
</div>
@include('layouts.script')
<script type="text/javascript">
	$.ajaxSetup({
		headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
	
	$(document).ready(function () {
		$('.demo1').colorpicker();
		$('.demo2').colorpicker();
		$('.demo3').colorpicker();
		$('.demo4').colorpicker();
		$('.demo5').colorpicker();
		$('.demo6').colorpicker();
	});
        
        $(document).ready(function(){
            $('textarea#script').val($('.note-editable').html());
        });
        
</script>
<script src="{{ URL::asset('asset/js/generic_scripts.js') }}"></script>
@stop