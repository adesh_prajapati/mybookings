
<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<body id="page-top" class="landing-page home">
    @include('layouts.header')
    <div id="inSlider" class="carousel carousel-fade" data-ride="carousel">
        <ol class="carousel-indicators">
            <?php $i = 0 ?>
            @foreach($deals_data  as $deals)
                <?php $class = ''; ?>
                @if($i == 0)
                    <?php $class = ' active' ?>
                @endif
                <li data-target="#inSlider" data-slide-to="<?php echo $i; ?>" class="<?php echo $class;?>"></li>
                <?php  $i++; ?>
            @endforeach
        </ol>
        @if($deals_data->count())
        <div class="carousel-inner" role="listbox">
            <?php $i = 0 ?>
            @foreach($deals_data  as $deals)
                @if($i == 0)
                    <?php $class = ' active' ?>
                @else
                    <?php $class = '' ?>
                @endif
                <?php $i++ ?>
                <div class="item{{$class}}">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>
                                {{$deals->title}}
                            </h1>
                            <p>{{$deals->short_description}}</p>
                            <p>
                                <a class="btn btn-lg btn-primary directory-button" href="#" role="button">More info</a>
                            </p>
                        </div>
                    </div>
                    <!-- Set background for slide in css -->
                    <div class="header-back one" style="background: url({{Url::asset('/hotelfiles/'.$deals->hotel_id.'/'.'images/'.$deals->image)}}) 50% 0 no-repeat;">
                    </div>
                </div>
            @endforeach
        </div>
        @else
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <div class="header-back one">
                </div>
            </div>
        </div>
        @endif
        <a class="left carousel-control" href="#inSlider" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#inSlider" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>


    <section id="features" class="container services mainbuttons">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="navy-line text-line"></div>
                <h1>{{ $hotel_data->name }}<br/> <span class="navy sub_heading">{{(isset($directory_setting)?$directory_setting->sub_heading:'')}}</span> </h1>
                <p>{{(isset($directory_setting)? strip_tags($directory_setting->intro_text): '')}}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 col-sm-4  col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/hotspots')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Hotspots</h2>
                    <p><i class="fa fa-map-marker"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/deals-tips')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Tips & Deals</h2>
                    <p><i class="fa fa-gift"></i></p>
                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/service')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Services</h2>
                    <p><i class="fa fa-user"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/faq')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>FAQ</h2>
                    <p><i class="fa fa-question"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/floor')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Floor plan</h2>
                    <p><i class="fa fa-map"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/menu')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Food & Beverages</h2>
                    <p><i class="fa fa-cutlery"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/gallery')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Photos</h2>
                    <p><i class="fa fa-camera"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/contact')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Contacts</h2>
                    <p><i class="fa fa-phone"></i></p>

                </a>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
                <a href="{{url($hotel_data->slug.'/directory/requests')}}" class="btn btn-primary btn-circle btn-lg btn-xxl">
                    <h2>Requests</h2>
                    <p><i class="fa fa-phone"></i></p>

                </a>
            </div>
        </div>
    </section>
    @include('layouts.sectionfooter')
    <!-- Mainly scripts -->
    <script src="{{ URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
    <script src="{{ URL::asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <!-- Custom and plugin javascript -->
    <script src="{{ URL::asset('asset/js/inspinia.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/pace/pace.min.js') }}"></script>
    <script src="{{ URL::asset('asset/js/plugins/wow/wow.min.js') }}"></script>
    <script>
        $(document).ready(function () {

            $('body').scrollspy({
                target: '.navbar-fixed-top',
                offset: 80
            });

        // Page scrolling feature
        $('a.page-scroll').bind('click', function(event) {
            var link = $(this);
            $('html, body').stop().animate({
                scrollTop: $(link.attr('href')).offset().top - 50
            }, 500);
            event.preventDefault();
            $("#navbar").collapse('hide');
        });
    });

        var cbpAnimatedHeader = (function() {
            var docElem = document.documentElement,
            header = document.querySelector( '.navbar-default' ),
            didScroll = false,
            changeHeaderOn = 200;
            function init() {
                window.addEventListener( 'scroll', function( event ) {
                    if( !didScroll ) {
                        didScroll = true;
                        setTimeout( scrollPage, 250 );
                    }
                }, false );
            }
            function scrollPage() {
                var sy = scrollY();
                if ( sy >= changeHeaderOn ) {
                    $(header).addClass('navbar-scroll')
                }
                else {
                    $(header).removeClass('navbar-scroll')
                }
                didScroll = false;
            }
            function scrollY() {
                return window.pageYOffset || docElem.scrollTop;
            }
            init();

        })();

    // Activate WOW.js plugin for animation on scrol
    new WOW().init();

</script>

</body>
</html>
