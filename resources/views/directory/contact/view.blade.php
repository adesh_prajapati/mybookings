<!DOCTYPE html>
<html lang="en">
@include('layouts.userhead')
<style>
    .grid .ibox {
        margin-bottom: 0;
    }
    .grid-item {
        margin-bottom: 25px;
        width: 350px;
    }
</style>
<body id="page-top" class="landing-page">
	@include('layouts.header')
    <input type="hidden" value="{{ csrf_token()}}" name="_token">
    <section id="features" class="container services mainbuttons">
    	<div class="row">
    		<div class="col-lg-12 text-center">
    			<div class="navy-line"></div>
    			<h1 class="navy"> Contact</span> </h1>
    		</div>
    	</div>
        <div class="row">
            <div class="col-lg-12">
            @if($contact_details)
                <div class="wrapper wrapper-content animated fadeInRight">
                    <div class="ibox-content forum-container">
                        @foreach ($contact_details as $key => $contact_detail)
                        <div class="forum-title">
                            <h3>{{$key}}</h3>
                        </div>
                        @foreach($contact_detail as $contact)
                        <div class="forum-item active">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="forum-icon">
                                     <i style="font-size:2em;" class="fa fa-phone"></i>
                                 </div>
                                 <span class="forum-item-title"><h3>{{$contact->title}}</h3></span>
                                 <div class="forum-sub-title">{{$contact->phone}}</div>
                             </div>
                         </div>
                     </div>
                     @endforeach
                     @endforeach
                 </div>
             </div>
             @else
             <div class="ibox-content m-b-sm border-bottom">
                <div class="text-center p-lg">
                    <h2>No Contact available</h2>

                </div>
            </div>
            @endif
        </div>
    </div>
</section>
@include('layouts.sectionfooter')
@include('layouts.script')
<script>
    $(window).load(function() {
        $('.grid').masonry({
            itemSelector: '.grid-item',
            columnWidth: 350,
            gutter: 25
        });
        $('.navbar-default').addClass('navbar-scroll')
    });
</script>
</body>
</html>
