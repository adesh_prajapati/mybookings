@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<style>
	.wizard > .content > .body  position: relative; }
</style>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h2>Add/Edit Contact</h2>
			</div>
			<div class="ibox-content">
				<form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post" action="{{ URL::route('create-hotel-pages')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<h1>Create pages</h1>
					<fieldset>
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-sm m-t-sm">
									<input id="title" placeholder="Title" name="userName" type="text" class="form-control required">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="col-lg-12">
								<div class="m-b-sm m-t-sm">
									<input id="title" placeholder="Address" name="userName" type="text" class="form-control required">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-content no-padding">
										<div class="summernote">
											
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="col-lg-12">
								<div class="col-lg-6 m-b-sm m-t-sm">
									<input id="title" placeholder="Website" name="website" type="text" class="form-control">
								</div>
								<div class="col-lg-6 m-b-sm m-t-sm">
									<input id="title" placeholder="Address" name="userName" type="text" class="form-control">
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="col-lg-12">
								<div class="col-lg-6 m-b-sm m-t-sm">
									<input id="title" placeholder="Phone" name="phone" type="text" class="form-control ">
								</div>
								<div class="col-lg-6 m-b-sm m-t-sm">
									<div class="checkbox checkbox-success">
										<input type="checkbox" id="checkbox3">
										<label for="checkbox3">
											Show as Hotspot
										</label>
									</div>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
							<div class="col-lg-12">
								<div class="checkbox checkbox-success">
									<input type="checkbox" id="checkbox3">
									<label for="checkbox3">
										Published
									</label>
								</div>
							</div>
							<div class="hr-line-dashed"></div>
						</div>
					</fieldset>
					<h1>Finish</h1>
					<fieldset>
						<h2>Terms and Conditions</h2>
						<input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> 
						<label for="acceptTerms">I agree with the Terms and Conditions.</label>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function(){
		$("#wizard").steps();
		$("#form").steps({
			bodyTag: "fieldset",
			onStepChanging: function (event, currentIndex, newIndex){
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex){
                    	return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                    	return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18){
                    	$(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3){
                    	$(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex){
                	var form = $(this);
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },onFinished: function (event, currentIndex){
                	var form = $(this);
                    // Submit form input
                    form.submit();
                }
            }).validate({
            	errorPlacement: function (error, element){
            		element.before(error);
            	},
            	rules: {
            		confirm: {
            			equalTo: "#password"
            		}
            	}
            });
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.summernote').summernote();
        });
    </script>
    @stop