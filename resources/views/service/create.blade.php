@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox">
			<div class="ibox-title">
				<h2>Add Menu/Service</h2>
			</div>
			<div class="ibox-content">
				<form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post" action="{{ URL::route('directory/service/create')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<input type="hidden" name="menu_id" value="{{(isset($menu_detail)? $menu_detail->id: '')}}"/>
					<h1>Create Menu/Service</h1>
					<fieldset>
						<div class="row">
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.title') }}
								</label>
								<div class="col-sm-10">
									<input placeholder="{{ trans('form.title') }}" 
									name="title" type="text" name="title" 
									value="{{(isset($menu_detail)? $menu_detail->title: '')}}" class="form-control" required="">
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.sub_text') }}
								</label>
								<div class="col-sm-10">
									<input placeholder="{{ trans('form.sub_text') }}" 
									type="text" name="sub_text" value="{{(isset($menu_detail)? $menu_detail->sub_text: '')}}" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">{{ trans('form.currency') }}
								</label>
								<div class="col-sm-4">
									@if(isset($menu_detail))
									<select class="chzn-select" name="currency" style="width: 350px">
										<option value="0">Select Currency</option>
										<option @if($menu_detail->currency == 'Pesos'){{'selected="selected"'}}@endif value="Pesos">Pesos</option>
										<option @if($menu_detail->currency == 'Rand'){{'selected="selected"'}}@endif value="Rand">Rand</option>
										<option @if($menu_detail->currency == 'Euro'){{'selected="selected"'}}@endif value="Euro">Euro</option>
										<option @if($menu_detail->currency == 'Dollars'){{'selected="selected"'}}@endif value="Dollars">Dollars</option>
										<option @if($menu_detail->currency == 'Pounds'){{'selected="selected"'}}@endif value="Pounds">Pounds</option>
									</select>
									@else
										<select class="chzn-select" name="currency" style="width: 350px">
										<option value="Pesos">Pesos</option>
										<option value="Rand">Rand</option>
										<option value="Euro">Euro</option>
										<option value="Dollars">Dollars</option>
										<option value="Pounds">Pounds</option>
									</select>
									@endif
								</div>
								<label class="col-sm-2 control-label">
								{{ trans('form.price') }}
								</label>
								<div class="col-sm-4">
									<input placeholder="{{ trans('form.price') }}" 
									 type="text" name="price" value="{{(isset($menu_detail)? $menu_detail->price: '')}}" class="form-control">
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">
								{{ trans('form.category') }}
								</label>
								<div class="col-sm-4">
									@if(isset($menu_detail))
									<select class="chzn-select" name="category" style="width: 350px">
											<option @if($menu_detail->category == 'Breakfast'){{'selected="selected"'}}@endif value="Breakfast">Breakfast</option>
											<option @if($menu_detail->category == 'Lunch'){{'selected="selected"'}}@endif value="Lunch">Lunch</option>
											<option @if($menu_detail->category == 'Dinner'){{'selected="selected"'}}@endif value="Dinner">Dinner</option>
											<option @if($menu_detail->category == 'Drinks'){{'selected="selected"'}}@endif value="Drinks">Drinks</option>
									</select>
									@else
									<select class="chzn-select" name="category" style="width: 350px">
											@foreach($category_data as $category)
											<option value="{{$category}}">{{$category}}</option>
											@endforeach
									</select>
									@endif
								</div>
								<div class="col-sm-2 col-sm-offset-2">
									<div class="i-checks">
										<label>
											@if(isset($menu_detail)) 
											<input type="checkbox" id="itself-chain" 
											name="is_published" value="on" 
											data-parsley-group="block1" {{(($menu_detail->is_published == 1)? 'checked=""': '')}}>
											@else
											<input type="checkbox" id="itself-chain" 
											name="is_published" value="on" 
											data-parsley-group="block1" checked="">
											@endif 
											<i></i>{{ trans('form.published') }}</label></div>
										</div>
									</div>
								</div>
							</fieldset>
							<h1>Finish</h1>
							<fieldset>
								<h2>Terms and Conditions</h2>
								<input id="acceptTerms" name="acceptTerms" type="checkbox"/> 
								<label for="acceptTerms">I agree with the Terms and Conditions.</label>
							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
		@include('layouts.script')
		<script>
			$(document).ready(function(){
				$("#wizard").steps();
				$("#form").steps({
					bodyTag: "fieldset",
					onStepChanging: function (event, currentIndex, newIndex){
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex){
                    	return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                    	return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18){
                    	$(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3){
                    	$(this).steps("previous");
                    }
                },
                onCanceled:function(){
					window.location = '<?php echo route('directory/service'); ?>';
				},
                onFinishing: function (event, currentIndex){
                	var form = $(this);
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },onFinished: function (event, currentIndex){
                	var form = $(this);
                    // Submit form input
                    form.submit();
                }
            }).validate({
            	errorPlacement: function (error, element){
            		element.after(error);
            	},
            	rules: {
            		confirm: {
            			equalTo: "#password"
            		}
            	}
            });
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.summernote').summernote();
            $('.i-checks').iCheck({
            	checkboxClass: 'icheckbox_square-green'
            });
            $(".chzn-select").chosen({
            	create_option: true,
            	persistent_create_option: true,
            	create_option_text: 'add',
            });
        });
    </script>
    @stop