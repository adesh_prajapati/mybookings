<!DOCTYPE html>
<html lang="en">
@include('layouts.head')
<body id="page-top" class="landing-page">
	@include('layouts.header')
  <input type="hidden" value="{{ csrf_token()}}" name="_token">
  <section id="features" class="container services mainbuttons">
   <div class="row">
    <div class="col-lg-12 text-center">
     <div class="navy-line"></div>
     <h1>ebookstore<br/> <span class="navy">Service</span> </h1>
   </div>
 </div>
 <div class="wrapper wrapper-content animated fadeInRight">
  <div class="row">
    <div class="col-lg-12">
      <div class="wrapper wrapper-content animated fadeInRight">
        @if($menu_data)
        <div class="ibox-content forum-container">
          @foreach ($menu_data as $key => $menu)
          <div class="forum-title">
            <h3>{{$key}}</h3>
          </div>
          @foreach($menu as $data)
          <div class="forum-item active">
            <div class="row">
              <div class="col-md-9">
                <div class="forum-icon">
                 <i class="fa fa-cutlery"></i>
               </div>
               <span class="forum-item-title"><h3>{{$data->title}}</h3></span>
               <div class="forum-sub-title">{{$data->sub_text}}</div>
             </div>
             <div class="col-md-1 forum-info pull-right">
               <span><i class="fa fa-usd"></i></span>
               <span>{{$data->price}}</span>
             </div>
           </div>
         </div>
         @endforeach
         @endforeach
       </div>
     </div>
     @else
     <div class="ibox-content m-b-sm border-bottom">
      <div class="text-center p-lg">
      <h2>No Menu available</h2>

      </div>
    </div>
    @endif
  </div>
</div>
</div>
</section>
@include('layouts.sectionfooter')

<script src="{{ URL::asset('asset/js/jquery-2.1.1.js') }}"></script>
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('asset/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ URL::asset('asset/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- Custom and plugin javascript -->
<script src="{{ URL::asset('asset/js/inspinia.js') }}"></script>
<script src="{{ URL::asset('asset/js/plugins/pace/pace.min.js') }}"></script>
<script src="{{ URL::asset('asset/js/plugins/wow/wow.min.js') }}"></script>
<script type="text/javascript">
 $(document).ready(function () {
  $('.navbar-default').addClass('navbar-scroll');
});
</script>
</body>
</html>
