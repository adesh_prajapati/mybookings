@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<div class="row">
	<div class="col-lg-12">
		<div class="ibox float-e-margins">
			<div class="ibox-title">
				<h5>All Menu for {{Session::get('hotel')['name']}}</h5>
				<input type="hidden" value="{{ csrf_token()}}" name="_token">
				<div class="ibox-tools">
					<a href="{{route('directory/service/create')}}" class="btn btn-primary btn-sm">
						<i class="fa fa-plus"></i>
						Create new Menu</a>
						<a class="btn btn-primary btn-sm" id="update_index"><i class="fa fa-save"></i> Update Index</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-content">
					<div class="table-responsive">
						@if($menu_details->count())
						<table class="table table-striped">
							<thead>
								<tr>
									<th></th>
									<th>Name</th>
									<th>Category</th>
									<th class="text-right data-sort-ignore="true"">Action</th>
									<th></th>
								</tr>
							</thead>
							<tr>
								<td colspan="5" class="text-center"><h2>Breakfast</h2></td>
							</tr>
							<tbody  id="sortable">
								<?php $i = 1 ?>
								@foreach($menu_details as $menu_detail)
								@if($menu_detail->category == 'Breakfast')
								<tr class="ui-state-default" data-rel="{{$menu_detail->category}}"> 
									<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{($menu_detail->index) ? $menu_detail->index:$i++}}</td>
									<td>{{$menu_detail->title}}</td>
									<td>{{$menu_detail->category}}</td>
									<td class="text-right">
										<div class="btn-group">
											<a class="btn-white btn btn-xs" 
											href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
											<i class="fa fa-edit"></i> Edit</a>
											<a class="btn-white btn btn-xs delete" 
											data-rel="{{$menu_detail->id}}">
											<i class="fa fa-trash"></i> Delete</a>
										</div>
									</td>
									<td></td>
								</tr>
								@endif
								@endforeach
								<tr>
									<td colspan="5" class="breakfast text-center" style="display:none;"><h3>Breakfast list not available</h3></td>
								</tr>
							</tbody>
							<tr>
								<td colspan="5" class="text-center"><h2>Lunch</h2></td>
							</tr>
							<tbody  id="sortable1">
								<?php $i = 1 ?>
								@foreach($menu_details as $menu_detail)
								@if($menu_detail->category == 'Lunch')
								<tr class="ui-state-default" data-rel="{{$menu_detail->category}}"> 
									<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
									<td>{{$menu_detail->title}}</td>
									<td>{{$menu_detail->category}}</td>
									<td class="text-right">
										<div class="btn-group">
											<a class="btn-white btn btn-xs" 
											href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
											<i class="fa fa-edit"></i> Edit</a>
											<a class="btn-white btn btn-xs delete" 
											data-rel="{{$menu_detail->id}}">
											<i class="fa fa-trash"></i> Delete</a>
										</div>
									</td>
									<td></td>
								</tr>
								@endif
								@endforeach
								<tr>
									<td colspan="5" class="lunch text-center" style="display:none;"><h3>Lunch list not available</h3></td>
								</tr>
							</tbody>
							<tr>
								<td colspan="5" class="text-center"><h2>Dinner</h2></td>
							</tr>
							<tbody  id="sortable2">
								@foreach($menu_details as $menu_detail)
								@if($menu_detail->category == 'Dinner')
								<tr class="ui-state-default" data-rel="{{$menu_detail->category}}"> 
									<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
									<td>{{$menu_detail->title}}</td>
									<td>{{$menu_detail->category}}</td>
									<td class="text-right">
										<div class="btn-group">
											<a class="btn-white btn btn-xs" 
											href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
											<i class="fa fa-edit"></i> Edit</a>
											<a class="btn-white btn btn-xs delete" 
											data-rel="{{$menu_detail->id}}">
											<i class="fa fa-trash"></i> Delete</a>
										</div>
									</td>
									<td></td>
								</tr>
								@endif
								@endforeach
								<tr>
									<td colspan="5" class="dinner text-center" style="display:none;"><h3>Dinner list not available</h3></td>
								</tr>
							</tbody>
							<tr>
								<td colspan="5" class="text-center"><h2>Drinks</h2></td>
							</tr>
							<tbody  id="sortable3">
								<?php $i = 1 ?>
								@foreach($menu_details as $key => $menu_detail)
								@if($menu_detail->category == 'Drinks')
								<tr class="ui-state-default-{{$menu_detail->id}}" data-rel="{{$menu_detail->category}}"> 
									<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
									<td>{{$menu_detail->title}}</td>
									<td>{{$menu_detail->category}}</td>
									<td class="text-right">
										<div class="btn-group">
											<a class="btn-white btn btn-xs" 
											href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
											<i class="fa fa-edit"></i> Edit</a>
											<a class="btn-white btn btn-xs delete" 
											data-rel="{{$menu_detail->id}}">
											<i class="fa fa-trash"></i> Delete</a>
										</div>
									</td>
									<td></td>
								</tr>
								@endif
								@endforeach
								<tr>
									<td colspan="5" class="drinks text-center" style="display:none;"><h3>
										Drinks not available</h3></td>
									</tr>
								</tbody>
								<tr>
									<td colspan="5" class="text-center"><h2>Room Service</h2></td>
								</tr>
								<tbody  id="sortable4">
									<?php $i = 1 ?>
									@foreach($menu_details as $key => $menu_detail)
									@if($menu_detail->category == 'Room Service')
									<tr class="ui-state-default-{{$menu_detail->id}}" data-rel="{{$menu_detail->category}}"> 
										<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
										<td>{{$menu_detail->title}}</td>
										<td>{{$menu_detail->category}}</td>
										<td class="text-right">
											<div class="btn-group">
												<a class="btn-white btn btn-xs" 
												href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
												<i class="fa fa-edit"></i> Edit</a>
												<a class="btn-white btn btn-xs delete" 
												data-rel="{{$menu_detail->id}}">
												<i class="fa fa-trash"></i> Delete</a>
											</div>
										</td>
										<td></td>
									</tr>
									@endif
									@endforeach
									<tr>
										<td colspan="5" class="room-service text-center" style="display:none;"><h3>Room Service not available</h3></td>
									</tr>
								</tbody>
								<tr>
									<td colspan="5" class="text-center"><h2>Services</h2></td>
								</tr>
								<tbody  id="sortable5">
									<?php $i = 1 ?>
									@foreach($menu_details as $key => $menu_detail)
									@if($menu_detail->category == 'Services')
									<tr class="ui-state-default-{{$menu_detail->id}}" data-rel="{{$menu_detail->category}}"> 
										<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
										<td>{{$menu_detail->title}}</td>
										<td>{{$menu_detail->category}}</td>
										<td class="text-right">
											<div class="btn-group">
												<a class="btn-white btn btn-xs" 
												href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
												<i class="fa fa-edit"></i> Edit</a>
												<a class="btn-white btn btn-xs delete" 
												data-rel="{{$menu_detail->id}}">
												<i class="fa fa-trash"></i> Delete</a>
											</div>
										</td>
										<td></td>
									</tr>
									@endif
									@endforeach
									<tr>
										<td colspan="5" class="service text-center" style="display:none;"><h3>Services not available</h3></td>
									</tr>
								</tbody>
								<tr>
								<td colspan="5" class="text-center"><h2>Other</h2></td>
								</tr>
								<tbody  id="sortable6">
									<?php $i = 1 ?>
									@foreach($menu_details as $key => $menu_detail)
									@if($menu_detail->category == 'Other')
									<tr class="ui-state-default-{{$menu_detail->id}}" data-rel="{{$menu_detail->category}}"> 
										<td class="index" data-id="{{$menu_detail->id}}" data-category="{{$menu_detail->category}}">{{$i++}}</td>
										<td>{{$menu_detail->title}}</td>
										<td>{{$menu_detail->category}}</td>
										<td class="text-right">
											<div class="btn-group">
												<a class="btn-white btn btn-xs" 
												href="{{ route('directory/service/edit',  ['id' => $menu_detail->id]) }}">
												<i class="fa fa-edit"></i> Edit</a>
												<a class="btn-white btn btn-xs delete" 
												data-rel="{{$menu_detail->id}}">
												<i class="fa fa-trash"></i> Delete</a>
											</div>
										</td>
										<td></td>
									</tr>
									@endif
									@endforeach
									<tr>
										<td colspan="5" class="other text-center" style="display:none;"><h3>Other not available</h3></td>
									</tr>
								</tbody>
							</table>
							@else
							<br/>
							<div class="ibox-content m-b-sm border-bottom">
								<div class="text-center p-lg">
									<h2>No Menu available</h2>

								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>

		@include('layouts.script')
		<script>
			$(document).ready(function(){
				$.ajaxSetup({
					headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
				});
				var updateIndex = function(e, ui) {
					$('td.index', ui.item.parent()).each(function (i) {
						$(this).html(i + 1);
					});
				};
				var Breakfast_menu = $('#sortable tr').length;
				var lunch_menu = $('#sortable1 tr').length;
				var dinner_menu = $('#sortable2 tr').length;
				var drink_menu = $('#sortable3 tr').length;
				var room_service_menu = $('#sortable4 tr').length;
				var service_menu = $('#sortable5 tr').length;
				var other_menu = $('#sortable6 tr').length;
				if(Breakfast_menu < 2){
					$('.breakfast').show();
				}
				if(lunch_menu < 2){
					$('.lunch').show();
				}
				if(dinner_menu < 2){
					$('.dinner').show();
				}
				if(drink_menu < 2){
					$('.drinks').show();
				}
				if(room_service_menu < 2){
					$('.room-service').show();
				}
				if(service_menu < 2){
					$('.service').show();
				}
				if(other_menu < 2){
					$('.other').show();
				}
				$( "#sortable" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable1" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable2" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable3" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable4" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable5" ).sortable({stop: updateIndex}).disableSelection();
				$( "#sortable6" ).sortable({stop: updateIndex}).disableSelection();
				$("#update_index").click(function() {
					var array = {};
					$('.table-striped').find("tr").each(function(i){
						var menu_sevice_id = $(this).find('td.index').data('id');
						var index = $(this).find('td.index').text();
						var category = $(this).find('td.index').data('category');
						if (typeof menu_sevice_id === "undefined") {
							return;
						}
						array[i] = {'id':menu_sevice_id, 'value':index,'category':category};
					});
					var route = "<?php echo route('directory/Service/Index'); ?>";
					$.ajax({
						url: route,
						type: 'POST',
						dataType: 'json',
						data: {data : array},
						success: function(result) {
							toastr.success('Service Menu Index Update successfully.');
						}
					});
				});
				$('.delete').click(function () {
					var menu_id = $(this).data('rel');
					swal({
						title: "Are you sure?",
						text: "Your will not be able to recover this Menu!",
						type: "warning",
						showCancelButton: true,
						confirmButtonColor: "#DD6B55",
						confirmButtonText: "Yes, delete it!",
						cancelButtonText: "No, cancel plx!",
						closeOnConfirm: false,
						closeOnCancel: false },
						function (isConfirm) {
							if (isConfirm) {
								var route = "<?php echo route('directory/service/delete'); ?>";
								$.ajax({
									url: route,
									type: 'POST',
									dataType: 'json',
									data: {menu_id : menu_id},
									success: function(result) {
										if(result.response == 'success'){
											$('.ui-state-default-'+menu_id).remove();
											swal("Deleted!", "Menu has been deleted.", "success");
											window.location.reload();
										}
									}
								});
							} else {
								swal("Cancelled", "Menu is safe :)", "error");
							}
						});
				});
			});
		</script>
		@stop