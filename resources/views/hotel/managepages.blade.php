@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<script src="{{URL::asset('asset/js/underscore-min.js')}}"></script>
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Manage Content</h2>
	</div>
	<div class="col-lg-2">
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<script type="text/template" id='page_type'>
		<% _.each(data, function (element) { %>
		<div class="col-xs-6 col-sm-4 col-md-3 col-lg-3">
			<a href="<%= element.action %>" class="btn btn-primary btn-circle btn-lg btn-xxl">
				<h2 class="name"><%= element.name %></h2>
				<p><i class="<%= element.icon %>"></i></p>
			</a>
		</div>
		<% }); %>
	</script>
	<div class="row">
		<div class="ibox">
			<div class="ibox-content">
				<div class="row mainbuttons" id="page_type_pages" style="margin: 0px;">

				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script type="text/javascript">
	$(window).load(function(){
		var route = "<?php echo route('directory/page/type'); ?>";
		var token = $('#token').val();
		console.log(token);
		$.ajax({
			url: route,
			type: 'GET',
			success: function(res) {
				var templatestring = _.template($("#page_type").html());
				$('#page_type_pages').append(templatestring({data: res}));
			}
		});
	});
	var elem = document.querySelector('.js-switch-1');
	var switchery = new Switchery(elem, { color: '#1AB394',disabled:true});
	var elem = document.querySelector('.js-switch-2');
	var switchery = new Switchery(elem, { color: '#1AB394' ,disabled:true});
</script>
@stop