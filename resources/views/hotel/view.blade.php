@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-9" id="hotel-info">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox">
					<div class="ibox-content">
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									@if(System::isAdmin())
									<a id="delete_hotel" class="btn btn-danger pull-right" 
									data-rel="{{$hotel_info->id}}"><i class="fa fa-trash"></i> {{ trans('form.delete') }}</a>
									@elseif(System::isChainMember())
									<a class="btn btn-danger pull-right" data-rel="{{$hotel_info->id}}" onclick="deleteHotel({{$hotel_info->id}})"><i class="fa fa-trash"></i> {{ trans('form.delete') }}</a>
									@endif
									<a class="btn btn-primary pull-right m-r-md" href="{{ route('hotel/edit', ['id' => $hotel_info->id]) }}"><i class="fa fa-edit"></i> {{ trans('form.edit') }}</a>
									<h2>{{ $hotel_info->name }}</h2>
								</div>

							</div>
						</div>
						<div class="row">
							<div class="col-lg-5">
								<dl class="dl-horizontal">
									<dt>{{ trans('form.name') }}:</dt> <dd>  {{ $hotel_info->name }}</dd>
									<dt>{{ trans('form.email') }}:</dt> <dd><a href="mailto:{{ $hotel_info->email }}" class="text-navy">{{ $hotel_info->email }}</a> </dd>
									<dt>{{ trans('form.phone') }}:</dt> <dd><a href="tel:{{ $hotel_info->phone }}" class="text-navy">{{ $hotel_info->phone }}</a> </dd>
									<dt>{{ trans('form.city') }}:</dt> <dd>{{ $hotel_info->city }}</dd>
									<dt>{{ trans('form.country') }}:</dt> <dd>{{ $hotel_info->country }}</dd>
								</dl>
							</div>
							<div class="col-lg-7" id="cluster_info">
								<dl class="dl-horizontal" >
									<dt>{{ trans('form.address') }}:</dt> <dd>{{ $hotel_info->address_1 }}</dd>
									<dt>{{ trans('form.postal_code') }}:</dt> <dd>{{ $hotel_info->postal_code }}</dd>
									<dt>{{ trans('form.website') }}:</dt> <dd><a href="{{ $hotel_info->website }}" target="_blank" class="text-navy">{{ $hotel_info->website }}</a></dd>
								</dl>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-3">
			<div class="wrapper wrapper-content animated fadeInUp">
				<div class="ibox">
					<div class="ibox-content">
						@if(System::isAdmin())
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Add Members</h3>
								</div>
							</div>
						</div>

						<div class="row">
							<form id="add-member"  method="post" action="{{ URL::route('assign/user')}}">
								<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
								<input type="hidden" name="entity_id" value="{{ $hotel_info->id }}"/>
								<div class="col-lg-8">
									{!! Form::select('user_id', ['' => 'Choose member'] + $user_list,null,['id'=>'choose-member','class' => 'chosen-select form-control','data-placeholder'=>'Choose a Member...']) !!}
								</div>
								<div class="col-lg-4">
									<button type="submit" name="submit_role" value="hotel" class="btn btn-primary" id="add-user">Add</button>
								</div>
								<div class="col-lg-12">
									<label class="error custom-error" style="display:none;"> Please choose a member...</label>
								</div>
							</form>
						</div>
						@endif
						<div class="row">
							<div class="col-lg-12">
								<div class="m-b-md">
									<h3>Assign Members</h3>
								</div>
								<div class="table-responsive">
									<table class="table table-striped table-hover">
										<tbody>
											@if($hotel_info->hotels->count()!=0)
											@foreach ($hotel_info->hotels as $hotel)
											<tr><td>{{ $hotel->user->first_name }}
												@if(System::isAdmin())
												<a
												data-user="{{ $hotel->user->id}}" data-role="{{ $hotel->role_id}}" data-hotel= "{{$hotel_info->id}}"
												class="close-link pull-right my-cross-link"><i class="fa fa-times"></i></a>
												@endif
											</td></tr>
											@endforeach
											@else
											<td>Not yet assign user</td>
											@endif
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script type="text/javascript">
	$(document).ready(function() {
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		var telInput = $("#phone"),
		errorMsg = $("#error-msg"),
		validMsg = $("#valid-msg");
		telInput.intlTelInput({
			utilsScript: "{{ URL::asset('asset/js/plugins/intelinput/utils.js') }}",
			customPlaceholder:function(selectedCountryPlaceholder, selectedCountryData) {
				$('#message').html(selectedCountryPlaceholder);
				return selectedCountryPlaceholder;
			}
		});
		var reset = function() {
			telInput.removeClass("error");
			errorMsg.addClass("hide");
			validMsg.addClass("hide");
		};
// on blur: validate
telInput.blur(function() {
	reset();
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			validMsg.removeClass("hide");
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			errorMsg.removeClass("hide");
		}
	}
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
$(document).ready(function(){
	var data = $("#phone").intlTelInput("getSelectedCountryData");
	var phone = '{{ $hotel_info->phone }}';
	$('#phone').val(phone.replace('+'+data.dialCode,''));
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-chain'           : {},
		'.chosen-select-1'           : {},
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}

	$("#urlBanner").change(function() {
		if (!/^http:\/\//.test(this.value)) {
			this.value = "http://" + this.value;
		}
	});
	$( "#add-member" ).submit(function( event ) {
		if($( "#choose-member" ).val()==''){
			$('.custom-error').show();
			event.preventDefault();
			return;
		}
	});
});

function edit(){
	$('#edit-hotel').show();
	$('#hotel-info').css('display','none');
	$(".chosen-select-1").val('{{$hotel_info->country}}').trigger("chosen:updated");
	$(".chosen-select-chain").val('{{$hotel_info->chain_id}}').trigger("chosen:updated");
}
function reload(){
	location.reload();
}
$('#delete_hotel').click(function () {
	var hotel_id = $(this).data('rel');
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this Hotel!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('hotel/delete'); ?>";
				var redirect_route = '<?php echo route('hotel/list'); ?>';
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {hotel_id : hotel_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "Hotel has been deleted.", "success");
							window.location = redirect_route;
						}
					}
				});
			} else {
				swal("Cancelled", "Hotel is safe :)", "error");
			}
		});
});
$('.close-link').click(function () {
	var user_id = $(this).data('user');
	var role_id = $(this).data('role');
	var hotel_id = $(this).data('hotel')
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this User!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('remove/access'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {user_id : user_id, role_id:role_id, entity_type:'hotel',hotel_id:hotel_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "User was successfully removed to Hotel.", "success");
							window.location.reload();
						}
					}
				});
			} else {
				swal("Cancelled", "Hotel is safe :)", "error");
			}
		});
});

function removeUser(user_id,role_id){
	$('#remove-user').modal('show');
	$('#remove-user-id').val(user_id);
	$('#remove-role-id').val(role_id);
}
});
</script>
<style type="text/css">
	.chosen-container.chosen-container-single{
		width:200px !important;
	}
</style>
@stop