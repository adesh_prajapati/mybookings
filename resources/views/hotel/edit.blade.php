@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div  class="col-lg-9">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>{{ trans('form.edit') }} Hotel Info</h5>
				</div>
				<div class="ibox-content">
					<form id="update-hotel"  data-parsley-validate class="form-horizontal" method="post" action="{{ URL::route('add/hotel')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" name="hotel_id" value="{{ $hotel_info->id }}"/>
						<input type="hidden" id="phone-number" value="{{ $hotel_info->phone }}" name="phone_number"/>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.email') }}</label>
							<div class="col-lg-10"><input type="email" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" name="email" placeholder="Email" value="{{ $hotel_info->email }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.name') }}</label>

							<div class="col-lg-10"><input type="text" name="name" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout"  placeholder="{{ trans('form.name') }}" value="{{ $hotel_info->name }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">Slug</label>

							<div class="col-lg-10"><input type="text" onkeyup="createSlug(this, 'created_slug')" onblur="createSlug(this, 'created_slug')" name="slug"  pattern="/^[a-z0-9-_]+$/" data-parsley-required-message="Slug is required" data-parsley-trigger="change focusout"  placeholder="Slug" value="{{ $hotel_info->slug }}" class="form-control" required="">
								<p class="created_slug"></p>
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_1') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout"  name="address_1" value="{{ $hotel_info->address_1 }}" placeholder="{{ trans('form.address_line_1') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_2') }}</label>

							<div class="col-lg-10"><input type="text" name="address_2"    value="{{ $hotel_info->address_2 }}" placeholder="{{ trans('form.address_line_2') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.province') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-required-message="Province is required" data-parsley-trigger="change focusout"  name="province" value="{{ $hotel_info->province }}" placeholder="{{ trans('form.province') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.postal_code') }}</label>

							<div class="col-lg-10"><input type="text" name="postal_code" data-parsley-required-message="Postal code is required" data-parsley-trigger="change focusout"  value="{{ $hotel_info->postal_code }}" placeholder="{{ trans('form.postal_code') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">{{ trans('form.city') }}</label>

							<div class="col-lg-10">
								<input type="text" name="city" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" placeholder="{{ trans('form.city') }}" value="{{ $hotel_info->city }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">{{ trans('form.country') }}</label>
							<div class="col-lg-4">
								{!! Form::select('country', $country,null,['id'=>'country','class' => 'chosen-select-1 form-control','data-placeholder'=>'Choose a Country...']) !!}
							</div>
							<label class="col-lg-2 control-label">
								{{ trans('form.timezone') }}</label>
								<div class="col-lg-4">
									{!! Form::select('timezone', $timezone,null,['class' => 'chosen-select-timezone form-control','data-placeholder'=>'Choose a Timezone...','value'=>"{{ old('timezone') }}"]) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.phone') }}</label>
								<div class="col-lg-6">
									<input type="text" id="phone" name="phone" data-parsley-required-message="Phone is required" data-parsley-trigger="change focusout"  value="{{ $hotel_info->phone }}" class="form-control" required="">
									<span class="hide" id="valid-msg">✓ Valid</span>
									<span class="hide" id="error-msg">Invalid number</span>
								</div>
								<div class="col-lg-4">
									<div id="message"></div>
								</div>
							</div>
							<div class="form-group"><label class="col-lg-2 control-label">Chain</label>

								<div class="col-lg-10">
									{!! Form::select('chain_id', ['' => 'choose-chain'] + $chains,null,['class' => 'chosen-select-chain form-control','data-placeholder'=>'Please create a chain','value'=>"{{ old('chain_id') }}"]) !!}
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-2"></div>
								<div class="col-lg-10">
									<div class="invalid-form-error-message"></div>
								</div>
							</div>
							<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.website') }}</label>

								<div class="col-lg-10"><input id="urlBanner" data-parsley-type="url" type="text" name="website" data-parsley-required-message="Website is required" data-parsley-trigger="change focusout"  value="{{ $hotel_info->website }}" placeholder="{{ trans('form.website') }}" class="form-control" required="">
								</div>
							</div>
                                                
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_facebook') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_facebook" data-parsley-type="url" name="social_facebook" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ $hotel_info->social_facebook}}" placeholder="{{ trans('form.social_facebook') }}" class="form-control" required="">
								</div>
							</div>
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_twitter') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_twitter" data-parsley-type="url" name="social_twitter" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ $hotel_info->social_twitter }}" placeholder="{{ trans('form.social_twitter') }}" class="form-control" required="">
								</div>
							</div>
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_instagram') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_instagram" data-parsley-type="url" name="social_instagram" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ $hotel_info->social_instagram }}" placeholder="{{ trans('form.social_instagram') }}" class="form-control" required="">
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.status') }}</label>
								<div class="col-lg-4">
									<div class="radio radio-success">
										<input type="radio" id="inlineRadio1" value="Active" name="status" {{($hotel_info->status == 'Active') ? 'checked=""' : '' }}>
										<label for="inlineRadio1">Active</label>
									</div>
									<div class="radio radio-danger">
										<input type="radio" value="Inactive" name="status" {{($hotel_info->status == 'Inactive') ? 'checked=""' : '' }}>
										<label for="inlineRadio2">Inactive</label>
									</div>
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<a type="button" class="btn btn-white" href="{{route('hotel/list')}}"><i class="fa fa-times"></i> {{ trans('form.cancel') }}</a>
									<button type="submit" name="update" value="update" class="btn btn-primary"><i class="fa fa-save"></i> {{ trans('form.update') }}</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="wrapper wrapper-content animated fadeInUp">
					<div class="ibox">
						<div class="ibox-content">
							@if(System::isAdmin())
							<div class="row">
								<div class="col-lg-12">
									<div class="m-b-md">
										<h3>Add Members</h3>
									</div>
								</div>
							</div>

							<div class="row">
								<form id="add-member"  method="post" action="{{ URL::route('assign/user')}}">
									<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
									<input type="hidden" name="entity_id" value="{{ $hotel_info->id }}"/>
									<div class="col-lg-8">
										{!! Form::select('user_id', ['' => 'Choose member'] + $user_list,null,['id'=>'choose-member','class' => 'chosen-select form-control','data-placeholder'=>'Choose a Member...']) !!}
									</div>
									<div class="col-lg-4">
										<button type="submit" name="submit_role" value="hotel" class="btn btn-primary" id="add-user">Add</button>
									</div>
									<div class="col-lg-12">
										<label class="error custom-error" style="display:none;"> Please choose a member...</label>
									</div>
								</form>
							</div>
							@endif
							<div class="row">
								<div class="col-lg-12">
									<div class="m-b-md">
										<h3>Assign Members</h3>
									</div>
									<div class="table-responsive">
										<table class="table table-striped table-hover">
											<tbody>
												@if($hotel_info->hotels->count()!=0)
												@foreach ($hotel_info->hotels as $hotel)
												<tr><td>{{ $hotel->user->first_name }}
													@if(System::isAdmin())
													<a
													data-user="{{ $hotel->user->id}}" data-role="{{ $hotel->role_id}}" data-hotel= "{{$hotel_info->id}}"
													class="close-link pull-right my-cross-link"><i class="fa fa-times"></i></a>
													@endif
												</td></tr>
												@endforeach
												@else
												<td>Not yet assign user</td>
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal inmodal fade" id="remove-user" tabindex="-1" role="dialog"  aria-hidden="true">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
						<form id="delete-hotel" method="post" action="{{ URL::route('remove/access')}}">
							<div class="modal-body">
								<p><strong>{{ trans('validation.confirm_user_delete', ['attribute' => $hotel_info->name,'name'=>'hotel' ]) }} </strong></p>
								<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
								<input type="hidden" name="user_id" id="remove-user-id"/>
								<input type="hidden" name="entity_type" value="hotel"/>
								<input type="hidden" name="role_id"  id="remove-role-id"/>
								<input type="hidden" name="hotel_id" value="{{ $hotel_info->id }}"/>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-white" data-dismiss="modal">{{ trans('form.cancel') }}</button>
								<button type="submit"    class="btn btn-danger">Remove User</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.script')
	<script type="text/javascript">
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val()}
		});
		$(".chosen-select-chain").val('{{$hotel_info->chain_id}}').trigger("chosen:updated");
		$(".chosen-select-1").val('{{$hotel_info->country}}').trigger("chosen:updated");
		$(".chosen-select-timezone").val('{{$hotel_info->timezone}}').trigger("chosen:updated");
		function createSlug(val, type){
			if(val.value.length == 0){
				$('.'+type).html('');
			}else{
				$('.'+type).html(val.value+"."+"{{ Config::get('app.url') }}"+"/directory");
			}
		}
		var telInput = $("#phone"),
		errorMsg = $("#error-msg"),
		validMsg = $("#valid-msg");
		telInput.intlTelInput({
			utilsScript: "{{ URL::asset('asset/js/plugins/intelinput/utils.js') }}",
			customPlaceholder:function(selectedCountryPlaceholder, selectedCountryData) {
				$('#message').html(selectedCountryPlaceholder);
				return selectedCountryPlaceholder;
			}
		});
		var reset = function() {
			telInput.removeClass("error");
			errorMsg.addClass("hide");
			validMsg.addClass("hide");
		};
// on blur: validate
telInput.blur(function() {
	reset();
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			validMsg.removeClass("hide");
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			errorMsg.removeClass("hide");
		}
	}
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
$(document).ready(function(){
	var data = $("#phone").intlTelInput("getSelectedCountryData");
	var phone = '{{ $hotel_info->phone }}';
	$('#phone').val(phone.replace('+'+data.dialCode,''));
	var config = {
		'.chosen-select'           : {},
		'.chosen-select-chain'           : {},
		'.chosen-select-1'           : {},
		'.chosen-select-timezone'           : {},
	}
	for (var selector in config) {
		$(selector).chosen(config[selector]);
	}

	$("#urlBanner").change(function() {
		if (!/^http:\/\//.test(this.value)) {
			this.value = "http://" + this.value;
		}
	});
	$( "#add-member" ).submit(function( event ) {
		if($( "#choose-member" ).val()==''){
			$('.custom-error').show();
			event.preventDefault();
			return;
		}
	});
});

function reload(){
	location.reload();
}

$('.close-link').click(function () {
	var user_id = $(this).data('user');
	var role_id = $(this).data('role');
	var hotel_id = $(this).data('hotel')
	swal({
		title: "Are you sure?",
		text: "You will not be able to recover this User!",
		type: "warning",
		showCancelButton: true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText: "Yes, delete",
		cancelButtonText: "No, cancel",
		closeOnConfirm: false,
		closeOnCancel: false },
		function (isConfirm) {
			if (isConfirm) {
				var route = "<?php echo route('remove/access'); ?>";
				$.ajax({
					url: route,
					type: 'POST',
					dataType: 'json',
					data: {user_id : user_id, role_id:role_id, entity_type:'hotel',hotel_id:hotel_id},
					success: function(result) {
						if(result.response == 'success'){
							swal("Deleted!", "User was successfully removed to Hotel.", "success");
							window.location.reload();
						}
					}
				});
			} else {
				swal("Cancelled", "Hotel is safe :)", "error");
			}
		});
});
function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
	if (event.keyCode === 8 || event.keyCode === 46
		|| event.keyCode === 37 || event.keyCode === 39) {
		return true;
}
else if ( key < 48 || key > 57 ) {
	return false;
}
else return true;
};
$('#update-hotel').parsley().on('form:validate', function (formInstance) {
	if($( ".chosen-select-chain" ).val()!='') {
		$('.invalid-form-error-message').html('');
	}else{
		$('.invalid-form-error-message').html('please choose a chain');
		formInstance.validationResult = false;
		return false;
	}
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
			formInstance.validationResult = true;
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			formInstance.validationResult = false;
		}
	}
});
$("#update-hotel").submit( function(eventObj) {
	$('#phone-number').val($("#phone").intlTelInput("getNumber"));
	return true;
});
</script>
<style type="text/css">
	.chosen-container.chosen-container-single{
		width:200px !important;
	}
</style>
@stop