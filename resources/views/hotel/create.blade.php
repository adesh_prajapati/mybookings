@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="ibox float-e-margins">
				<div class="ibox-title">
					<h5>Add Hotel</h5>
				</div>
				<div class="ibox-content">
					<form id="create-hotel" data-parsley-validate class="form-horizontal" method="post" action="{{ URL::route('add/hotel')}}">
						<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
						<input type="hidden" id="phone-number" value="{{ old('phone_number') }}" name="phone_number"/>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.email') }}</label>

							<div class="col-lg-10"><input type="email" data-parsley-required-message="Email is required" data-parsley-trigger="change focusout" data-parsley-type="email" name="email" placeholder="{{ trans('form.email') }}" value="{{ old('email') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.name') }}</label>

							<div class="col-lg-10"><input type="text" name="name" data-parsley-required-message="Name is required" data-parsley-trigger="change focusout"  placeholder="{{ trans('form.name') }}" value="{{ old('name') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">Slug</label>

							<div class="col-lg-10">
								<input id="slug" type="text" pattern="/^[a-z0-9-_]+$/" 
								onkeyup="createSlug(this, 'created_slug')" 
								onblur="createSlug(this, 'created_slug')" name="slug" 
								data-parsley-required-message="Slug is required" data-parsley-trigger="change focusout"  placeholder="Slug" value="{{ old('slug') }}" class="form-control" required="">
								<p class="created_slug"></p>
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">Chain</label>

							<div class="col-lg-10">
								{!! Form::select('chain_id', ['' => 'choose-chain'] + $chains,null,['class' => 'chosen-select-chain form-control','data-placeholder'=>'Please choose a chain','value'=>"{{ old('chain_id') }}"]) !!}
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label"></label>
							<div class="col-lg-10">
								<div class="i-checks"><label> <input type="checkbox" id="itself-chain" name="as_a_chain" value="on" data-parsley-group="block1"> <i></i> This Hotel is itself a chain</label></div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-lg-2"></div>
							<div class="col-lg-10">
								<div class="invalid-form-error-message"></div>
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_1') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-required-message="Address is required" data-parsley-trigger="change focusout"  name="address_1" value="{{ old('address_1') }}" placeholder="{{ trans('form.address_line_1') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.address_line_2') }}</label>

							<div class="col-lg-10"><input type="text" name="address_2"    value="{{ old('address_2') }}" placeholder="{{ trans('form.address_line_2') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.province') }}</label>

							<div class="col-lg-10"><input type="text" data-parsley-trigger="change focusout"  name="province" value="{{ old('province') }}" placeholder="{{ trans('form.province') }}" class="form-control">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.postal_code') }}</label>

							<div class="col-lg-10"><input type="text" name="postal_code" data-parsley-required-message="Postal code is required" data-parsley-trigger="change focusout"   value="{{ old('postal_code') }}" placeholder="{{ trans('form.postal_code') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.city') }}</label>

							<div class="col-lg-10"><input type="text" name="city" data-parsley-required-message="City is required" data-parsley-trigger="change focusout" placeholder="{{ trans('form.city') }}" value="{{ old('city') }}" class="form-control" required="">
							</div>
						</div>
						<div class="form-group"><label class="col-lg-2 control-label">{{ trans('form.phone') }}</label>

							<div class="col-lg-6">
								<input type="text" id="phone"  name="phone"  data-parsley-required-message="Phone is required" data-parsley-trigger="change focusout"  value="{{ old('phone') }}"  class="form-control" required="">
								<span class="hide" id="valid-msg">✓ Valid</span>
								<span class="hide" id="error-msg">Invalid number</span>
							</div>
							<div class="col-lg-4">
								<div  id="message"></div>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-2 control-label">{{ trans('form.country') }}</label>
							<div class="col-lg-4">
								{!! Form::select('country', $country,null,['class' => 'chosen-select form-control','data-placeholder'=>'Choose a Country...','value'=>"{{ old('country') }}"]) !!}
							</div>
							<label class="col-lg-2 control-label">
								{{ trans('form.timezone') }}</label>
								<div class="col-lg-4">
									{!! Form::select('timezone', $timezone,null,['class' => 'chosen-select form-control','data-placeholder'=>'Choose a Timezone...','value'=>"{{ old('country') }}"]) !!}
								</div>
							</div>
							<div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.website') }}</label>
								<div class="col-lg-10">
									<input type="text" id="urlBanner" data-parsley-type="url" name="website" data-parsley-required-message="Website is required" data-parsley-trigger="change focusout"  value="{{ old('website') }}" placeholder="{{ trans('form.website') }}" class="form-control" required="">
								</div>
							</div>
                                                
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_facebook') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_facebook" data-parsley-type="url" name="social_facebook" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ old('social_facebook') }}" placeholder="{{ trans('form.social_facebook') }}" class="form-control" required="">
								</div>
							</div>
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_twitter') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_twitter" data-parsley-type="url" name="social_twitter" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ old('social_twitter') }}" placeholder="{{ trans('form.social_twitter') }}" class="form-control" required="">
								</div>
							</div>
                                                        <div class="form-group">
								<label class="col-lg-2 control-label">{{ trans('form.social_instagram') }}</label>
								<div class="col-lg-10">
									<input type="text" id="social_instagram" data-parsley-type="url" name="social_instagram" data-parsley-required-message="Must be a valid URL" data-parsley-trigger="change focusout"  value="{{ old('social_instagram') }}" placeholder="{{ trans('form.social_instagram') }}" class="form-control" required="">
								</div>
							</div>
							<div class="form-group">
								<div class="col-lg-offset-2 col-lg-10">
									<a type="button" class="btn btn-white" href="{{route('hotel/list')}}"><i class="fa fa-times"></i> {{ trans('form.cancel') }}</a>
									<button type="submit" name="update"  value="create" class="btn btn-primary"><i class="fa fa-save"></i> {{ trans('form.submit') }}</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	@include('layouts.script')
	<script type="text/javascript">
		$.ajaxSetup({
			headers: { 'X-CSRF-Token' : $('input[name="_token"]').val() }
		});
	</script>
	<script type="text/javascript">
		function createSlug(val, type){
			if(val.value.length == 0){
				$('.'+type).html('');
			}else{
				$('.'+type).html(val.value+"."+"{{ Config::get('app.url') }}"+"/directory");
			}
		}
		var telInput = $("#phone"),
		errorMsg = $("#error-msg"),
		validMsg = $("#valid-msg");
		telInput.intlTelInput({
			utilsScript: "{{ URL::asset('asset/js/plugins/intelinput/utils.js') }}",
			preferredCountries: [ "nl"],
			customPlaceholder:function(selectedCountryPlaceholder, selectedCountryData) {
				$('#message').html(selectedCountryPlaceholder);
				return selectedCountryPlaceholder;
			}
		});
		var reset = function() {
			telInput.removeClass("error");
			errorMsg.addClass("hide");
			validMsg.addClass("hide");
		};
// on blur: validate
telInput.blur(function() {
	reset();
	if ($.trim(telInput.val())) {
		if (telInput.intlTelInput("isValidNumber")) {
			validMsg.removeClass("hide");
			telInput.removeClass("parsley-error");
			telInput.addClass("parsley-success");
		} else {
			telInput.removeClass("parsley-success");
			telInput.addClass("parsley-error");
			errorMsg.removeClass("hide");
		}
	}
});

// on keyup / change flag: reset
telInput.on("keyup change", reset);
$(document).ready(function(){
		//$('[id^=phone]').keypress(validateNumber);
		$("#slug").blur(function (formInstance) {
			var route = "<?php echo route('hotel/slug'); ?>";
			var value = $(this).val();
			$.ajax({
				type: 'POST',
				url: route,
				dataType: 'json',
				data: { 'value' : value},
				success : function(res){
					console.log(res.result);
					if(res.result == 'yes'){
						$("#slug").removeClass("parsley-success");
						$("#slug").addClass("parsley-error");
						$("#slug").removeClass("hide");
						$('#slug').after('<div class="error">Slug already exists</div>');
					}else{
						$("#slug").addClass("parsley-success");
						$("#slug").removeClass("parsley-error");
						$(".error").html('');
					}
				}
			});
		});
		var config = {
			'.chosen-select'           : {},
			'.chosen-select-chain'           : {},
		}
		for (var selector in config) {
			$(selector).chosen(config[selector]);
			$(selector).val('Netherlands').trigger("chosen:updated");
		}
		$("#urlBanner").change(function() {
			if (!/^http:\/\//.test(this.value)) {
				this.value = "http://" + this.value;
			}
		});

		$('.i-checks').iCheck({
			checkboxClass: 'icheckbox_square-green'
		});

		
		$('.chosen-select-chain').on('change', function(){
			$('.i-checks').iCheck('uncheck');
		});
		$('.i-checks').on('ifChecked', function() {
			$(".chosen-select-chain").val('').trigger("chosen:updated");
		});
	});
function reload(){
	alert('i m here');
	location.reload();
}
function validateNumber(event) {
	var key = window.event ? event.keyCode : event.which;
	if (event.keyCode === 8 || event.keyCode === 46
		|| event.keyCode === 37 || event.keyCode === 39) {
		return true;
}
else if ( key < 48 || key > 57 ) {
	return false;
}
else return true;
};
</script>
@if(System::isAdmin())
<script type="text/javascript">
	$('#create-hotel').parsley().on('form:validate', function (formInstance) {
		if ($.trim(telInput.val())) {
			if (telInput.intlTelInput("isValidNumber")) {
				telInput.removeClass("parsley-error");
				telInput.addClass("parsley-success");
				formInstance.validationResult = true;
			} else {
				telInput.removeClass("parsley-success");
				telInput.addClass("parsley-error");
				formInstance.validationResult = false;
			}
		}
	});
	$("#create-hotel").submit( function(eventObj) {
		if($('#itself-chain').parent().hasClass('checked') == false && !$( ".chosen-select-chain" ).val()) {
			toastr.error('You must either choose a chain or make this hotel itself a chain.');
			return false;
		}
		$('#phone-number').val($("#phone").intlTelInput("getNumber"));
		return true;
	});
</script>
@endif
<style type="text/css">
	.intl-tel-input{
		width: 100%;
	}
</style>
@stop