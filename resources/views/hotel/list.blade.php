@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
			{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
  <div class="wrapper wrapper-content animated fadeInRight ecommerce">
    <div class="row">
      <div class="col-lg-12">
        <div class="ibox">
          <div class="ibox-title">
            <h5>All Hotel assigned to this account</h5>
            <input type="hidden" name="_token" value="{{ csrf_token()}}"/>
            <div class="ibox-tools">
              <a class="btn btn-primary btn-sm" href="{{ URL::route('hotel/create')}}">
                <i class="fa fa-plus"></i>
                Create new Hotel</a>
              </div>
            </div>
            @if($hotel_lists->count())
            <div class="ibox-content">
              <table class="footable table table-stripped toggle-arrow-tiny" 
              data-page-size="15">
              <thead>
                <tr>
                  <th data-toggle="true">Status</th>
                  <th data-hide="name">Name</th>
                  <th data-hide="email">Email</th>
                  <th data-hide="website">Website</th>
                  <th class="text-right" data-sort-ignore="true">Action</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($hotel_lists as $index => $hotel)
                <tr id="page_data_{{$hotel->id}}">
                  <td>
                    @if($hotel->status == 'Active')
                    <span class="label label-primary">{{$hotel->status}}</span>
                    @else
                    <span class="label label-default">{{$hotel->status}}</span>
                    @endif
                  </td>
                  <td>
                    <a class="text-navy" href="{{ route('hotel/view', ['id' => $hotel->id]) }}">{{ $hotel->name }}</a>
                    <br/>
                    <small>
                    <a class="text-grey" target="_blank" href="{{ route('directory/redirect', ['hotel_slug' => $hotel->slug])}}"><i class="fa fa-sitemap"> </i>{{ $hotel->slug }}</a>
                    </small>
                  </td>
                  <td>
                    <a href="mailto:{{ $hotel->email }}" class="text-navy">{{ $hotel->email }}</a>
                    <br/>
                  </td>
                  <td>
                    <a href="{{ $hotel->website }}" target="_blank" class="text-navy"><i class="fa fa-sitemap"> </i> {{ $hotel->website }}</a>
                    <br/>
                  </td>
                  <td class="text-right">
                    <div class="btn-group">
                      <a href="{{ route('hotel/view', ['id' => $hotel->id]) }}" 
                      class="btn-white btn btn-xs delete"><i class="fa fa-folder"></i> View </a>
                      <a href="{{ route('hotel/edit', ['id' => $hotel->id]) }}" 
                      class="btn-white btn btn-xs"><i class="fa fa-pencil"></i> Edit </a>
                    </div>
                  </td>
                </tr>
                @endforeach
              </tbody>
              <tfoot>
                <tr>
                  <td colspan="6">
                    <ul class="pagination pull-right"></ul>
                  </td>
                </tr>
              </tfoot>
            </table>
          </div>
          @else
          <br/>
          <div class="ibox-content m-b-sm border-bottom">
            <div class="text-center p-lg">
              <h2>No Hotel available</h2>

            </div>
          </div>
          @endif
        </div>
      </div>
    </div>
  </div>
@include('layouts.script')
<script type="text/javascript">
  $(document).ready(function() {
    $('.footable').footable();
  });
</script>
@stop