@extends('layouts.admin')
@section('content')
@include('layouts.navbartop')
<div class="row wrapper border-bottom white-bg page-heading">
	<div class="col-lg-10">
		<h2>Hotel Create Pages</h2>
	</div>
	<div class="col-lg-2">
	</div>
</div>
<div class="wrapper wrapper-content">
	<div class="row">
		@if ($errors->has())
		<div class="alert alert-danger alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			@foreach ($errors->all() as $error)
				{{ $error }}<br>        
			@endforeach
		</div>
		@endif
		@if (session('success'))
		<div class="alert alert-success alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			{{ session('success') }}
		</div>
		@endif
	</div>
</div>
<style>
	.wizard > .content > .body  position: relative; }
</style>
<div class="row">
	<div class="col-lg-10">
		<div class="ibox">
			<div class="ibox-title">
				<h5>Wizard with Validation</h5>
				<div class="ibox-tools">
					<a class="collapse-link">
						<i class="fa fa-chevron-up"></i>
					</a>
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-wrench"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#">Config option 1</a>
						</li>
						<li><a href="#">Config option 2</a>
						</li>
					</ul>
					<a class="close-link">
						<i class="fa fa-times"></i>
					</a>
				</div>
			</div>
			<div class="ibox-content">
				<form id="form" data-parsley-validate class="form-horizontal wizard-big" method="post" action="{{ URL::route('create-hotel-pages')}}">
					<input type="hidden" name="_token" value="{{ csrf_token()}}"/>
					<h1>Create pages</h1>
					<fieldset>
						<div class="row">
							<div class="col-lg-8">
								<div class="m-b-sm m-t-sm">
									<input id="title" placeholder="Title" name="userName" type="text" class="form-control required">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<button id="edit" class="btn btn-primary btn-xs m-l-sm pull-right" type="button">Edit</button>
										<button id="save" class="btn btn-primary  btn-xs pull-right" type="button">Save</button>
									</div>
									<div class="ibox-content no-padding">
										<div class="click2edit wrapper p-md">
											<h3>Lorem Ipsum is simply</h3>
											dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
											when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
											<br/>
											<br/>
											<ul>
												<li>Remaining essentially unchanged</li>
												<li>Make a type specimen book</li>
												<li>Unknown printer</li>
											</ul>
										</div>

									</div>
								</div>
							</div>
							<div class="col-lg-8">
								<div class="form-group">
									<label>Published</label>
									<input type="checkbox" name="published" class="js-switch" checked required/>
								</div>
							</div>
						</div>
					</fieldset>
					<h1>Translate</h1>
					<fieldset>
						<div class="row">
							<div class="col-lg-6">
								<div class="m-b-sm m-t-sm">
									{!! Form::select('language', ['English','Adangme','Albanian'],null,['id'=>'language','class' => 'chosen-select-1 form-control',
									'data-placeholder'=>'Choose a Language...']) !!}
								</div>
								<div class="m-b-sm m-t-sm">
									<input id="address" placeholder="Address" name="text" type="text" class="form-control required">
								</div>
							</div>
							<div class="col-lg-12">
								<div class="ibox float-e-margins">
									<div class="ibox-title">
										<h5>Example of Edit by click and save as a html</h5>
										<button id="edit" class="btn btn-primary btn-xs m-l-sm" type="button">Edit</button>
										<button id="save" class="btn btn-primary  btn-xs" type="button">Save</button>
										<div class="ibox-tools">
											<a class="collapse-link">
												<i class="fa fa-chevron-up"></i>
											</a>
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">
												<i class="fa fa-wrench"></i>
											</a>
											<ul class="dropdown-menu dropdown-user">
												<li><a href="#">Config option 1</a>
												</li>
												<li><a href="#">Config option 2</a>
												</li>
											</ul>
											<a class="close-link">
												<i class="fa fa-times"></i>
											</a>
										</div>
									</div>
									<div class="ibox-content no-padding">
										<div class="click2edit wrapper p-md">
											<h3>Lorem Ipsum is simply</h3>
											dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
											when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
											typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
											<br/>
											<br/>
											<ul>
												<li>Remaining essentially unchanged</li>
												<li>Make a type specimen book</li>
												<li>Unknown printer</li>
											</ul>
										</div>

									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6">
									<div class="form-group">
										<input id="website_url" name="website_url" type="text" 
										placeholder="Website" class="form-control required">
									</div>
									<div class="form-group">
										<input id="phone_no" name="phone_no" type="text" 
										placeholder="Phone" class="form-control required">
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group">
										<input id="category" name="category" type="text" 
										placeholder="Category" class="form-control required">
									</div>
									<div class="form-group">
										<input id="category" name="show_as_hotspot" type="text" 
										placeholder="Show as Hotspot" class="form-control required">
									</div>
								</div>
							</div>
						</div>
					</fieldset>
					<h1>Finish</h1>
					<fieldset>
						<h2>Terms and Conditions</h2>
						<input id="acceptTerms" name="acceptTerms" type="checkbox" class="required"> 
						<label for="acceptTerms">I agree with the Terms and Conditions.</label>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
@include('layouts.script')
<script>
	$(document).ready(function(){
		$("#wizard").steps();
		$("#form").steps({
			bodyTag: "fieldset",
			onStepChanging: function (event, currentIndex, newIndex){
                    // Always allow going backward even if the current step contains invalid fields!
                    if (currentIndex > newIndex){
                    	return true;
                    }

                    // Forbid suppressing "Warning" step if the user is to young
                    if (newIndex === 3 && Number($("#age").val()) < 18)
                    {
                    	return false;
                    }

                    var form = $(this);

                    // Clean up if user went backward before
                    if (currentIndex < newIndex)
                    {
                        // To remove error styles
                        $(".body:eq(" + newIndex + ") label.error", form).remove();
                        $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
                    }

                    // Disable validation on fields that are disabled or hidden.
                    form.validate().settings.ignore = ":disabled,:hidden";

                    // Start validation; Prevent going forward if false
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex)
                {
                    // Suppress (skip) "Warning" step if the user is old enough.
                    if (currentIndex === 2 && Number($("#age").val()) >= 18){
                    	$(this).steps("next");
                    }

                    // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
                    if (currentIndex === 2 && priorIndex === 3){
                    	$(this).steps("previous");
                    }
                },
                onFinishing: function (event, currentIndex){
                	var form = $(this);
                    // Disable validation on fields that are disabled.
                    // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
                    form.validate().settings.ignore = ":disabled";

                    // Start validation; Prevent form submission if false
                    return form.valid();
                },onFinished: function (event, currentIndex){
                	var form = $(this);
                    // Submit form input
                    form.submit();
                }
            }).validate({
            	errorPlacement: function (error, element){
            		element.before(error);
            	},
            	rules: {
            		confirm: {
            			equalTo: "#password"
            		}
            	}
            });
            var elem = document.querySelector('.js-switch');
            var switchery = new Switchery(elem, { color: '#1AB394' });
            $('.summernote').summernote();

            $('#edit').click(function(){
            	$('.click2edit').summernote({focus: true});
            });
            $('#save').click(function(){
            	var aHTML = $('.click2edit').code();
            	$('.click2edit').destroy();
            });
        });
</script>
@stop